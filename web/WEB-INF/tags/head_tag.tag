<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="tiaxa" tagdir="/WEB-INF/tags" %>

<head>
<meta charset="utf-8"
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
<title>AA - PCM</title>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/vue.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/axios.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/vue-resource.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/pace.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/coreui.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/custom-tooltips.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/main.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/vue-treeselect.min.js"></script>
<link href="<c:out value="${pageContext.request.contextPath}"/>/Styles/css/coreui-icons.min.css" rel="stylesheet">
<link href="<c:out value="${pageContext.request.contextPath}"/>/Styles/css/flag-icon.min.css" rel="stylesheet">
<link href="<c:out value="${pageContext.request.contextPath}"/>/Styles/css/font-awesome.min.css" rel="stylesheet">
<link href="<c:out value="${pageContext.request.contextPath}"/>/Styles/css/simple-line-icons.css" rel="stylesheet">
<link href="<c:out value="${pageContext.request.contextPath}"/>/Styles/css/vue-select.css" rel="stylesheet">
<link href="<c:out value="${pageContext.request.contextPath}"/>/Styles/css/vue-treeselect.min.css" rel="stylesheet">
<link href="<c:out value="${pageContext.request.contextPath}"/>/Styles/css/style.css" rel="stylesheet">
<link href="<c:out value="${pageContext.request.contextPath}"/>/Styles/css/pace.min.css" rel="stylesheet">

<script src="https://unpkg.com/vuejs-datepicker"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@^2"></script>

</head>
