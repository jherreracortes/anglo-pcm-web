<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core'      prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt'       prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div id="app">
    <ol class="breadcrumb">
          <li class="breadcrumb-item">Inicio</li>
          <li class="breadcrumb-item">
            <a href="#">Formularios</a>
          </li>
        </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <h4 class="card-title mb-0">Tráfico</h4>
                            <div class="small text-muted">24-07-2018</div>
                        </div>
                        <!-- /.col-->
                        <div class="col-sm-2">
                            <select class="form-control" v-model="bot_id" @change="change_data">
                                <option v-for="bot in bots_availables" :value="bot.id_bot">{{ bot.name }}</option>
                            </select>
                        </div>
                        <div class="col-sm-6 d-none d-md-block">
                            <button class="btn btn-primary float-right" type="button" :disabled="refresh_disabled" @click="getDataBot">
                                <i class="fa fa-refresh"></i>
                            </button>
                            <div class="btn-group btn-group-toggle float-right mr-3" data-toggle="buttons">
                                <label class="btn btn-outline-secondary active" @click="changeRange('1d')">
                                    <input id="option1" type="radio" value="1d"> Hoy
                                </label>
                                <label class="btn btn-outline-secondary" @click="changeRange('7d')">
                                    <input id="option2" type="radio" value="7d"> 7 Días
                                </label>
                                <label class="btn btn-outline-secondary" @click="changeRange('15d')">
                                    <input id="option3" type="radio" value="15d"> 15 Días
                                </label>
                                <label class="btn btn-outline-secondary" @click="changeRange('30d')">
                                    <input id="option4" type="radio" value="30d"> 30 Días
                                </label>
                                <label class="btn btn-outline-secondary" @click="changeRange('90d')">
                                    <input id="option5" type="radio" value="90d"> 90 Días
                                </label>
                            </div>
                        </div>
                        <!-- /.col-->
                    </div>
                    <!-- /.row-->
                    <div id="chart-wrapper" class="chart-wrapper">
                        <canvas id="chart_canvas"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<%=request.getContextPath()%>/reports/app.js"></script
