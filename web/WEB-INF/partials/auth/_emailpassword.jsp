<%@page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core'      prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt'       prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>



    <h1>Reset Your Password</h1>
    <p class="text-muted">Send Password Reset Email</p>
    <form method="POST" action="<%=request.getContextPath()%>/auth/resetpassword.html">

    <div class="alert alert-<%=request.getAttribute("status")%>">
      <%=request.getAttribute("message")%>
    </div>

      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text">
            <i class="icon-envelope-letter"></i>
          </span>
        </div>
        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
        <input type="hidden" name="token" value="<%= request.getAttribute("token") %>">
      </div>
      <br>
      <br>

      <div class="row">
        <div class="col-6">
          <button onclick="this.form.submit();" type="button" class="btn btn-primary px-4">Send</button>
        </div>
      </div>

    </form>

    <span id="response"></span>
