<%@page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core'      prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt'       prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>



    <h1>Reset Your Password</h1>
    <br>
    <form id="form" method="POST" v-on:submit="validateForm">

      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text">
            <i class="icon-envelope-letter"></i>
          </span>
        </div>
        <input type="email" class="form-control" id="email" name="email" placeholder="Email" v-model="email" v-bind:class="{ 'is-invalid': attemptSubmit && missingEmail }">
        <div class="invalid-feedback">Este campo es obligatorio.</div>
      </div>

      <div class="input-group mb-3">
          <div class="input-group-prepend">
              <span class="input-group-text">
                  <i class="icon-lock"></i>
              </span>
          </div>
          <input type="password" class="form-control" id="password" name="password" placeholder="Password" v-model="password" v-bind:class="{ 'is-invalid': attemptSubmit && missingPassword }">
          <div class="invalid-feedback">Este campo es obligatorio.</div>
      </div>

      <div class="input-group mb-4">
          <div class="input-group-prepend">
              <span class="input-group-text">
                  <i class="icon-lock"></i>
              </span>
          </div>
          <input type="password" class="form-control" name="c_password" placeholder="Confirm Password">
          <div class="invalid-feedback">Este campo debe ser igual al anterior.</div>
      </div>

      <br>
      <br>

      <div class="row">
        <div class="col-6">
          <button type="submit" class="btn btn-primary px-4">Send</button>
        </div>
      </div>

    </form>

    <span id="response"></span>
