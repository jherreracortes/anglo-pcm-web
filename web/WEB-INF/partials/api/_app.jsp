<%@page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn" %>
<%@ taglib prefix="tiaxa" tagdir="/WEB-INF/tags" %>

var urlProcs = '<%= request.getContextPath() %>/api/';
var urlLogout = '<%=request.getContextPath()%>/logout.shtml';

function loadChart(data_bot) {
    let labels = [];
    let data = [];
    for (let i=0;i<data_bot.length;i++) {
        labels.push(data_bot[i][0]);
    }
    for (let i=0;i<data_bot.length;i++) {
        data.push(data_bot[i][1]);
    }
    $('#chart_canvas').remove();
    $('#chart-wrapper').append('<canvas id="chart_canvas"><canvas>');
    var lineChart = new Chart($('#chart_canvas'), {
        type: 'line',
        data: {
            labels: labels,
            datasets: [
                {
                    label: 'Conversaciones',
                    backgroundColor: 'rgba(151, 187, 205, 0.5)',
                    borderColor: 'rgba(151, 187, 205, 0.8)',
                    highlightFill: 'rgba(151, 187, 205, 0.75)',
                    highlightStroke: 'rgba(151, 187, 205, 1)',
                    lineTension: 0,
                    data: data
                }
            ]
        },
        options: {
            responsive: true
        }
    });
}

var vue = new Vue({
	el : '#app',
	data : {
        bot_id: 0,
        bots_availables : <c:out value="${bots_availables}" escapeXml="false"/>,
        all_data: {},
        current_data: [],
        refresh_disabled: false,
        range: '1d',
        chat_activos: 0
    },
    methods: {
        getChatActivos() {
            axios.get('http://localhost:8080/webBoss/reports/reportOpenChats.json')
                .then(response => {
                    let response_chats = response.data;
                    if (typeof(response_chats) !== 'undefined' && response_chats.cant) {
                        this.chat_activos = response_chats.cant;
                    }
            }, (error) => {
                console.log("SHOW ERROR: " + error);
            });
        },
        changeRange(range) {
            this.range = range;
            this.change_data();
        },
        change_data() {
            current_bot_data = [];
            let new_array = [];
            current_bot_data = this.all_data.filter(row => row.id_bot === this.bot_id);
            if (current_bot_data.length > 0) {
                let data = [];
                if (this.range === '1d') {
                    data = current_bot_data[0].msgs_today;
                } else if (this.range === '7d') {
                    data = current_bot_data[0].msgs_day.slice(-7);
                } else if (this.range === '15d') {
                    data = current_bot_data[0].msgs_day.slice(-15);
                } else if (this.range === '30d') {
                    data = current_bot_data[0].msgs_day.slice(-30);
                } else if (this.range === '90d') {
                    data = current_bot_data[0].msgs_day.slice(-90);
                }
                for(let c = 0;c < data.length;c++) {
                    let d = [];
                    if (this.range === '1d') {
                        d.push(data[c].hour);
                    } else {
                        d.push(data[c].day);
                    }
                    d.push(data[c].cant);
                    new_array.push(d);
                }
                this.current_data = new_array;
                loadChart(this.current_data);
            }
        },
        defineBot() {
            this.bot_id = this.bots_availables[0].id_bot;
        },
        getDataBot() {
            this.refresh_disabled = true;
            axios.get('http://localhost:8080/webBoss/reports/reportGeneral.json')
                .then(response => {
                    let response_bot = response.data;
                    this.all_data = response_bot;
                    this.change_data();
                    this.refresh_disabled = false;
            }, (error) => {
                console.log("SHOW ERROR: " + error);
                this.refresh_disabled = false;
            });
        }

    },
    mounted() {
        this.getChatActivos();
        this.defineBot();
        this.getDataBot();
    }
});
