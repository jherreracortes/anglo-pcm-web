<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core'      prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt'       prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script src="https://unpkg.com/vue@latest"></script>
<script src="https://unpkg.com/vue-select@latest"></script>
<script src="https://unpkg.com/lodash@latest/lodash.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/v-mask/dist/v-mask.min.js"></script>

<div id="app">
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                      <h3 class="card-title mb-0" style="padding-left: 20px;">Sistema Gesti&oacute;n Competencias - Nueva Actividad </h3>
                        <div class="col-sm-4">
                        </div>
                        </div>
                    </div>
                    <div>
                    <div class="table-responsive-md">
                    <table class="table">  
                    <thead >
                      <tbody >
                      <tr>
                        <td style="padding-left: 20px;">Nombre Actividad *</td>
                        <td colspan="2" style="width: 450px;padding-right: 120px;">
                        <input type="text" v-model="nombre_actividad" ref="nombre_actividad" size="22">
                        </td>
                        <th scope="col"></th> 
                        <th scope="col"></th>  
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Programa *</td>
                        <td colspan="2" style="width: 450px;padding-right: 120px;">
                       <treeselect v-model="programa_cbo"
                           :options="programas" 
                           :disable-branch-nodes="true"
                           :show-count="true"
                           />
                        </td>
                        <th scope="col"></th> 
                        <th scope="col"></th>  
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Tipo de Actividad *</td>
                        <td colspan="2" style="width: 450px;padding-right: 120px;">
                        <treeselect v-model="tipo_actividades_cbo"
                           :options="tipo_actividades" 
                           :disable-branch-nodes="true"
                           :show-count="true"
                           />
                        </td>
                        <th scope="col"></th> 
                        <th scope="col"></th>  
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Solicitante *</td>
                         <td colspan="2" style="width: 450px;padding-right: 120px;">
                        <treeselect v-model="solicitante_cbo"
                           :options="solicitantes" 
                           :disable-branch-nodes="true"
                           :show-count="true"
                           />
                        </td>
                          <th scope="col"></th>
                          <th scope="col"></th> 
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Descripci&oacute;n Actividad *</td>
                        <td colspan="2" style="width: 450px;padding-right: 120px;">
                        <textarea rows="5" cols="40" v-model="descripcion" ref="descripcion" placeholder="Agrege una breve Descripcion"></textarea>
                        </td>
                        <th scope="col"></th> 
                        <th scope="col"></th>  
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Horas a Asignar *</td>
                        <td colspan="2" style="width: 450px;padding-right: 120px;">
                        <input type="text" v-model="hh_actividad" ref="hh_actividad" placeholder="Ingrese la Cantidad de Horas" size="22">
                        </td>
                        <th scope="col"></th> 
                        <th scope="col"></th>  
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Equipo *</td>
                        <td colspan="2" style="width: 450px;padding-right: 120px;">
                        <treeselect v-model="maquinaria_cbo"
                           :options="maquinarias" 
                           :disable-branch-nodes="false"
                           :value-consists-of="valueConsistsOf"
                           :multiple="true"
                           :limit="5"
                           :show-count="true"
                           />
                        </td>
                        <th scope="col"></th> 
                        <th scope="col"></th>  
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Meta(Area-Grupo) *</td>
                        <td colspan="2" style="width: 450px;padding-right: 120px;">
                        <treeselect v-model="area_grupo_cbo"
                           :options="grupos" 
                           :disable-branch-nodes="false"
                           :value-consists-of="valueConsistsOf"
                           :multiple="true"
                           :limit="5"
                           :show-count="true"
                           />
                        </td>
                        <th scope="col"></th> 
                        <th scope="col"></th>  
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Fecha Inicial *</td>
                        <td><vuejs-datepicker 
                            :monday-first="true" 
                            :calendar-button="true" 
                            calendar-button-icon="fa fa-calendar" 
                            :bootstrap-styling="true"
                            format="dd-MM-yyyy" 
                            id="date" 
                            v-model="fecha"></vuejs-datepicker></td>
                          <td style="padding-left: 20px;">Fecha T&eacute;rmino *</td>
                        <td><vuejs-datepicker 
                            :monday-first="true" 
                            :calendar-button="true" 
                            calendar-button-icon="fa fa-calendar" 
                            :bootstrap-styling="true" 
                            format="dd-MM-yyyy" 
                            id="date" 
                            v-model="fecha_termino"></vuejs-datepicker></td>
                        <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th></td>
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Vigencia *</td>
                        <td><div v-for="answer in ['ACTIVO', 'DESACTIVADO']">
                                    <input 
                                      type="radio" 
                                      name="answer" 
                                      :id="answer"
                                      :value="answer"
                                      v-model="estado_vigencia_actividad">
                                    {{ answer }}
                                </div></th>
                          <th scope="col"></th>
                          <th scope="col"></th> 
                      </tr>
                      
                         <tr>
                          <td><button v-on:click="agregar_actividad" class="btn btn-primary">Agregar Nueva Actividad</button></td>

                        </tr>
                      </div>
                      </thead>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript"> 
Vue.component('treeselect', VueTreeselect.Treeselect);
Vue.use(VueMask.VueMaskPlugin);
var app6 = new Vue({
  el: '#app',
  data:{
    nombre_actividad:'',
    fecha: new Date(),
    fecha_termino: new Date(),
    descripcion:'',
    hh_actividad:'',
    programa_cbo:null,
    programas:[],
    maquinarias:[],
    tipo_actividades:[],
    tipo_actividades_cbo:null,
    maquinaria_cbo:null,
    area_grupo_cbo: null,
    grupos:[],
    estado_vigencia_actividad:'ACTIVO',
    solicitante_cbo:null,
    solicitantes:[],
    selected: 0,
    valueConsistsOf: 'LEAF_PRIORITY'
  },

  created: function () {
    this.obtenerAPI()
    
  },
  components: {
    vuejsDatepicker

  },
  
    methods:{

      obtenerAPI()
            {
              
                axios.get('<%=request.getContextPath()%>/forms/BuscadorMaquinarias.json')
                .then(response => this.maquinarias = response.data)
                .catch(error => {});

                axios.get('<%=request.getContextPath()%>/forms/BuscadorProgramas.json')
                .then(response => this.programas = response.data)
                .catch(error => {});

                axios.get('<%=request.getContextPath()%>/forms/BuscadorTipoActividades.json')
                .then(response => this.tipo_actividades = response.data)
                .catch(error => {});

                axios.get('<%=request.getContextPath()%>/forms/BuscadorGruposParaActividades.json')
                .then(response =>this.grupos = response.data)
                .catch(error => {});

                axios.get('<%=request.getContextPath()%>/forms/BuscadorSolicitantes.json')
                .then(response =>this.solicitantes = response.data)
                .catch(error => {});

            },

       agregar_actividad: function(){
         
         //console.log("cbo: "+this.hh_actividad);

         var expreg = new RegExp("^[0-9]+([.][0-9]+)?$");
         // expresion para solo numeros y puntos (solo 1)

         /*
         if(expreg.test(this.hh_actividad))
         {
          console.log("La matrícula es correcta");
          }
          else
          {console.log("La matrícula NO es correcta");
            }
         */   
         var validacion=true;
         var fecha_now =new Date();
         var fecha_vali = new Date(this.fecha);
         var fecha_termino_vali = new Date(this.fecha_termino);
              //console.log("fecha :"+fecha_vali);
              //console.log("fecha_termino :"+fecha_termino_vali);

         var AnyoHoy = fecha_now.getFullYear();
         var MesHoy = fecha_now.getMonth();
         var DiaHoy = fecha_now.getDate();
             
         var AnyoFecha = fecha_vali.getFullYear();
         var MesFecha = fecha_vali.getMonth();
         var DiaFecha = fecha_vali.getDate();
              
         var AnyoFecha_termino = fecha_termino_vali.getFullYear();
         var MesFecha_termino = fecha_termino_vali.getMonth();
         var DiaFecha_termino = fecha_termino_vali.getDate();


          if(this.nombre_actividad.lenght > 0 || this.nombre_actividad !='')
          { 

              if(this.programa_cbo != null )
              {

                if(this.tipo_actividades_cbo != null)
                {

                 if(this.solicitante_cbo != null)
                  { 

                    if(this.descripcion.lenght > 0 || this.descripcion !='')
                    {

                      if(this.hh_actividad != '' || this.hh_actividad > 0 )
                      {

                        if(expreg.test(this.hh_actividad))
                        {

                        if(this.maquinaria_cbo != null)
                        {

                          if(this.area_grupo_cbo != null)
                          {

                          if(AnyoFecha_termino == AnyoHoy && MesFecha_termino == MesHoy && DiaFecha_termino == DiaHoy)
                            {
                                alert("La Fecha De termino no puede ser el dia de Hoy");
                                validacion=false;

                            }
                            else
                                { 
                                    if(AnyoFecha_termino == AnyoFecha && MesFecha_termino == MesFecha && DiaFecha_termino == DiaFecha)
                                    {
                                        alert("La Fecha De termino no puede ser igual a la Inicial");
                                        validacion=false;
                                    }
                                    else
                                    {
                                        if(AnyoFecha_termino <= AnyoFecha && MesFecha_termino <= MesFecha &&  DiaFecha_termino <=  DiaFecha)
                                        {
                                          alert("La Fecha De termino no puede ser menor a la fecha Inicial");
                                          validacion=false;  
                                        }
                                        else
                                        { 
                                          if(AnyoFecha_termino < AnyoFecha)
                                            {
                                                alert("El año de la Fecha De termino no puede ser menor al Inicial");
                                                validacion=false;
                                            }
                                            else
                                            { 
                                                if(AnyoFecha_termino < AnyoFecha && MesFecha_termino < MesFecha)
                                                {

                                                  alert("El mes de termino no puede ser menor al Inicial");
                                                  validacion=false;
                                                }
                                                else
                                                {
                                                    if(MesFecha_termino < MesFecha && DiaFecha_termino <  DiaFecha)
                                                    {
                                                        alert("La Fecha De termino no puede ser menor a la fecha Inicial");
                                                        validacion=false;
                                                    }
                                                    else
                                                    {
                                                      validacion=true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                              
                              }
                              else
                              {
                                  alert("Debe seleccionar almenos 1 Grupo");
                                  validacion=false;
                              }
                          }
                          else
                          {
                            alert("Debe seleccionar almenos 1 Equipo");
                            validacion=false;
                          }

                       }
                       else
                       {
                          alert("Las HH debe ser valor numerico o decimales");
                          this.$refs.hh_actividad.focus();
                          validacion=false;
                       } 

                    }
                    else
                    {
                        alert("Debe agregar la cantidad de horas a designar (no puede ser 0)");
                        this.$refs.hh_actividad.focus();
                        validacion=false;
                    }
                }
                else
                {
                    alert("Debe agregar una descripcion a la actividad");
                    this.$refs.descripcion.focus();
                    validacion=false;
                }
              } 
              else
              {
                 alert("Debe seleccionar un solicitante para la actividad");
                    validacion=false;
              } 

              }
              
              else
              {
                alert("Debe seleccionar el tipo de actividad");
                validacion=false;
              }
            }
            else
            {
              alert("Debe seleccionar el programa");
              validacion=false;
            }
          }
        else
        {
          alert("Debe ingresar un nombre para asignarle a la Actividad");
          this.$refs.nombre_actividad.focus();
          validacion=false;         
        }

         console.log("validacion: "+validacion);
       
        if(validacion)
        {
            axios.get('<%=request.getContextPath()%>/forms/insertNewActividad.json?nombre_actividad='+this.nombre_actividad+'&programa='+this.programa_cbo+'&descripcion='+this.descripcion+'&hh_actividad='+this.hh_actividad+'&id_equipos='+this.maquinaria_cbo+'&validez='+this.estado_vigencia_actividad+'&grupos='+this.area_grupo_cbo+'&fecha_inicio='+this.fecha+'&fecha_termino='+this.fecha_termino+'&tipo_actividades='+this.tipo_actividades_cbo+'&solicitante='+this.solicitante_cbo)
                                    .then(function(response){

                                    alert("Se creo la nueva Actividad");
                                    window.location = "<%=request.getContextPath()%>/forms/formActividades.html";
                                  })
                                  .catch(function(){
                                    alert("Problemas al ingresar la Actividad");

                                  });
        }

        
      }

    }
  })

</script>


