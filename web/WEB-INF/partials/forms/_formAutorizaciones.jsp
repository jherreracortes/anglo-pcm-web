<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core'      prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt'       prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script src="https://unpkg.com/vue@latest"></script>
<script src="https://unpkg.com/vue-select@latest"></script>
<script src="https://unpkg.com/lodash@latest/lodash.min.js"></script>

<style>
img {
  height: auto;
  max-width: 2.5rem;
  margin-right: 1rem;
}

.d-center {
  display: flex;
  align-items: center;
}

.selected img {
  width: auto;
  max-height: 23px;
  margin-right: 0.5rem;
}

.v-select .dropdown li {
  border-bottom: 1px solid rgba(112, 128, 144, 0.1);
}

.v-select .dropdown li:last-child {
  border-bottom: none;
}

.v-select .dropdown li a {
  padding: 10px 20px;
  width: 100%;
  font-size: 1.25em;
  color: #3c3c3c;
}

.v-select .dropdown-menu .active > a {
  color: #fff;
}
</style>

<div id="app">
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                      <h3 class="card-title mb-0" style="padding-left: 20px;">Sistema Gesti&oacute;n Competencias - Agregar Autorizacion</h3>
                  </div>
                    </div>
                    <div class="table-responsive-md">
                    <table class="table"> 
                      <tbody>
                       <tr>
                          <td style="padding-left: 20px;padding-top: 20px;width: 400px;padding-bottom: 20px;">Rut</td> 
                          <td colspan="2" style="padding-right: 120px;padding-left: 12px;">
                            <v-select v-model="rut_cbo" label="rut" :filterable="false" :options="options" @search="onSearch">
                              <template slot="no-options">
                                Ingrese el rut a realizar la busqueda..
                              </template>
                              <template slot="option" slot-scope="option">
                                <div class="d-center">
                                  {{ option.rut }}
                                  </div>
                              </template>
                              <template slot="selected-option" slot-scope="option">
                                <div class="selected d-center">
                                  {{ option.rut }}
                                </div>
                              </template>
                              </v-select>
                            </td>
                            <th scope="col"></th>
                          <th scope="col"></th> 
                        </tr>
                      <tbody v-if="message_nombre != 'NULL'">
                      <tr>
                        <td style="padding-left: 20px;">Nombre Operador</td>
                        <td>{{message_nombre}}</td>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>   
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Area</td>
                        <td>{{message_area}}</td>
                        <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th> 
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Grupo</td>
                        <td>{{message_grupo}}</td>
                        <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th> 
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Equipo *</td>
                        <td colspan="2" style="width: 450px;padding-right: 120px;">
                        <treeselect v-model="maquinaria_cbo"
                           :options="maquinarias" 
                           :disable-branch-nodes="true"
                           :show-count="true"
                           />
                        </td>
                        <th scope="col"></th> 
                        <th scope="col"></th>  
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Tipo De Autorizaci&oacute;n *</td>
                        <td><select v-model="tipo_autorizacion_cbo" required="">
                                  <option :value="null" disabled>--Seleccione la Autorizaci&oacute;n--</option>
                                  <option value="A">Autorizaci&oacute;n</option>
                                  <option value="E">Evaluaci&oacute;n</option>
                                  <option value="R">Re-Evaluaci&oacute;n</option>
                                  <option value="V">Validaci&oacute;n</option>
                                  <option value="RI">Re-Instrucci&oacute;n</option>
                                  </select></td>
                          <th scope="col">Operacion Invierno </th>                       
                          <th scope="col"><label for="si"> SI&nbsp;</label><input type="radio" id="one" value="SI" v-model="operacion_invierno">
                          </th>
                          <th scope="col"><label for="no"> NO&nbsp;</label><input type="radio" id="two" value="NO" v-model="operacion_invierno">
                          </th> 
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Fecha Autorizaci&oacute;n *</td>
                        <td><vuejs-datepicker  id="fecha" v-model="fecha" format='dd-MM-yyyy'></vuejs-datepicker></td>
                        <th scope="col">MDE *</th>                       
                          <th scope="col"><label for="si"> SI&nbsp;</label><input type="radio" id="one" value="SI" v-model="mde">
                          </th>
                          <th scope="col"><label for="no"> NO&nbsp;</label><input type="radio" id="two" value="NO" v-model="mde">
                          </th>  
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Programa </td>
                          <td colspan="2" style="width: 450px;padding-right: 120px;">
                        <treeselect v-model="programa_cbo"
                           :options="programas" 
                           :disable-branch-nodes="true"
                           :show-count="true"
                           />
                        </td>
                        <th scope="col"></th> 
                        <th scope="col"></th> 
                      </tr>
                      <tr v-if="tipo_autorizacion_cbo === 'A' || tipo_autorizacion_cbo === 'V'">
                        <td style="padding-left: 20px;">Vigencia *</td>
                        <td><select v-model="vigencia_cbo" required="">
                                  <option :value="null" disabled>--Seleccione la Vigencia--</option>
                                  <option value="1">1 Año</option>
                                  <option value="2">2 Años</option>
                                  <option value="3">3 Años</option>
                                  <option value="4">4 Años</option>
                                  <option value="5">5 Años</option>
                                  <option value="6">6 Años</option>
                                  <option value="7">7 Años</option>
                                  <option value="8">8 Años</option>
                                  <option value="9">9 Años</option>
                                  <option value="10">10 Años</option>
                                  <option value="11">mas de 10 Años</option>
                                  </select></td>
                                  <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th> 
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Autorizador</td>
                        <td><select v-model="monitores_cbo">
                                  <option :value="null" disabled>--Seleccione un Autorizador--</option>
                                  <option value="ninguno" >--Otro Monitor--</option>
                                  <option v-for="monitor in monitores" :value=monitor.rut>{{monitor.nombre}}</option> 
                                </select></td>
                                <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th> 
                      </tr>
                      <tr>
                        <td></td>
                        <td><button v-on:click="insertar" class="btn btn-primary mb-2">Guardar</button></td> 
                       </tr> 
                      </tbody>
                      <tbody v-else="rut_cbo == ''"></tbody>
                    </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript"> 

Vue.component("v-select", VueSelect.VueSelect);
Vue.component('treeselect', VueTreeselect.Treeselect);
var app6 = new Vue({
  el: '#app',
  data() {
        return {
            fecha: new Date(),
            keywords: null,
            results: [],
            fecha_autorizacion_equipo:[],
            fecha_autorizacion:new Date(),
            message_nombre : 'NULL',
            message_rut:'NULL',
            message_area:'NULL',
            message_grupo:'NULL',
            rut_cbo : '',
            operadores:[],
            maquinarias:[],
            programas:[],
            monitores:[],
            selected: 0,
            options: [],
            maquinaria_cbo:null,
            programa_cbo:null,
            monitores_cbo:null,
            vigencia_cbo:null,
            programa_cbo:null,
            tipo_autorizacion_cbo:null,
            operacion_invierno:'NO',
            mde:'NO',
            area:'',
            grupo:''
        };
    },
    watch: {
        'rut_cbo': function(val, oldVal){

          if(this.rut_cbo != null)
          {
              this.message_rut=JSON.stringify(val.rut).replace(/"/g, "");
              this.message_nombre=JSON.stringify(val.nombre).replace(/"/g, "");
              this.message_area=JSON.stringify(val.area).replace(/"/g, "");
              this.message_grupo=JSON.stringify(val.grupo).replace(/"/g, "");
          }
          else
          {
             this.message_nombre='NULL';
          }    
            
        },

        'maquinaria_cbo': function(val) {

      if(this.maquinaria_cbo != null && this.tipo_autorizacion_cbo == 'A' )
       {
         console.log(this.maquinaria_cbo);
           axios.get('<%=request.getContextPath()%>/forms/validacion_autorizacion_equipos.json?rut='+this.message_rut+'&equipo='+val)
                .then(response => {
                  //console.log(JSON.stringify(response.data));
                   //console.log(JSON.stringify(response.data[0].fecha_termino).replace(/"/g, ""));
                    if(JSON.stringify(response.data) !='[]')
                    {
                      
                     this.fecha=JSON.stringify(response.data[0].fecha_termino).replace(/"/g, "");
                     this.fecha_autorizacion=JSON.stringify(response.data[0].fecha_termino).replace(/"/g, "");
                     
                    }
                    else
                    { 
                      if(JSON.stringify(response.data) == '[]')
                      {
                        this.fecha=new Date();
                        this.fecha_autorizacion=null;
                        
                      }
                    }

                    } )
                .catch(error => {});
                

       }
       else
       {
          this.fecha=new Date();
          this.fecha_autorizacion=null;
          this.vigencia_cbo=null;

       }     
      },
      'tipo_autorizacion_cbo': function(val) {
      
      if(this.maquinaria_cbo != null && this.tipo_autorizacion_cbo == 'A')
       {
         console.log(this.maquinaria_cbo);
           axios.get('<%=request.getContextPath()%>/forms/validacion_autorizacion_equipos.json?rut='+this.message_rut+'&equipo='+this.maquinaria_cbo)
                .then(response => {

                    if(JSON.stringify(response.data) !='[]')
                    {
                      
                     this.fecha=JSON.stringify(response.data[0].fecha_termino).replace(/"/g, "");
                     this.fecha_autorizacion=JSON.stringify(response.data[0].fecha_termino).replace(/"/g, "");
                     
                    }
                    else
                    { 
                      if(JSON.stringify(response.data) == '[]')
                      {
                        this.fecha=new Date();
                        this.fecha_autorizacion=null;
                        
                      }
                    }

                    } )
                .catch(error => {});
                

       }
       else
       {
          this.fecha=new Date();
          this.fecha_autorizacion=null;
          this.vigencia_cbo=null;
       }     
      }
      },
  components: {
    vuejsDatepicker

  },

    methods:{

      onSearch: function onSearch(search, loading) {
      loading(true);
      this.search(loading, search, this);
    },
    search: _.debounce(function (loading, search, vm) {
      this.fetch(loading, search, vm);
      this.buscar();
    }, 350),

      fetch(loading, search, vm) {
            
            axios.get('<%=request.getContextPath()%>/forms/BuscadorOperadores.json?fecha='+escape(search))
                .then(response => vm.options = response.data)
                .catch(error => {});
                loading(false);
        },
            buscar()
            {
              
                axios.get('<%=request.getContextPath()%>/forms/BuscadorMaquinarias.json')
                .then(response => this.maquinarias = response.data)
                .catch(error => {});

                axios.get('<%=request.getContextPath()%>/forms/BuscadorMonitores.json')
                .then(response => this.monitores = response.data)
                .catch(error => {});

                axios.get('<%=request.getContextPath()%>/forms/BuscadorProgramas.json')
                .then(response => this.programas = response.data)
                .catch(error => {});

            },
       insertar: function(){
        var validacion=true;
        //console.log("fecha autorizacion :"+formatDate(this.fecha_autorizacion));
        //console.log("fecha :"+formatDate(this.fecha));
        
        if(this.fecha_autorizacion != null)
            {
              var fecha_auto_vali = new Date(this.fecha_autorizacion);
              var fecha_datepiker_vali = new Date(this.fecha);
              //console.log("fecha_auto_vali :"+fecha_auto_vali);
              //console.log("fecha_datepiker_vali :"+fecha_datepiker_vali);
              var AnyoFecha_auto = fecha_auto_vali.getFullYear();
              var MesFecha_auto = fecha_auto_vali.getMonth();
              var DiaFecha_auto = fecha_auto_vali.getDate();
              
              var AnyoFecha_ingreso = fecha_datepiker_vali.getFullYear();
              var MesFecha_ingreso = fecha_datepiker_vali.getMonth();
              var DiaFecha_ingreso = fecha_datepiker_vali.getDate();
          }
       
          /*
        console.log("fecha_auto_vali :"+AnyoFecha_auto+" mes "+MesFecha_auto+" dia "+DiaFecha_auto);
        console.log("fecha_datepiker_vali :"+AnyoFecha_ingreso+" mes "+MesFecha_ingreso+" dia "+DiaFecha_ingreso);
        console.log("feccha vali funct: "+formatDate(fecha_auto_vali));
        console.log("fecha_datepiker_vali funct: "+formatDate(fecha_datepiker_vali));
       */
        function formatDate(date) {
                    var d = new Date(date),
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();

                    if (month.length < 2) month = '0' + month;
                    if (day.length < 2) day = '0' + day;

                    return [year, month, day].join('-');
                    }  
        
        if (this.maquinaria_cbo == "null" || this.maquinaria_cbo == null ) 
        {
          alert("Debe Seleccionar un Equipo");
          validacion=false;

        }
        else
        {
          
           if(this.tipo_autorizacion_cbo == "null" || this.tipo_autorizacion_cbo == null )
            {
              alert("Debe Seleccionar El tipo de Autorización");
              validacion=false;
            }
            else
            {
              if(this.tipo_autorizacion_cbo == "A")
              {
                if(this.vigencia_cbo == "null" || this.vigencia_cbo == null )
                  {
                    alert("Debe Seleccionar la Vigencia");
                    validacion=false;
                  }
                  else
                  {
                    if(this.fecha_autorizacion != null)
                    {
                      if (AnyoFecha_ingreso < AnyoFecha_auto)
                      {
                            alert("La fecha de autorizacion debe ser igual o mayor a la que sugiere :"+formatDate(this.fecha_autorizacion));
                              validacion=false; 
                      }
                      else
                        {
                            if (AnyoFecha_ingreso == AnyoFecha_auto && MesFecha_ingreso < MesFecha_auto){
                                alert("La fecha de autorizacion debe ser igual o mayor a la que sugiere :"+formatDate(this.fecha_autorizacion));
                                  validacion=false;           
                            }
                            else{
                                if (AnyoFecha_ingreso == AnyoFecha_auto && MesFecha_ingreso == MesFecha_auto && DiaFecha_ingreso < DiaFecha_auto){
                                    alert("La fecha de autorizacion debe ser igual o mayor a la que sugiere :"+formatDate(this.fecha_autorizacion));
                                      validacion=false; 
                                }
                                else{
                                    if (AnyoFecha_ingreso == AnyoFecha_auto && MesFecha_ingreso == MesFecha_auto && DiaFecha_ingreso == DiaFecha_auto){
                                         //alert ("Has introducido la fecha de Hoy");
                                    }
                                    else{
                                        //alert ("La fecha introducida es posterior a Hoy");
                                    }
                                }
                            }
                        }
                    }
                  }
              }

            }
          } 

          if(validacion)
          {
              if(this.programa_cbo == "null" || this.programa_cbo == null )
              {
                this.programa_cbo=1;
              }

           
            axios.get('<%=request.getContextPath()%>/forms/insertAutorizacion.json?fecha='+formatDate(this.fecha)+"&operador="+this.message_rut+"&maquinaria="+this.maquinaria_cbo+"&monitor="+this.monitores_cbo+"&vigencia="+this.vigencia_cbo+"&tipo_autorizacion="+this.tipo_autorizacion_cbo+"&ope_invierno="+this.operacion_invierno+"&programa="+this.programa_cbo+"&mde="+this.mde)
                .then(function(response){
                  //console.log(response);
                alert("Se ingresa la autorizacion");
                window.location = "<%=request.getContextPath()%>/forms/formAutorizaciones.html";
              })
              .catch(function(){
                alert("Problemas al Ingresar la autorizacion");

              });
             
           } 
           
        
      }
  }
  })

</script>


