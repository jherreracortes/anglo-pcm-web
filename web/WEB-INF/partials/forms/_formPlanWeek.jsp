<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core'      prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt'       prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div id="app">
    <div class="table-responsive-md">
      <h4 class="card-title mb-0">Formulario Plan Semanal</h4>  
        <table class="table">  
            <thead>
                <tr> 
                    <th scope="col">
                        <label for="files">Upload a CSV formatted file:</label>
                    </th>
                    <th scope="col"> 
                        <input type="file" id="file" ref="file" accept=".csv" v-on:change="handleFileUpload()"/>
                    </th>
                </tr>
            </thead>
            <thead>
                <tr> 
                    <th scope="col"></th>
                    <th scope="col">
                        <button v-on:click="submitFile()" class="btn btn-primary mb-2" >GUARDAR</button>
                    </th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script type="text/javascript"> 
new Vue({
  el: '#app',
  methods: {
    submitFile(){

            let formData = new FormData();
            const file = this.$refs.file.files[0];
            if (!file) 
            {
        
              alert('Debe cargar un archivo csv');
              return;
            }


            if (file.size > 1024 * 2048) 
            {
        
              alert("El archivo "+file.name+" no puede ser mayor a 20MB");
              return;
            }

            formData.append('file', this.file);
            
            axios.post( '<%=request.getContextPath()%>/forms/upload_csv',
                formData,
                {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
              }
            ).then(function(response){
          alert("Se cargo correctamente el archivo "+file.name+" para ser procesado");
        })
        .catch(function(){
          //console.log('FAILURE!!');
          alert("Problemas al cargar el archivo "+file.name+" validar separadores y que no este en blanco")

        });
      },
      handleFileUpload(){
        this.file = this.$refs.file.files[0];

        
      }
  },
});

</script>