<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core'      prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt'       prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div id="app">
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                      <h3 class="card-title mb-0" style="padding-left: 20px;">Sistema Gesti&oacute;n Competencias - Edicion Monitor</h3>
                        <div class="col-sm-4">
                        </div>
                        </div>
                    </div>
                    <div>
                    <div class="table-responsive-md">
                      <table class="table" v-for="programa in programas"> 
                      <tr> 
                          <th scope="col" style="padding-left: 20px;padding-top: 22px;padding-bottom: 0px;">Area - Grupo</th>
                          <th scope="col" colspan="2" style="width: 450px;">
                           <treeselect v-model="area_grupo_cbo"
                           :options="grupo" 
                           :disable-branch-nodes="true"
                           :show-count="true"
                           /></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>
                        <tr> 
                          <th scope="col" style="padding-left: 20px;">Nombre</th>
                          <th scope="col"><input type="text" v-model="programa.nombre" placeholder="Ingrese Su Nombre" disabled ></th>
                          <th scope="col">Apellido Paterno</th>
                          <th scope="col"><input type="text" v-model="programa.apellido_paterno" placeholder="Ingrese Su Apellido Paterno" disabled ></th>
                          <th scope="col">Apellido Materno</th>
                          <th scope="col"><input type="text" v-model="programa.apellido_materno"  placeholder="Ingrese Su Apellido Materno" disabled ></th>
                        </tr>
                        <tr> 
                          <th scope="col" style="padding-left: 20px;">SAP</th>
                          <th scope="col"><input type="text" v-model="programa.sap"  v-mask="'###########'"  placeholder="Ingrese El SAP" disabled></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>
                        <tr>  
                          <th scope="col" style="padding-left: 20px;">Estado Vigencia</th>
                          <th scope="col"><input type="text" v-model="programa.estado_vigencia"  disabled >
                         </th>
                         <th scope="col"></th>
                         <th scope="col"></th>
                         <th scope="col"></th>
                         <th scope="col"></th> 
                        </tr>
                        <tr> 
                          <th scope="col" style="padding-left: 20px;">Empleado Por</th>
                          <th scope="col">
                            <input type="text" v-model="programa.tipo_empleado"  disabled >
                        </th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th> 
                        </tr>


                        
                      <!--<tr>  
                          <th scope="col" style="padding-left: 20px;">Cargo</th>
                          <th scope="col" colspan="2"><input type="text" v-model="cargo" ref="cargo" placeholder="Ingrese Su Cargo" size="35" disabled ></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>
                        <tr> 
                          <th scope="col" style="padding-left: 20px;padding-top: 20px">Cargo</th>
                          <th scope="col" colspan="2" style="width: 450px;">
                           <treeselect v-model="programa.cargo_grupo_cbo"
                           :options="cargo_grupo" 
                           :disable-branch-nodes="true"
                           :show-count="true"
                           disabled
                           /></th> 
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>-->
                        <!--<tr>  
                          <th scope="col" style="padding-left: 20px;">Superintendencia</th>
                          <th scope="col" colspan="2"><input type="text" v-model="superintendencia" ref="superintendencia" placeholder="Ingrese La Superintendencia" size="35" disabled></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>
                        <tr> 
                          <th scope="col" style="padding-left: 20px;padding-top: 20px">Superintendencia</th>
                          <th scope="col" colspan="2" style="width: 450px;">
                           <treeselect v-model="programa.superintendencia_grupo_cbo"
                           :options="superintendencia_grupo" 
                           :disable-branch-nodes="true"
                           :show-count="true"
                           disabled
                           /></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>
                        <tr>  
                          <th scope="col" style="padding-left: 20px;">Tramo</th>
                          <th scope="col" colspan="2"><input type="text" v-model="programa.tramo"  placeholder="Ingrese Su Tramo" size="35" disabled ></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr> -->
                        
                         <tr> 
                          <td style="padding-left: 20px;"><button v-on:click="modificar" class="btn btn-primary">Modificar Monitor</button></td>
                        </tr>
                      </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript"> 
Vue.component('treeselect', VueTreeselect.Treeselect);
var app6 = new Vue({
  el: '#app',
   mounted:function(){
     this.obtenerAPI()
   },
  data:{
    area_grupo_cbo: null,
    grupo:[],
    programas:[{
    nombre:'',
    apellido_paterno:'',
    apellido_materno:'',
    rut:'',
    sap:'',
    estado_vigencia:'',
    tipo_empleado:'',
    }],
    
    form : {}
  },
    methods:{
      obtenerAPI ()
       {

                var prodId = getParameterByName('rut');
                     

                     function getParameterByName(name) 
                     {
                        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                        results = regex.exec(location.search);
                        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
                      }

                      axios.get('<%=request.getContextPath()%>/forms/MonitorEdit.json?rut='+prodId)
                .then(response => {

                    this.programas = response.data;
                    console.log(JSON.stringify(response.data[0].area_grupo).replace(/"/g, ""));
                    this.area_grupo_cbo=JSON.stringify(response.data[0].area_grupo).replace(/"/g, "");

                    })
                    .catch(function (error) {
                    });

                    axios.get('<%=request.getContextPath()%>/forms/BuscadorGrupos.json')
                .then(response => {
                    this.grupo = response.data;
                    this.area_grupo_cbo=this.area_grupo_cbo;
                    } )
                .catch(error => {});

            
            },

       modificar: function(){


               var prodId = getParameterByName('rut');
                     

                     function getParameterByName(name) 
                     {
                        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                        results = regex.exec(location.search);
                        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
                      }
              
              axios.get('<%=request.getContextPath()%>/forms/insertModMonitor.json?rut_operador='+prodId+'&area_grupo='+this.area_grupo_cbo)
                    .then(function(response){
                    alert("Se Modifica el Monitor");
                    window.location = "<%=request.getContextPath()%>/forms/formMonitor.html";
                  })
                  .catch(function(){
                    alert("Problemas al Modificar el Monitor");

                  });
                 
            
          }
          }
        })

</script>


