<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core'      prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt'       prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@riophae/vue-treeselect@0.0.38/dist/vue-treeselect.min.css">
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/v-mask/dist/v-mask.min.js"></script>
<div id="app">
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                      <h3 class="card-title mb-0" style="padding-left: 20px;">Sistema Gesti&oacute;n Competencias - Nuevo Operador </h3>
                        <div class="col-sm-4">
                        </div>
                        </div>
                    </div>
                    <div>
                    <div class="table-responsive-md">
                    <table class="table">  
                    <thead >
                      <div>
                        <tr> 
                          <th scope="col" style="padding-left: 20px;">Rut</th>
                          <th scope="col"><input type="text" v-model="rut" ref="rut" autocomplete="off" placeholder="Ingrese RUT" ><!--<input type="text" v-model="rut" oninput="checkRut(this)" placeholder="Ingrese RUT" >--></th>
                          
                          <th scope="col">
                            
                          <div v-if="oldValue_rut == 'Rut en BD'">
                            <span title="El Rut ya existe"> <img class="img-avatar"  src="<%=request.getContextPath()%>/img/avatars/warning.png" ></span>
                          </div>
                         <div v-else-if="validador_inputs == 1">
                            <span :title=oldValue_rut> <img class="img-avatar"  src="<%=request.getContextPath()%>/img/avatars/rut_valido.png" ></span>
                          </div>
                          <div v-else-if="oldValue_rut == 'Rut Invalido'">
                            <span :title=oldValue_rut> <img class="img-avatar"  src="<%=request.getContextPath()%>/img/avatars/rut_invalido.png" ></span>
                          </div>
                        </th>
                          <th scope="col"><input type="hidden" v-model="oldValue_rut"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>

                        <tr v-if="validador_inputs ==1"> 
                          <th scope="col" style="padding-left: 20px;">Area - Grupo</th>
                          <th scope="col" colspan="2" style="width: 450px;">
                           <treeselect v-model="area_grupo_cbo"
                           :options="grupo" 
                           :disable-branch-nodes="true"
                           :show-count="true"
                           /></th> 
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>
                        <tr v-if="validador_inputs == 1"> 
                          <th scope="col" style="padding-left: 20px;">Nombre</th>
                          <th scope="col"><input type="text" v-model="nombre" ref="nombre" placeholder="Ingrese Su Nombre" ></th>
                          <th scope="col">Apellido Paterno</th>
                          <th scope="col"><input type="text" v-model="apellido_paterno" ref="apellido_paterno" placeholder="Ingrese Su Apellido Paterno" ></th>
                          <th scope="col">Apellido Materno</th>
                          <th scope="col"><input type="text" v-model="apellido_materno" ref="apellido_materno" placeholder="Ingrese Su Apellido Materno" ></th>
                        </tr>
                        <tr v-if="validador_inputs ==1"> 
                          <th scope="col" style="padding-left: 20px;">SAP</th>
                          <th scope="col"><input type="text" v-model="sap" ref="sap" v-mask="'###########'"  placeholder="Ingrese El SAP"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>
                        <tr v-if="validador_inputs ==1">  
                          <th scope="col" style="padding-left: 20px;">Genero</th>
                          <th scope="col">
                          <div v-for="genero in ['Masculino', 'Femenino']">
                                    <input 
                                      type="radio" 
                                      name="genero" 
                                      :id="genero"
                                      :value="genero"
                                      v-model="genero_ope">
                                    {{ genero }}
                         </div>
                         </th>
                         <th scope="col"></th>
                         <th scope="col"></th>
                         <th scope="col"></th>
                         <th scope="col"></th> 
                        </tr>
                        <tr v-if="validador_inputs ==1">  
                          <th scope="col" style="padding-left: 20px;">Estado Vigencia</th>
                          <th scope="col">
                          <div v-for="answer in ['VIGENTE', 'FINIQUITADO']">
                                    <input 
                                      type="radio" 
                                      name="answer" 
                                      :id="answer"
                                      :value="answer"
                                      v-model="estado_vigencia_ope">
                                    {{ answer }}
                         </div>
                         </th>
                         <th scope="col"></th>
                         <th scope="col"></th>
                         <th scope="col"></th>
                         <th scope="col"></th> 
                        </tr>
                        <tr v-if="validador_inputs == 1"> 
                          <th scope="col" style="padding-left: 20px;">Empleado Por</th>
                          <th scope="col">
                          <div v-for="emp_ope in ['ANGLO', 'CONTRATISTA']">
                                    <input 
                                      type="radio" 
                                      name="emp_ope" 
                                      :id="emp_ope"
                                      :value="emp_ope"
                                      v-model="estado_empleado_ope">
                                    {{ emp_ope }}
                          </div>
                        </th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th> 
                        </tr>
                        <tr v-if="validador_inputs == 1"> 
                        <th scope="col" style="padding-left: 20px;">Fecha Nacimiento</th>
                        <th scope="col"><vuejs-datepicker v-model="fecha_nacimiento" format='dd-MM-yyyy'></vuejs-datepicker></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                      </tr>
                      <tr v-if="validador_inputs == 1">  
                          <th scope="col" style="padding-left: 20px;padding-top: 20px">Cargo</th>
                          <th scope="col" colspan="2" style="width: 450px;">
                           <treeselect v-model="cargo_grupo_cbo"
                           :options="cargo_grupo" 
                           :disable-branch-nodes="true"
                           :show-count="true"
                           /></th> 
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>
                        <tr v-if="validador_inputs == 1">  
                          <th scope="col" style="padding-left: 20px;padding-top: 20px">Superintendencia</th>
                          <th scope="col" colspan="2" style="width: 450px;">
                           <treeselect v-model="superintendencia_grupo_cbo"
                           :options="superintendencia_grupo" 
                           :disable-branch-nodes="true"
                           :show-count="true"
                           /></th> 
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>
                        <tr v-if="validador_inputs == 1">  
                          <th scope="col" style="padding-left: 20px;padding-top: 20px">Tramo</th>
                          <th scope="col" colspan="2" style="width: 450px;">
                           <treeselect v-model="tramo"
                           :options="tramos" 
                           :disable-branch-nodes="true"
                           :show-count="true"
                           /></th> 
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>
                        <tr v-if="validador_inputs == 1"> 
                        <th scope="col" style="padding-left: 20px;">Fecha Ingreso</th>
                        <th scope="col"><vuejs-datepicker v-model="fecha_ingreso" format='dd-MM-yyyy'></vuejs-datepicker></th>
                        <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>  
                      </tr>
                         <tr v-if="validador_inputs == 1"> 
                          <td style="padding-left: 20px;"><button v-on:click="agregar_operador" class="btn btn-primary">Agregar Nuevo Operador</button></td>
                        </tr>
                      </thead>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript"> 
  Vue.component('treeselect', VueTreeselect.Treeselect);
  Vue.use(VueMask.VueMaskPlugin);

var app6 = new Vue({
  el: '#app',
   mounted:function(){
     this.obtenerAPI()
   },
  data:{
    area_grupo_cbo: null,
    cargo_grupo:[],
    superintendencia_grupo_cbo:null,
    superintendencia_grupo:[],
    cargo_grupo_cbo:null,
    grupo:[],
    validacion_rut:[],
    nombre:'',
    apellido_paterno:'',
    apellido_materno:'',
    input_rut:'',
    rut:'',
    sap:'',
    oldValue_rut: '',
    genero_ope:'',
    estado_vigencia_ope:'VIGENTE',
    estado_empleado_ope:'ANGLO',
    fecha_nacimiento:new Date(),
    tramo:null,
    tramos:[],
    fecha_ingreso:new Date(),
    estado_vigencia:'',
    estado_vigencia_rdb:'VIGENTE',
    validador_inputs:0,
    selected: 0,
    grupo_cbo: 0,
    validacion_rut_new:''
  },
  components: {
    vuejsDatepicker
  },
  watch: {
    rut: function(val, oldVal) {
       this.oldValue_rut=VerificaRut(val);
       if(this.oldValue_rut=='Rut Valido')
       {
        if (val.charAt(0)==0)
        {
          rutificador=val;
          rut_valido_0 = rutificador.substring(1);
        }
        else
        {
          rut_valido_0 = val;
        }
       axios.get('<%=request.getContextPath()%>/forms/validacion_rut_autorizacion.json?rut='+rut_valido_0)
                .then(response =>  
                {
                    this.validacion_rut = response.data;
                    console.log("validacion rut: "+JSON.stringify(this.validacion_rut));
                    if(JSON.stringify(this.validacion_rut) != '[]')
                    {
                      this.oldValue_rut="Rut en BD";
                      
                    }else
                    {

                      this.validador_inputs=1;
                      this.$refs.rut.disabled;
                    }
                   
                })
                .catch(error => {});
      }
      else
      {
          this.oldValue_rut='Rut Invalido';
          this.validador_inputs=0;
      }        
      function VerificaRut(rut) 
      {
        if (rut.toString().trim() != '' && rut.toString().indexOf('-') > 0) {
            var caracteres = new Array();
            var serie = new Array(2, 3, 4, 5, 6, 7);
            var dig = rut.toString().substr(rut.toString().length - 1, 1);
            rut = rut.toString().substr(0, rut.toString().length - 2);

            for (var i = 0; i < rut.length; i++) {
                caracteres[i] = parseInt(rut.charAt((rut.length - (i + 1))));
            }

            var sumatoria = 0;
            var k = 0;
            var resto = 0;

            for (var j = 0; j < caracteres.length; j++) {
                if (k == 6) {
                    k = 0;
                }
                sumatoria += parseInt(caracteres[j]) * parseInt(serie[k]);
                k++;
            }

            resto = sumatoria % 11;
            dv = 11 - resto;

            if (dv == 10) {
                dv = "K";
            }
            else if (dv == 11) {
                dv = 0;
            }

            if (dv.toString().trim().toUpperCase() == dig.toString().trim().toUpperCase())
             {
                return this.oldValue_rut='Rut Valido';
            }else
             { 
              return this.oldValue_rut='Rut Invalido'; 
            }
        }
        else {
          return this.oldValue_rut='Rut Invalido';
        }
    }  
}
  },
    methods:{
      obtenerAPI ()
       {

                axios.get('<%=request.getContextPath()%>/forms/BuscadorGrupos.json')
                .then(response => {
                    this.grupo = response.data;
                    } )
                .catch(error => {});

                axios.get('<%=request.getContextPath()%>/forms/CargosCBO.json')
                .then(response => {
                    this.cargo_grupo = response.data;
                    } )
                .catch(error => {});

                axios.get('<%=request.getContextPath()%>/forms/SuperintendenciaCBO.json')
                .then(response => {
                    this.superintendencia_grupo = response.data;
                    } )
                .catch(error => {});

                axios.get('<%=request.getContextPath()%>/forms/TramoCBO.json')
                .then(response => {
                    this.tramos = response.data;
                    } )
                .catch(error => {});
       },

        buscar_rut: function(){


              //window.location = "<%=request.getContextPath()%>/forms/formNewEquipo.html";

          },

       agregar_operador: function(){

        var validacion_form=false;
        var fecha_nac_vali = new Date(this.fecha_nacimiento);
        var fecha_ingre_vali = new Date(this.fecha_ingreso);
        var fecha_now =new Date();
        var AnyoFecha_nac = fecha_nac_vali.getFullYear();
        var MesFecha_nac = fecha_nac_vali.getMonth();
        var DiaFecha_nac = fecha_nac_vali.getDate();

        var AnyoFecha_ingreso = fecha_ingre_vali.getFullYear();
        var MesFecha_ingreso = fecha_ingre_vali.getMonth();
        var DiaFecha_ingreso = fecha_ingre_vali.getDate();
 
        var AnyoHoy = fecha_now.getFullYear();
        var MesHoy = fecha_now.getMonth();
        var DiaHoy = fecha_now.getDate();

        if(this.oldValue_rut == 'Rut Valido')
        {
            
            if(this.area_grupo_cbo != null)
              {
                  if(this.nombre != '' || this.nombre.length > 0)
                    {

                       if(this.apellido_paterno != '' || this.apellido_paterno.length > 0)
                          {
                             if(this.apellido_materno != '' || this.apellido_materno.length > 0)
                                {
                                   if(this.sap != '' || this.sap.length > 0)
                                    {
                                      if(this.genero_ope !='')
                                      {
                                       if(AnyoFecha_nac == AnyoHoy && MesFecha_nac == MesHoy && DiaFecha_nac == DiaHoy)
                                          {
                                            alert("La Fecha De Nacimiento no puede ser el dia de Hoy");
                                            validacion_form=false;

                                          }
                                          else
                                          {
                                            if(AnyoFecha_ingreso == AnyoFecha_nac && MesFecha_nac == MesFecha_ingreso && DiaFecha_nac == DiaFecha_ingreso)
                                            {
                                              alert("La Fecha De Ingreso no puede ser igual a la Fecha de Nacimiento");
                                              validacion_form=false;
                                            }
                                            else
                                            {
                                               if(this.cargo_grupo_cbo != null)
                                               {
                                                    if(this.superintendencia_grupo_cbo != null)
                                                       {
                                                          if(this.tramo != null)
                                                               {
                                                                  validacion_form=true;
                                                               }
                                                               else
                                                               {
                                                                 alert("Debe Ingresar un Tramo al Operador");
                                                                 validacion_form=false;
                                                               }
                                                       }
                                                       else
                                                       {
                                                         alert("Debe Ingresar una Superintendencia al Operador");
                                                         validacion_form=false;
                                                        
                                                       }
                                               }
                                               else
                                               {
                                                 alert("Debe Ingresar un Cargo al Operador");
                                                 validacion_form=false;
                                                
                                               }

                                            }

                                          }
                                        }
                                        else
                                        {
                                            alert("Debe seleccionar el Genero del Operador");
                                                 validacion_form=false;
                                        }   
                                    }
                                    else
                                    {
                                      alert("Debe Ingresar El SAP para el Operador");
                                      validacion_form=false;
                                      this.$refs.sap.focus();
                                    }

                                }
                                else
                                {
                                  alert("Debe Ingresar Apellido Materno para el Operador");
                                  validacion_form=false;
                                  this.$refs.apellido_materno.focus();
                                }
                          }
                          else
                          {
                            alert("Debe Ingresar Apellido Paterno para el Operador");
                            validacion_form=false;
                            this.$refs.apellido_paterno.focus();
                          }
                    }
                    else
                    {
                      alert("Debe Ingresar un Nombre para el Operador");
                      validacion_form=false;
                      this.$refs.nombre.focus();
                    }
              }
              else
              {
                alert("Debe seleccionar el area y grupo al que pertenecera el Operador");
                validacion_form=false;
              }
        }
        else
        {
          alert("Debe ingresar un rut valido");
          validacion_form=false;
          this.$refs.rut.focus();
        }
        
        if(validacion_form)
          {
              //console.log("todos los datos completos");
            //para eliminar el 0 de la izquierda si llegan a ingresarlo  
            if (this.rut.charAt(0)==0)
            {
                rutificador=this.rut;
                rut_valido_0 = rutificador.substring(1);
            }
            else
            {
                rut_valido_0 = this.rut;
            }


              axios.get('<%=request.getContextPath()%>/forms/insertOperador.json?rut_operador='+rut_valido_0+'&area_grupo='+this.area_grupo_cbo+'&nombre='+this.nombre+'&app_paterno='+this.apellido_paterno+'&app_materno='+this.apellido_materno+'&sap='+this.sap+'&estado_vigencia='+this.estado_vigencia_ope+'&empleado_por='+this.estado_empleado_ope+'&fecha_nacimiento='+this.fecha_nacimiento+'&cargo_operador='+this.cargo_grupo_cbo+'&superintendencia='+this.superintendencia_grupo_cbo+'&tramo='+this.tramo+'&fecha_ingreso='+this.fecha_ingreso+'&genero='+this.genero_ope)
                .then(function(response){
                alert("Se ingresa el Operador");
                window.location = "<%=request.getContextPath()%>/forms/formGruposOperarios.html";
              })
              .catch(function(){
                alert("Problemas al Ingresar el Operador");

              });

            
          }

      }

    }
  })

</script>


