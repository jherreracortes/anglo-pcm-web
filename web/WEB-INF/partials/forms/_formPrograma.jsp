<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core'      prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt'       prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div id="app">
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                      <h3 class="card-title mb-0" style="padding-left: 20px;">Sistema Gesti&oacute;n Competencias - Programa </h3>
                        <div class="col-sm-4">
                        </div>
                        </div>
                    </div>
                    <div>
                    <div class="table-responsive-md">
                    <table class="table">  
                    <thead>
                        <tr> 
                          <th scope="col">Programas</th>
                          <th scope="col">Estado Vigencia</th>
                          <th scope="col">Acciones</th>
                          <th scope="col"></th>
                          <th scope="col"
                            style="
                            padding-top: 12px;
                            padding-right: 12px;
                            padding-bottom: 12px;
                            padding-left: 12px;
                            "
                            >
                              <button v-on:click="agregar_programa" class="btn btn-primary">Agregar Programa</button>
                            </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr  v-for="programa in programas">
                            <td>{{programa.nombre}}</td>
                            <td>{{programa.estado_programa}}</td>
                            <td><a :href="'<%=request.getContextPath()%>/forms/formProgramaEdit.html?id_programa='+programa.id_programa"> Editar</td>
                            <td></td>
                            <td></td>  
                        </tr>
                      </tbody>
                    </table>
                  </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript"> 
var app6 = new Vue({
  el: '#app',
  data:{
    programas: []
  },
  created: function () {
    this.obtenerAPI()
    
  },
    methods:{
      obtenerAPI ()
       {
                axios.get('<%=request.getContextPath()%>/forms/ReporteProgramas.json')
                .then(response => {
                    this.programas = response.data;
                    })
                    .catch(function (error) {
                    });
        },

        agregar_programa: function(){


              window.location = "<%=request.getContextPath()%>/forms/formNewPrograma.html";

          }
          }
        })

</script>


