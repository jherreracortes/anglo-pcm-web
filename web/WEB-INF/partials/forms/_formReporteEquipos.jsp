<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core'      prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt'       prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!--<script src="https://unpkg.com/vuetify@0.16.9/dist/vuetify.min.js"></script>
<script src="https://unpkg.com/vue/dist/vue.js"></script>-->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/vue_2_6.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/vuetify.min.js"></script>
<!--<link rel="stylesheet" href="https://unpkg.com/vuetify@0.16.9/dist/vuetify.min.css">-->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
<link href="<c:out value="${pageContext.request.contextPath}"/>/Styles/css/vuetify.min.css" rel="stylesheet">
<style>
.myButton {
  display: inline-block;
  font-weight: 400;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  border: 1px solid transparent;
  padding: 0.375rem 0.75rem;
  font-size: 0.875rem;
  line-height: 1.5;
  border-radius: 0.25rem;
  transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
}

@media screen and (prefers-reduced-motion: reduce) {
  .myButton {
    transition: none;
  }
}

.myButton:hover, .myButton:focus {
  text-decoration: none;
}

.myButton:focus, .myButton.focus {
  outline: 0;
  box-shadow: 0 0 0 0.2rem rgba(32, 168, 216, 0.25);
}

.myButton.disabled, .myButton:disabled {
  opacity: 0.65;
}

.myButton:not(:disabled):not(.disabled) {
  cursor: pointer;
}

.myButton:not(:disabled):not(.disabled):active, .myButton:not(:disabled):not(.disabled).active {
  background-image: none;
}

a.myButton.disabled,
fieldset:disabled a.myButton {
  pointer-events: none;
}

.myButton-primary {
  color: #fff;
  background-color: #20a8d8;
  border-color: #20a8d8;
}

.myButton-primary:hover {
  color: #fff;
  background-color: #1b8eb7;
  border-color: #1985ac;
}

.myButton-primary:focus, .myButton-primary.focus {
  box-shadow: 0 0 0 0.2rem rgba(32, 168, 216, 0.5);
}

.myButton-primary.disabled, .myButton-primary:disabled {
  color: #fff;
  background-color: #20a8d8;
  border-color: #20a8d8;
}

.myButton-primary:not(:disabled):not(.disabled):active, .myButton-primary:not(:disabled):not(.disabled).active,
.show > .myButton-primary.dropdown-toggle {
  color: #fff;
  background-color: #1985ac;
  border-color: #187da0;
}

.myButton-primary:not(:disabled):not(.disabled):active:focus, .myButton-primary:not(:disabled):not(.disabled).active:focus,
.show > .myButton-primary.dropdown-toggle:focus {
  box-shadow: 0 0 0 0.2rem rgba(32, 168, 216, 0.5);
}

</style>
<div id="app">
  <v-app id="inspire">
    <div class="card-body">
                    <div class="row">
                            <h5 class="card-title mb-0" style="padding-left: 20px;">Sistema Gesti&oacute;n Competencias - Equipos </h5>    
                    </div>
                   
                        <table>
                          <tr>
                            <th 
                            style="
                            padding-top: 12px;
                            padding-right: 12px;
                            padding-bottom: 12px;
                            padding-left: 12px;
                            ">Area</th>
                            <th style="width: 120px;"></th>
                            <td
                            style="
                            padding-top: 12px;
                            padding-right: 12px;
                            padding-bottom: 12px;
                            padding-left: 12px;
                            ">
                              <treeselect v-model="area_cbo"
                           :options="areas" 
                           :disable-branch-nodes="true"
                           :show-count="true"/>
                            </td>
                          </tr>
                          <tr>
                          </tr>
                          <tr>
                            <th></th>
                            <th style="width: 120px;"></th>
                            <td
                            style="
                            padding-top: 12px;
                            padding-right: 12px;
                            padding-bottom: 12px;
                            padding-left: 12px;
                            "
                            >
                              <button v-on:click="buscar" class="myButton myButton-primary mb-2">Buscar</button>
                            </td>
                            <th style="width: 150px;"></th>
                            <th style="width: 150px;"></th>
                            <td
                            style="
                            padding-top: 12px;
                            padding-right: 12px;
                            padding-bottom: 12px;
                            padding-left: 12px;
                            "
                            >
                              <button v-on:click="agregar_equipo" class="myButton myButton-primary mb-2">Agregar Equipo</button>
                            </td>
                            
                          </tr>
                        </table>
     <v-card>
        <v-card-text>
          <v-card-title>
        Equipos
        <v-spacer></v-spacer>
        <v-text-field
          :append-icon="search?'close':'search'"
          :append-icon-cb="() => (search = '')"
          @keydown.native.escape="search=''"
          label="Search"
          single-line
          hide-details
          v-model="search"
        ></v-text-field>
      </v-card-title>
    <div>
      <div class="table-responsive-md">
                    <table class="table">
      <v-data-table
        v-bind:headers="headers"
        v-bind:items="items"
        v-bind:search="search"
        v-bind:pagination.sync="pagination"
        hide-actions
        class="elevation-1">
        <template slot="headerCell" slot-scope="props">{{ props.header.text }}
          <v-tooltip bottom>
            <span slot="activator">
              {{ props.header.text }}
            </span>
            <span>
              {{ props.header.text }}
            </span>
            <span>
              {{ props.header.text }}
            </span>
          </v-tooltip>
        </template>
        <template slot="items" slot-scope="props">
          <td  class="text-xs-left">{{ props.item.nombre_maquinaria }}</td>
          <td  class="text-xs-right">{{ props.item.estado_vigencia }}</td>
          <td  class="text-xs-right"><a :href="'<%=request.getContextPath()%>/forms/formReporteAutorizaciones.html?id_equipo='+props.item.id_maquinaria"> Autorizaciones</td>
          <td  class="text-xs-right"><a :href="'<%=request.getContextPath()%>/forms/formReporteEquipoEdit.html?id_equipo='+props.item.id_maquinaria">Editar</td>
        </template>
      </v-data-table>
      <div class="text-xs-center pt-2">
        <v-pagination v-model="pagination.page" :length="pages"></v-pagination>
        </v-card-text>
          <div style="flex: 1 1 auto;"></div>
      </v-card>
       </table>
     </div>
      </div>
        </v-card>
  </v-app>
</div>
</div>
</div>
<script type="text/javascript">
Vue.component('treeselect', VueTreeselect.Treeselect);

var app6= new Vue({
  el: '#app',
  data () {
    return {
      search: '',
      areas: [],
      area_cbo: null,
      pagination: {
      reporteEquipos:[],
        rowsPerPage: 10
      },
      selected: [],
      headers: [
        { text: "Nombre Equipo",value: "nombre_maquinaria" },
        { text: "Vigencia", value: "estado_vigencia" },
        { text: "Acciones", value: "id_maquinaria" }
      ],
      items: [],
    }
  },
  created: function () {
    this.obtenerAPI()
    
  },
  computed: {
    pages () {
      return this.pagination.rowsPerPage ? Math.ceil(this.items.length / this.pagination.rowsPerPage) : 0
    }
  },
  methods: {
obtenerAPI ()
       {
                axios.get('<%=request.getContextPath()%>/forms/reporteAreas.json')
                .then(response => this.areas = response.data)
                .catch(error => {});

               var prodId = getParameterByName('id_area');
                     

                     function getParameterByName(name) 
                     {
                        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                        results = regex.exec(location.search);
                        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
                      }

                      if(prodId.length > 0)
                      {
                        axios.get('<%=request.getContextPath()%>/forms/ReporteEquipos.json?id_area='+prodId)
                .then(response => {

                    this.items = response.data;
                    this.area_cbo =prodId;
                    //console.log(JSON.stringify(this.reporteGrupos));
                    })
                    .catch(function (error) {
                    //console.log(error);
                    });
                      }    
      },

       buscar: function(){

        axios.get('<%=request.getContextPath()%>/forms/ReporteEquipos.json?id_area='+this.area_cbo)
                .then(response => {

                    this.items = response.data;
                    //console.log(JSON.stringify(this.reporteGrupos));
                    })
                    .catch(function (error) {
                    //console.log(error);
                    });
            
          },
          agregar_equipo: function(){


              window.location = "<%=request.getContextPath()%>/forms/formNewEquipo.html";

          }
  
}
})
</script>