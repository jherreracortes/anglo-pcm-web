<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core'      prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt'       prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script src="https://unpkg.com/vue@latest"></script>
<script src="https://unpkg.com/vue-select@latest"></script>
<script src="https://unpkg.com/lodash@latest/lodash.min.js"></script>

<div id="app">
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                      <h3 class="card-title mb-0" style="padding-left: 20px;">Sistema Gesti&oacute;n Competencias - Equipo Nuevo </h3>
                        <div class="col-sm-4">
                        </div>
                        </div>
                    </div>
                    <div>
                    <div class="table-responsive-md">
                    <table class="table">  
                    <thead >
                      <div>
                        <tr > 
                          <th scope="col">Nombre Maquina</th>
                          <th scope="col"><input type="text" v-model="nombre_maquinaria" ref="nombre_maquinaria"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>
                        <tr > 
                          <th scope="col">Estado Vigencia</th>
                          <th scope="col">
                          <div v-for="answer in ['ACTIVO', 'DESACTIVADO']">
                                    <input 
                                      type="radio" 
                                      name="answer" 
                                      :id="answer"
                                      :value="answer"
                                      v-model="estado_vigencia_rdb">
                                    {{ answer }}
                                </div></th> 
                             <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>   
                        </tr>
                        <tr> 
                          <th scope="col"style="padding-left: 20px;">Area</th>
                          <th scope="col" colspan="2" style="width: 200px;padding-right: 120px;" ><treeselect v-model="area_cbo"
                           :options="areas" 
                           :disable-branch-nodes="true"
                           :show-count="true"
                           /></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>
                         <tr>
                          <td><button v-on:click="agregar_equipo" class="btn btn-primary">Agregar Nuevo Equipo</button></td>
                        </tr>
                      </div>
                      </thead>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript"> 
Vue.component('treeselect', VueTreeselect.Treeselect);
var app6 = new Vue({
  el: '#app',
   mounted:function(){
     this.obtenerAPI()
   },
  data:{
    areas: [],
    area_cbo:null,
    nombre_maquinaria:'',
    estado_vigencia:'',
    estado_vigencia_rdb:'ACTIVO',
    selected: 0,
  },
  
    methods:{
      obtenerAPI ()
       {

              axios.get('<%=request.getContextPath()%>/forms/reporteAreas.json')
                .then(response => this.areas = response.data)
                .catch(error => {});
       },

       agregar_equipo: function(){

          if(this.nombre_maquinaria.lenght > 0 || this.nombre_maquinaria !='')
          {
              if(this.area_cbo != null )
              {
              
              axios.get('<%=request.getContextPath()%>/forms/insertNewEquipo.json?nombre_maquinaria='+this.nombre_maquinaria+"&area="+this.area_cbo+"&vigencia="+this.estado_vigencia_rdb)
                .then(function(response){

                alert("Se creo el nuevo equipo");
                window.location = "<%=request.getContextPath()%>/forms/formReporteEquipos.html";
              })
              .catch(function(){
                alert("Problemas al ingresar el nuevo equipo");

              });

            }
            else
            {

              alert("Debe seleccionar un area a la que se asignara el equipo");
            }
          }
        else
        {
          alert("Debe ingresar un  nombre para asignarle al equipo");
          this.$refs.nombre_maquinaria.focus();
        }
      }

    }
  })

</script>


