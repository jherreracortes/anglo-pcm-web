<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core'      prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt'       prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<!--<script src="https://unpkg.com/vuetify@0.16.9/dist/vuetify.min.js"></script>
<script src="https://unpkg.com/vue/dist/vue.js"></script>-->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/vue_2_6.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/vuetify.min.js"></script>
<!--<link rel="stylesheet" href="https://unpkg.com/vuetify@0.16.9/dist/vuetify.min.css">-->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
<link href="<c:out value="${pageContext.request.contextPath}"/>/Styles/css/vuetify.min.css" rel="stylesheet">
<style>
.myButton {
  display: inline-block;
  font-weight: 400;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  border: 1px solid transparent;
  padding: 0.375rem 0.75rem;
  font-size: 0.875rem;
  line-height: 1.5;
  border-radius: 0.25rem;
  transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
}

@media screen and (prefers-reduced-motion: reduce) {
  .myButton {
    transition: none;
  }
}

.myButton:hover, .myButton:focus {
  text-decoration: none;
}

.myButton:focus, .myButton.focus {
  outline: 0;
  box-shadow: 0 0 0 0.2rem rgba(32, 168, 216, 0.25);
}

.myButton.disabled, .myButton:disabled {
  opacity: 0.65;
}

.myButton:not(:disabled):not(.disabled) {
  cursor: pointer;
}

.myButton:not(:disabled):not(.disabled):active, .myButton:not(:disabled):not(.disabled).active {
  background-image: none;
}

a.myButton.disabled,
fieldset:disabled a.myButton {
  pointer-events: none;
}

.myButton-primary {
  color: #fff;
  background-color: #20a8d8;
  border-color: #20a8d8;
}

.myButton-primary:hover {
  color: #fff;
  background-color: #1b8eb7;
  border-color: #1985ac;
}

.myButton-primary:focus, .myButton-primary.focus {
  box-shadow: 0 0 0 0.2rem rgba(32, 168, 216, 0.5);
}

.myButton-primary.disabled, .myButton-primary:disabled {
  color: #fff;
  background-color: #20a8d8;
  border-color: #20a8d8;
}

.myButton-primary:not(:disabled):not(.disabled):active, .myButton-primary:not(:disabled):not(.disabled).active,
.show > .myButton-primary.dropdown-toggle {
  color: #fff;
  background-color: #1985ac;
  border-color: #187da0;
}

.myButton-primary:not(:disabled):not(.disabled):active:focus, .myButton-primary:not(:disabled):not(.disabled).active:focus,
.show > .myButton-primary.dropdown-toggle:focus {
  box-shadow: 0 0 0 0.2rem rgba(32, 168, 216, 0.5);
}

.redondeado {
   border-radius: 5px;
 }
 .confondo {
   background-color: #def;
 }

</style>

<div id="app">
  <v-app id="inspire">
    <div class="card-body">
                    <div class="row">
                            <h5 class="card-title mb-0" style="padding-left: 20px;">Sistema Gesti&oacute;n Competencias - Autorizaciones</h5>    
                    </div>
                        <table>
                          <tr>
                            <th style="padding: 12px;width: 124px;">Area Operador</th>
                            <td style="padding: 12px;width: 250px;">
                            <treeselect style="width: 250px;" 
                            v-model="area_cbo"
                           :options="areas" 
                           :disable-branch-nodes="true"
                           :show-count="true"></treeselect>
                            </td>
                            <td style="width: 250px;"></td>
                            <td style="width: 250px;"></td>
                            <td style="width: 250px;"></td>
                          </tr>
                          <tr>
                            <th style="padding: 12px;width: 124px;">Equipo</th>
                            <td style="padding: 12px; width: 250px;">
                            <treeselect style="width: 250px;" 
                            v-model="maquinaria_cbo"
                           :options="maquinarias" 
                           :disable-branch-nodes="true"
                           :show-count="true"></treeselect>
                            </td>
                            <td style="width: 250px;"></td>
                            <td style="width: 250px;"></td>
                            <td style="width: 250px;"></td>
                          </tr>
                          <tr>
                            <th style="padding-top: 15px;padding-left: 12px;width: 124px;">Fecha Inicial</th>
                            <td style="padding: 12px; width: 200px;">
                            <vuejs-datepicker 
                            :monday-first="true" 
                            :calendar-button="true" 
                            calendar-button-icon="fa fa-calendar" 
                            :bootstrap-styling="true" 
                            format="dd-MM-yyyy" 
                            id="date" 
                            v-model="fecha"></vuejs-datepicker> 
                            </td>
                            <th style="padding-top: 15px;padding-left: 12px;width: 124px;">Fecha Termino</th>
                            <td style="padding: 12px; width: 200px;">
                            <vuejs-datepicker 
                            :monday-first="true" 
                            :calendar-button="true" 
                            calendar-button-icon="fa fa-calendar" 
                            :bootstrap-styling="true" 
                            format="dd-MM-yyyy" 
                            id="date" 
                            v-model="fecha_termino"></vuejs-datepicker> 
                            </td>
                          </tr>
                          <tr>
                            <th style="width: 120px;"></th>
                            <td
                            style="
                            padding-top: 12px;
                            padding-right: 12px;
                            padding-bottom: 12px;
                            padding-left: 12px;
                            "
                            >
                              <button v-on:click="buscar" class="myButton myButton-primary mb-2">Buscar</button>
                            </td>
                            <th style="width: 150px;"></th>
                            <th style="width: 150px;"></th>
                            <td
                            style="
                            padding-top: 12px;
                            padding-right: 12px;
                            padding-bottom: 12px;
                            padding-left: 12px;
                            "
                            >
                              <button v-on:click="agregar_autorizacion" class="myButton myButton-primary mb-2">Agregar Autorizacíon</button>
                            </td>
                            
                          </tr>
                        </table>

     <v-card> 
        <v-card-text>
          <v-card-title>
        Autorizaciones
        <v-spacer></v-spacer>
        <v-text-field
          :append-icon="search?'close':'search'"
          :append-icon-cb="() => (search = '')"
          @keydown.native.escape="search=''"
          label="Search"
          single-line
          hide-details
          v-model="search"
        ></v-text-field>
      </v-card-title>
    <div>
      <div class="table-responsive-md">
                    <table class="table">
      <v-data-table
        v-bind:headers="headers"
        v-bind:items="items"
        v-bind:search="search"
        v-bind:pagination.sync="pagination"
        hide-actions
        class="elevation-1">
        <template slot="headerCell" slot-scope="props">{{ props.header.text }}
          <v-tooltip bottom>
            <span slot="activator">
              {{ props.header.text }}
            </span>
            <span>
              {{ props.header.text }}
            </span>
            <span>
              {{ props.header.text }}
            </span>
            <span>
              {{ props.header.text }}
            </span>
          </v-tooltip>
        </template>
        <template slot="items" slot-scope="props">
          <td>{{ props.item.area }}</td>
          <td  class="text-xs-right">{{ props.item.nombre_maquinaria }}</td>
          <td  class="text-xs-right">{{ props.item.rut_operador }}</td>
          <td  class="text-xs-right">{{ props.item.nombre_operador }}</td>
          <td  class="text-xs-right">{{ props.item.fecha_autorizacion }}</td>
          <td  class="text-xs-right" v-if="props.item.tipo_autorizacion === 'A'">Autorizado</td>
          <td  class="text-xs-right" v-else-if="props.item.tipo_autorizacion === 'E'">En Evaluacion</td>
          <td  class="text-xs-right" v-else-if="props.item.tipo_autorizacion === 'R'">Re-Evaluacion</td>
          <td  class="text-xs-right" v-else-if="props.item.tipo_autorizacion === 'V'">Validacion</td>
          <td  class="text-xs-right" v-else-if="props.item.tipo_autorizacion === 'RI'">En Re-Instrucci&oacute;n</td>
          <td  class="text-xs-right" v-if="props.item.rut_monitor === 'null'">Sin Monitor</td>
          <td  class="text-xs-right" v-else="props.item.rut_monitor !== null">{{ props.item.rut_monitor }}</td>
          <td  class="text-xs-right" v-if="props.item.rut_monitor === 'null'">Sin Monitor</td>
          <td  class="text-xs-right" v-else="props.item.rut_monitor !== null">{{ props.item.nombre_monitor }}</td>
          <td  class="text-xs-right"><a :href="'<%=request.getContextPath()%>/forms/formEditAutorizaciones.html?id='+props.item.id_autorizacion">Editar</td>
        </template>
      </v-data-table>
      <div class="text-xs-center pt-2">
        <v-pagination v-model="pagination.page" :length="pages"></v-pagination>
        </v-card-text>
          <div style="flex: 1 1 auto;"></div>
      </v-card>
       </table>
     </div>
     </div>
        </v-card>  
  </v-app>
</div>
</div>
</div>
<script type="text/javascript">
Vue.component('treeselect', VueTreeselect.Treeselect);

var app6= new Vue({
  el: '#app',
  data () {
    return {
      search: '',
      fecha: new Date(),
      fecha_termino: new Date(),
      areas: [],
      area_cbo: null,
      maquinaria_cbo:null,
      maquinarias:[],
      reporte_autorizacion:[],
      pagination: {

        rowsPerPage: 10
      },
      selected: [],
      headers: [
        { text: "Area Operador",value: "area" },
        { text: "Equipo", value: "nombre_maquinaria" },
        { text: "Rut Operador", value: "rut_operador" },
        { text: "Nombre Operador", value: "nombre_operador" },
        { text: "Fecha Autorizacion", value: "fecha_autorizacion" },
        { text: "Autorizacion", value: "tipo_autorizacion" },
        { text: "Rut Monitor", value: "rut_monitor" },
        { text: "Nombre Monitor", value: "nombre_monitor" },
        { text: "Acciones", value: "" }
      ],
      items: [],
    }
  },
  created: function () {
    this.obtenerAPI()
    
  },
  components: {
    vuejsDatepicker

  },
  computed: {
    pages () {
      return this.pagination.rowsPerPage ? Math.ceil(this.items.length / this.pagination.rowsPerPage) : 0
    }
  },
  methods: {
obtenerAPI ()
       {
                axios.get('<%=request.getContextPath()%>/forms/reporteAreas.json')
                .then(response => this.areas = response.data)
                .catch(error => {});

                axios.get('<%=request.getContextPath()%>/forms/BuscadorMaquinarias.json')
                .then(response => this.maquinarias = response.data)
                .catch(error => {});

                var prodId = getParameterByName('id_equipo');
                     

                     function getParameterByName(name) 
                     {
                        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                        results = regex.exec(location.search);
                        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
                      }

                      if(prodId.length >0)
                      {

                           axios.get('<%=request.getContextPath()%>/forms/ReporteAutorizacionMaquina.json?area=0&equipo='+prodId+'&reporte=0')
                            .then(response => {
                              this.items = response.data;
                              this.maquinaria_cbo = prodId;
                              })
                            .catch(error => {});
                      }
                      
      },

       buscar: function(){

                    if(this.maquinaria_cbo != null && this.area_cbo == null)
                          {  
                          axios.get('<%=request.getContextPath()%>/forms/ReporteAutorizacion.json?area=0&equipo='+this.maquinaria_cbo+'&fecha='+this.fecha+'&fecha_termino='+this.fecha_termino)
                            .then(response => this.items = response.data)
                            .catch(error => {});
                          }
                          else
                          {

                            if(this.area_cbo != null && this.maquinaria_cbo == null)
                              {  
                              axios.get('<%=request.getContextPath()%>/forms/ReporteAutorizacion.json?area='+this.area_cbo+'&equipo=0&fecha='+this.fecha+'&fecha_termino='+this.fecha_termino)
                                .then(response => this.items = response.data)
                                .catch(error => {});
                              }
                              else
                              {
                                if(this.area_cbo == null && this.maquinaria_cbo == null)
                                {
                                  axios.get('<%=request.getContextPath()%>/forms/ReporteAutorizacion.json?area=0&equipo=0&fecha='+this.fecha+'&fecha_termino='+this.fecha_termino)
                                .then(response => this.items = response.data)
                                .catch(error => {});
                                }
                                else
                                {
                                  if(this.area_cbo != null && this.maquinaria_cbo != null)
                                    {
                                      axios.get('<%=request.getContextPath()%>/forms/ReporteAutorizacion.json?area='+this.area_cbo+'&equipo='+this.maquinaria_cbo+'&fecha='+this.fecha+'&fecha_termino='+this.fecha_termino)
                                    .then(response => this.items = response.data)
                                    .catch(error => {});
                                    }
                                }

                              }
                          }  

          },
          agregar_autorizacion: function(){
              window.location = "<%=request.getContextPath()%>/forms/formAutorizaciones.html";
          }
  
}
})
</script>