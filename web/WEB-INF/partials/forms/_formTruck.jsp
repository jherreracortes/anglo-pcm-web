<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core'      prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt'       prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div id="app">
  
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <h4 class="card-title mb-0">Reportes Caudales</h4>    
                        </div>
                        </div>
                        <!-- /.col-->
                    </div>
                    <div>
                    <div class="form-inline">
                      <div class="form-group mx-sm-3 mb-2">
                        <label>Fecha De B&uacute;squeda</label>
                        <vuejs-datepicker  id="fecha" v-model="fecha" format='dd-MM-yyyy'> </vuejs-datepicker>
                      </div>
                      <button v-on:click="buscar" class="btn btn-primary mb-2">Buscar</button>
                    </div> 
                    <!-- /.row-->
                    <div class="table-responsive-md">
                    <table class="table">  
                      <thead>
                        <tr> 
                          <th scope="col">Nombre Estacion</th>
                          <th scope="col">Fecha</th>
                          <th scope="col">Caudal Medio</th>
                          <th scope="col">Estado</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr  v-for="estacion in estaciones">
                          <td>{{estacion.estacion}}</td>
                          <td>{{estacion.fecha}}</td>
                          <td>{{estacion.caudal_medio}}</td>
                          <td>{{estacion.estado}}</td>
                        </tr>
                      </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript"> 
var app6 = new Vue({
  el: '#app',
  data: {
    fecha: new Date(),
    estaciones: []

  },
  components: {
    vuejsDatepicker

  },
    methods:{
        buscar: function(){
                axios.get('<%=request.getContextPath()%>/forms/reporteCaudal.json?fecha='+this.fecha)
                .then(response => {
                    this.estaciones = response.data;
                    console.log(this.estaciones);
                    })
                    .catch(function (error) {
                    console.log(error);
                    });  
            }
          }
        })

</script>
