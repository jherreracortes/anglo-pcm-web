<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core'      prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt'       prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script src="https://unpkg.com/vue@latest"></script>
<script src="https://unpkg.com/vue-select@latest"></script>
<script src="https://unpkg.com/lodash@latest/lodash.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/v-mask/dist/v-mask.min.js"></script>
<style>
img {
  height: auto;
  max-width: 2.5rem;
  margin-right: 1rem;
}

.d-center {
  display: flex;
  align-items: center;
}

.selected img {
  width: auto;
  max-height: 23px;
  margin-right: 0.5rem;
}

.v-select .dropdown li {
  border-bottom: 1px solid rgba(112, 128, 144, 0.1);
}

.v-select .dropdown li:last-child {
  border-bottom: none;
}

.v-select .dropdown li a {
  padding: 10px 20px;
  width: 100%;
  font-size: 1.25em;
  color: #3c3c3c;
}

.v-select .dropdown-menu .active > a {
  color: #fff;
}
</style>
<div id="app">
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                      <h3 class="card-title mb-0" style="padding-left: 20px;">Sistema Gesti&oacute;n Competencias - Edicion Operadores</h3>
                  </div>
                    </div>
                    <div class="table-responsive-md">
                    <table class="table"> 
                      <tbody>
                       <tr>
                           <th scope="col" style="padding-left: 20px;">Rut</th>
                          <th scope="col" colspan="2">
                            <v-select v-model="rut_cbo" label="rut" :filterable="false" :options="options" @search="onSearch">
                              <template slot="no-options">
                                Ingrese el rut a realizar la busqueda..
                              </template>
                              <template slot="option" slot-scope="option">
                                <div class="d-center">
                                  {{ option.rut }}
                                  </div>
                              </template>
                              <template slot="selected-option" slot-scope="option">
                                <div class="selected d-center">
                                  {{ option.rut }}
                                </div>
                              </template>
                              </v-select>
                            </th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>
                      <tbody v-if="nombre != 'NULL'">
                      <tr> 
                          <th scope="col" style="padding-left: 20px;">Area - Grupo</th>
                          <th scope="col" colspan="2" style="width: 450px;">
                           <treeselect v-model="area_grupo_cbo"
                           :options="grupo" 
                           :disable-branch-nodes="true"
                           :show-count="true"
                           /></th> 
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>
                        <tr> 
                          <th scope="col" style="padding-left: 20px;">Nombre</th>
                          <th scope="col"><input type="text" v-model="nombre" ref="nombre" placeholder="Ingrese Su Nombre" ></th>
                          <th scope="col">Apellido Paterno</th>
                          <th scope="col"><input type="text" v-model="apellido_paterno" ref="apellido_paterno" placeholder="Ingrese Su Apellido Paterno" ></th>
                          <th scope="col">Apellido Materno</th>
                          <th scope="col"><input type="text" v-model="apellido_materno" ref="apellido_materno" placeholder="Ingrese Su Apellido Materno" ></th>
                        </tr>
                        <tr> 
                          <th scope="col" style="padding-left: 20px;">SAP</th>
                          <th scope="col"><input type="text" v-model="sap" ref="sap" v-mask="'###########'"  placeholder="Ingrese El SAP"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>
                        <tr>  
                          <th scope="col" style="padding-left: 20px;">Estado Vigencia</th>
                          <th scope="col">
                          <div v-for="answer in ['VIGENTE', 'FINIQUITADO']">
                                    <input 
                                      type="radio" 
                                      name="answer" 
                                      :id="answer"
                                      :value="answer"
                                      v-model="estado_vigencia_ope">
                                    {{ answer }}
                         </div>
                         </th>
                         <th scope="col"></th>
                         <th scope="col"></th>
                         <th scope="col"></th>
                         <th scope="col"></th> 
                        </tr>
                        <tr> 
                          <th scope="col" style="padding-left: 20px;">Empleado Por</th>
                          <th scope="col">
                          <div v-for="emp_ope in ['ANGLO', 'CONTRATISTA']">
                                    <input 
                                      type="radio" 
                                      name="emp_ope" 
                                      :id="emp_ope"
                                      :value="emp_ope"
                                      v-model="estado_empleado_ope">
                                    {{ emp_ope }}
                          </div>
                        </th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th> 
                        </tr>
                        <tr> 
                        <th scope="col" style="padding-left: 20px;">Fecha Nacimiento</th>
                        <th scope="col"><vuejs-datepicker v-model="fecha_nacimiento" format='dd-MM-yyyy'></vuejs-datepicker></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                      </tr>
                      <tr>  
                          <th scope="col" style="padding-left: 20px;padding-top: 20px">Cargo</th>
                          <th scope="col" colspan="2" style="width: 450px;">
                           <treeselect v-model="cargo_grupo_cbo"
                           :options="cargo_grupo" 
                           :disable-branch-nodes="true"
                           :show-count="true"
                           /></th> 
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>
                        <tr>  
                          <th scope="col" style="padding-left: 20px;padding-top: 20px">Superintendencia</th>
                          <th scope="col" colspan="2" style="width: 450px;">
                           <treeselect v-model="superintendencia_grupo_cbo"
                           :options="superintendencia_grupo" 
                           :disable-branch-nodes="true"
                           :show-count="true"
                           /></th> 
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>
                        <tr>  
                          <th scope="col" style="padding-left: 20px;">Tramo</th>
                          <th scope="col" colspan="2"><treeselect v-model="tramo"
                           :options="tramos" 
                           :disable-branch-nodes="true"
                           :show-count="true"
                           /></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                        </tr>
                        <tr> 
                        <th scope="col" style="padding-left: 20px;">Fecha Ingreso</th>
                        <th scope="col"><vuejs-datepicker v-model="fecha_ingreso" format='dd-MM-yyyy'></vuejs-datepicker></th>
                        <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th>  
                      </tr>
                         <tr> 
                          <td style="padding-left: 20px;"><button v-on:click="modificar_operador" class="btn btn-primary">Modificar Operador</button></td>
                        </tr>
                      </tbody>
                      <tbody v-else="rut_cbo == ''"></tbody>
                    </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript"> 

Vue.component("v-select", VueSelect.VueSelect);
Vue.component('treeselect', VueTreeselect.Treeselect);
Vue.use(VueMask.VueMaskPlugin);
var app6 = new Vue({
  el: '#app',
  data() {
        return {
            area_grupo_cbo: null,
            grupo:[],
            cargo_grupo:[],
            superintendencia_grupo_cbo:null,
            superintendencia_grupo:[],
            cargo_grupo_cbo:null,
            nombre:'NULL',
            apellido_paterno:'',
            apellido_materno:'',
            rut:'',
            sap:'',
            estado_vigencia_ope:'VIGENTE',
            estado_empleado_ope:'ANGLO',
            fecha_nacimiento:new Date(),
            cargo:'',
            superintendencia:'',
            tramo:null,
            tramos:[],
            fecha_ingreso:new Date(),
            rut_cbo : '',
            options: []
        };
    },
    watch: {
        'rut_cbo': function(val, oldVal){

          if(this.rut_cbo != null)
          {
              this.rut=JSON.stringify(val.rut).replace(/"/g, "");
              this.nombre=JSON.stringify(val.nombre).replace(/"/g, "");
              this.apellido_paterno=JSON.stringify(val.apellido_paterno).replace(/"/g, "");
              this.apellido_materno=JSON.stringify(val.apellido_materno).replace(/"/g, "");
              this.area_grupo=JSON.stringify(val.area_grupo).replace(/"/g, "");
              this.sap=JSON.stringify(val.sap).replace(/"/g, "");
              this.estado_vigencia_ope=JSON.stringify(val.estado_vigencia).replace(/"/g, "");
              this.estado_empleado_ope=JSON.stringify(val.tipo_empleado).replace(/"/g, "");
              this.fecha_nacimiento=JSON.stringify(val.fecha_nacimiento).replace(/"/g, "");
              this.cargo=JSON.stringify(val.cargo).replace(/"/g, "");
              this.superintendencia=JSON.stringify(val.superintendencia).replace(/"/g, "");
              this.tramo=JSON.stringify(val.id_tramo).replace(/"/g, "");
              this.fecha_ingreso=JSON.stringify(val.fecha_ingreso).replace(/"/g, "");

                axios.get('<%=request.getContextPath()%>/forms/BuscadorGrupos.json')
                .then(response => {
                    this.grupo = response.data;
                    this.area_grupo_cbo=this.area_grupo;
                    } )
                .catch(error => {});

                axios.get('<%=request.getContextPath()%>/forms/CargosCBO.json')
                .then(response => {
                    this.cargo_grupo = response.data;
                    this.cargo_grupo_cbo = this.cargo;
                   
                    } )
                .catch(error => {});

                axios.get('<%=request.getContextPath()%>/forms/SuperintendenciaCBO.json')
                .then(response => {
                    this.superintendencia_grupo = response.data;
                    this.superintendencia_grupo_cbo = this.superintendencia;
                   
                    } )
                .catch(error => {});

                 axios.get('<%=request.getContextPath()%>/forms/TramoCBO.json')
                .then(response => {
                    this.tramos = response.data;
                    this.tramo = this.tramo;
                    } )
                .catch(error => {});
          }
          else
          {
             this.nombre='NULL';
          }    
            
        }
      },
  components: {
    vuejsDatepicker

  },
    methods:{
      onSearch: function onSearch(search, loading) {
      loading(true);
      this.search(loading, search, this);
    },
    search: _.debounce(function (loading, search, vm) {
      this.fetch(loading, search, vm);
      
    }, 350),

      fetch(loading, search, vm) {
            
            axios.get('<%=request.getContextPath()%>/forms/BuscadorOperadoresEdit.json?rut='+escape(search))
                .then(response => vm.options = response.data)
                .catch(error => {});
                loading(false);
        },

        modificar_operador: function(){

              //window.location = "<%=request.getContextPath()%>/forms/formEditOperador.html";

            var validacion_form=false;
            var fecha_nac_vali = new Date(this.fecha_nacimiento);
            console.log("fecha_nacimiento: "+this.fecha_nacimiento);
            console.log("fecha_nacimiento length: "+fecha_nac_vali);
            var fecha_ingre_vali = new Date(this.fecha_ingreso);
            var fecha_now =new Date();
            var AnyoFecha_nac = fecha_nac_vali.getFullYear();
            var MesFecha_nac = fecha_nac_vali.getMonth();
            var DiaFecha_nac = fecha_nac_vali.getDate();

            var AnyoFecha_ingreso = fecha_ingre_vali.getFullYear();
            var MesFecha_ingreso = fecha_ingre_vali.getMonth();
            var DiaFecha_ingreso = fecha_ingre_vali.getDate();

            console.log("fehca Nacimiento: "+formatDate(this.fecha_nacimiento));
            console.log("fehca ingresa: "+formatDate(this.fecha_ingreso));
     
            var AnyoHoy = fecha_now.getFullYear();
            var MesHoy = fecha_now.getMonth();
            var DiaHoy = fecha_now.getDate();  

            function formatDate(date) {
                    var d = new Date(date),
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();

                    if (month.length < 2) month = '0' + month;
                    if (day.length < 2) day = '0' + day;

                    return [year, month, day].join('-');
                    }                       

              if(this.area_grupo_cbo != null)
              {
                  if(this.nombre != '' || this.nombre.length > 0)
                    {

                       if(this.apellido_paterno != '' || this.apellido_paterno.length > 0)
                          {
                             if(this.apellido_materno != '' || this.apellido_materno.length > 0)
                                {
                                   if(this.sap != '' || this.sap.length > 0)
                                    {
                                      
                                     if(fecha_nac_vali != "Invalid Date")
                                     {

                                          if(fecha_ingre_vali != "Invalid Date")
                                          {

                                            if(AnyoFecha_nac == AnyoHoy && MesFecha_nac == MesHoy && DiaFecha_nac == DiaHoy)
                                            {
                                              alert("La Fecha De Nacimiento no puede ser el dia de Hoy");
                                              validacion_form=false;

                                            }
                                            else
                                            {
                                              if(AnyoFecha_ingreso == AnyoFecha_nac && MesFecha_nac == MesFecha_ingreso && DiaFecha_nac == DiaFecha_ingreso)
                                              {
                                                alert("La Fecha De Ingreso no puede ser igual a la Fecha de Nacimiento");
                                                validacion_form=false;
                                              }
                                              else
                                              {
                                                 if(this.cargo_grupo_cbo != null)
                                                 {
                                                      if(this.superintendencia_grupo_cbo != null)
                                                         {
                                                            if(this.tramo != null)
                                                                 {
                                                                    validacion_form=true;
                                                                 }
                                                                 else
                                                                 {
                                                                   alert("Debe Ingresar un Tramo al Operador");
                                                                   validacion_form=false;
                                                                   this.$refs.tramo.focus();
                                                                 }
                                                         }
                                                         else
                                                         {
                                                           alert("Debe Ingresar una Superintendencia al Operador");
                                                           validacion_form=false;
                                                         
                                                         }
                                                 }
                                                 else
                                                 {
                                                   alert("Debe Ingresar un Cargo al Operador");
                                                   validacion_form=false;
                                                  
                                                 }

                                              }

                                            }
                                          }
                                          else
                                          {

                                            alert("La Fecha De Ingreso No Es Valida");
                                            validacion_form=false;
                                          }

                                          }
                                          else
                                          {
                                            alert("Debe Ingresar la Fecha De Nacimiento");
                                            validacion_form=false;
                                          }
                                    }
                                    else
                                    {
                                      alert("Debe Ingresar El SAP para el Operador");
                                      validacion_form=false;
                                      this.$refs.sap.focus();
                                    }

                                }
                                else
                                {
                                  alert("Debe Ingresar Apellido Materno para el Operador");
                                  validacion_form=false;
                                  this.$refs.apellido_materno.focus();
                                }
                          }
                          else
                          {
                            alert("Debe Ingresar Apellido Paterno para el Operador");
                            validacion_form=false;
                            this.$refs.apellido_paterno.focus();
                          }
                    }
                    else
                    {
                      alert("Debe Ingresar un Nombre para el Operador");
                      validacion_form=false;
                      this.$refs.nombre.focus();
                    }
              }
              else
              {
                alert("Debe seleccionar el area y grupo al que pertenecera el Operador");
                validacion_form=false;
              }

              if(validacion_form)
              {
                 
                  axios.get('<%=request.getContextPath()%>/forms/insertModOperador.json?rut_operador='+this.rut+'&area_grupo='+this.area_grupo_cbo+'&nombre='+this.nombre+'&app_paterno='+this.apellido_paterno+'&app_materno='+this.apellido_materno+'&sap='+this.sap+'&estado_vigencia='+this.estado_vigencia_ope+'&empleado_por='+this.estado_empleado_ope+'&fecha_nacimiento='+formatDate(this.fecha_nacimiento)+'&cargo_operador='+this.cargo_grupo_cbo+'&superintendencia='+this.superintendencia_grupo_cbo+'&tramo='+this.tramo+'&fecha_ingreso='+formatDate(this.fecha_ingreso))
                    .then(function(response){
                    alert("Se Modifica el Operador");
                    window.location = "<%=request.getContextPath()%>/forms/formGruposOperarios.html";
                  })
                  .catch(function(){
                    alert("Problemas al Modificar el Operador");

                  });
            
                 }
          }
  }
  })

</script>


