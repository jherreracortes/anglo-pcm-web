<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core'      prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt'       prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div id="app">
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                      <h3 class="card-title mb-0" style="padding-left: 20px;">Sistema Gesti&oacute;n Competencias - Edicion De Equipo </h3>
                        <div class="col-sm-4">
                        </div>
                        </div>
                    </div>
                    <div>
                    <div class="table-responsive-md">
                    <table class="table">  
                    <thead v-for="reporteEquipo in reporteEquipos">
                      <div>
                        <tr > 
                          <th scope="col">Nombre Maquina</th>
                          <th scope="col"><input type="text" v-model="reporteEquipo.nombre_maquinaria"></th>
                        </tr>
                        <tr > 
                          <th scope="col">Estado Vigencia</th>
                          <th scope="col">
                          <div v-for="answer in ['ACTIVO', 'DESACTIVADO']">
                                    <input 
                                      type="radio" 
                                      name="answer" 
                                      :id="answer"
                                      :value="answer"
                                      v-model="reporteEquipo.estado_vigencia">
                                    {{ answer }}
                                </div></th> 
                        </tr>
                         <tr>
                          <td><button v-on:click="modificar(reporteEquipo)" class="btn btn-primary">Modificar</button></td>
                        </tr>
                      </div>
                      </thead>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript"> 

var app6 = new Vue({
  el: '#app',
   mounted:function(){
     this.obtenerAPI()
   },
  data:{
    reporteEquipos:[{
    nombre_maquinaria:'',
    estado_vigencia:''
    }],
    
    form : {}
  },
    methods:{
      obtenerAPI ()
       {

                var prodId = getParameterByName('id_equipo');
                     

                     function getParameterByName(name) 
                     {
                        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                        results = regex.exec(location.search);
                        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
                      }

                      axios.get('<%=request.getContextPath()%>/forms/ReporteEquipoEdit.json?id_equipo='+prodId)
                .then(response => {

                    this.reporteEquipos = response.data;
                    console.log(JSON.stringify(this.reporteEquipos));
                    })
                    .catch(function (error) {
                    });

            },

       modificar: function(data){


               var prodId = getParameterByName('id_equipo');
                     

                     function getParameterByName(name) 
                     {
                        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                        results = regex.exec(location.search);
                        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
                      }
              
              axios.get('<%=request.getContextPath()%>/forms/insertModMaquina.json?id_equipo='+prodId+"&nombre="+data.nombre_maquinaria+"&vigencia="+data.estado_vigencia)
                .then(function(response){

                alert("Se realiza modificacion al equipo");
                window.location = "<%=request.getContextPath()%>/forms/formReporteEquipos.html";
              })
              .catch(function(){
                alert("Problemas al modificar el equipo");

              });
                 
            
          }
          }
        })

</script>


