<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core'      prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt'       prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script src="https://unpkg.com/vue@latest"></script>
<script src="https://unpkg.com/vue-select@latest"></script>
<script src="https://unpkg.com/lodash@latest/lodash.min.js"></script>

<style>
img {
  height: auto;
  max-width: 2.5rem;
  margin-right: 1rem;
}

.d-center {
  display: flex;
  align-items: center;
}

.selected img {
  width: auto;
  max-height: 23px;
  margin-right: 0.5rem;
}

.v-select .dropdown li {
  border-bottom: 1px solid rgba(112, 128, 144, 0.1);
}

.v-select .dropdown li:last-child {
  border-bottom: none;
}

.v-select .dropdown li a {
  padding: 10px 20px;
  width: 100%;
  font-size: 1.25em;
  color: #3c3c3c;
}

.v-select .dropdown-menu .active > a {
  color: #fff;
}
</style>

<div id="app">
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                      <h3 class="card-title mb-0" style="padding-left: 20px;">Sistema Gesti&oacute;n Competencias - Modificar Autorizacion</h3>
                  </div>
                    </div>
                    <div class="table-responsive-md">
                    <table class="table"> 
                      <tbody>
                      <tbody>
                      <tr>
                        <td style="padding-left: 20px;">Nombre Operador</td>
                        <td>{{rut_cbo}}</td>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>   
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Area</td>
                        <td>{{area}}</td>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th> 
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Grupo</td>
                        <td>{{grupo}}</td>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th> 
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Equipo *</td>
                        <td colspan="2" style="width: 450px;padding-right: 120px;">
                        <treeselect v-model="maquinaria_cbo"
                           :options="maquinarias" 
                           :disable-branch-nodes="true"
                           :show-count="true"
                           
                           />
                        </td>
                        <th scope="col"></th> 
                        <th scope="col"></th>  
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Tipo De Autorizaci&oacute;n *</td>
                        <td><select v-model="tipo_autorizacion_cbo" required="">
                                  <option :value="null" disabled>--Seleccione la Autorizaci&oacute;n--</option>
                                  <option value="A">Autorizaci&oacute;n</option>
                                  <option value="E">Evaluacion</option>
                                  <option value="R">Re-Evaluacion</option>
                                  <option value="V">Validaci&oacute;n</option>
                                  <option value="RI">Reinstrucci&oacute;n</option>
                            </select>
                        </td>          
                          <th scope="col">Operacion Invierno </th>                       
                          <th scope="col"><label for="si"> SI&nbsp;</label><input type="radio" id="one" value="SI" v-model="operacion_invierno">
                          </th>
                          <th scope="col"><label for="no"> NO&nbsp;</label><input type="radio" id="two" value="NO" v-model="operacion_invierno">
                          </th> 
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Fecha Autorizaci&oacute;n *</td>
                        <td><vuejs-datepicker  id="fecha" v-model="fecha" format='dd-MM-yyyy'></vuejs-datepicker></td>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th> 
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Programa </td>
                          <td colspan="2" style="width: 450px;padding-right: 120px;">
                        <treeselect v-model="programa_cbo"
                           :options="programas" 
                           :disable-branch-nodes="true"
                           :show-count="true"
                           />
                        </td>
                        <th scope="col"></th> 
                        <th scope="col"></th> 
                      </tr>
                      <tr v-if="tipo_autorizacion_cbo === 'A' || tipo_autorizacion_cbo === 'V'">
                        <td style="padding-left: 20px;">Vigencia *</td>
                        <td><select v-model="vigencia_cbo" required="">
                                  <option :value="null" disabled>--Seleccione la Vigencia--</option>
                                  <option value="1">1 Año</option>
                                  <option value="2">2 Años</option>
                                  <option value="3">3 Años</option>
                                  <option value="4">4 Años</option>
                                  <option value="5">5 Años</option>
                                  <option value="6">6 Años</option>
                                  <option value="7">7 Años</option>
                                  <option value="8">8 Años</option>
                                  <option value="9">9 Años</option>
                                  <option value="10">10 Años</option>
                                  <option value="11">mas de 10 Años</option>
                            </select>
                        </td>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th> 
                      </tr>
                      <tr>
                        <td style="padding-left: 20px;">Autorizador</td>
                        <td><select v-model="monitores_cbo">
                                  <option :value="null" disabled>--Seleccione un Autorizador--</option>
                                  <option value="ninguno" >--Otro Monitor--</option>
                                  <option v-for="monitor in monitores" :value=monitor.rut>{{monitor.nombre}}</option> 
                            </select>
                        </td>
                          <th scope="col"></th>
                          <th scope="col"></th>
                          <th scope="col"></th> 
                      </tr>
                      <tr>
                        <td></td>
                        <td><button v-on:click="insertar" class="btn btn-primary mb-2">Guardar</button></td> 
                       </tr> 
                      </tbody>
                      <tbody></tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript"> 

Vue.component("v-select", VueSelect.VueSelect);
Vue.component('treeselect', VueTreeselect.Treeselect);
var app6 = new Vue({
  el: '#app',
   mounted:function(){
     this.obtenerAPI()
   },
  data() {
        return {
            fecha: new Date(), 
            results: [],
            fecha_autorizacion_equipo:[],
            rut_cbo : '',
            operadores:[],
            maquinarias:[],
            programas:[],
            monitores:[],
            selected: 0,
            options: [],
            maquinaria_cbo:null,
            programa_cbo:null,
            monitores_cbo:null,
            vigencia_cbo:null,
            programa_cbo:null,
            tipo_autorizacion_cbo:null,
            operacion_invierno:'NO',
            area:'',
            grupo:''
        };
    },

    watch: {
      'tipo_autorizacion_cbo': function(val) 
      {
      
          if(this.maquinaria_cbo != null && this.tipo_autorizacion_cbo == 'A')
           {

              this.vigencia_cbo=null;
           }     
        },
        'maquinaria_cbo': function(val) 
      {
      
          if(this.maquinaria_cbo != null && this.tipo_autorizacion_cbo == 'A')
           {
              this.vigencia_cbo=null;
           }     
        }
      },
  components: {
    vuejsDatepicker
  },
    methods:{
            obtenerAPI ()
            {
                var prodId = getParameterByName('id');
                     function getParameterByName(name) 
                     {
                        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                        results = regex.exec(location.search);
                        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
                      }

                      axios.get('<%=request.getContextPath()%>/forms/AutorizacionesEdit.json?id_autorizacion='+prodId)
                .then(response => {

                    console.log(JSON.stringify(response.data[0].rut).replace(/"/g, ""));
                    this.rut_cbo=JSON.stringify(response.data[0].rut).replace(/"/g, "");
                    this.area=JSON.stringify(response.data[0].area).replace(/"/g, "");
                    this.grupo=JSON.stringify(response.data[0].grupo_turno).replace(/"/g, "");
                    this.maquinaria_cbo=JSON.stringify(response.data[0].id_maquinaria).replace(/"/g, "");
                    this.tipo_autorizacion_cbo=JSON.stringify(response.data[0].tipo_autorizacion).replace(/"/g, "");
                    this.vigencia_cbo=JSON.stringify(response.data[0].fecha_termino).replace(/"/g, "");
                    this.programa_cbo=JSON.stringify(response.data[0].id_programa).replace(/"/g, "");
                    this.monitores_cbo=JSON.stringify(response.data[0].autorizador_por).replace(/"/g, "");
                    this.fecha=JSON.stringify(response.data[0].fecha_inicio).replace(/"/g, "");
                    this.operacion_invierno=JSON.stringify(response.data[0].operacion_invierno).replace(/"/g, "");

                    })
                    .catch(function (error) {
                    });

                    axios.get('<%=request.getContextPath()%>/forms/BuscadorMaquinarias.json')
                .then(response => {
                    this.maquinarias = response.data;
                    this.maquinaria_cbo=this.maquinaria_cbo;
                    } )
                .catch(error => {});

                    axios.get('<%=request.getContextPath()%>/forms/BuscadorProgramas.json')
                .then(response => {
                    this.programas = response.data;
                    this.programa_cbo=this.programa_cbo;
                    } )
                .catch(error => {});

                    axios.get('<%=request.getContextPath()%>/forms/BuscadorMonitores.json')
                .then(response => {
                    this.monitores = response.data;
                    this.monitores_cbo=this.monitores_cbo;
                    } )
                .catch(error => {});
                    /*
                    axios.get('<%=request.getContextPath()%>/forms/BuscadorGrupos.json')
                .then(response => {
                    this.grupo = response.data;
                    this.area_grupo_cbo=this.area_grupo_cbo;
                    } )
                .catch(error => {});
                  */
            
            },

       insertar: function(){
        var validacion=true;
        var prodId = getParameterByName('id');
                     

                     function getParameterByName(name) 
                     {
                        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                        results = regex.exec(location.search);
                        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
                      }
        function formatDate(date) {
                    var d = new Date(date),
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();

                    if (month.length < 2) month = '0' + month;
                    if (day.length < 2) day = '0' + day;

                    return [year, month, day].join('-');
                    }  
        
        if (this.maquinaria_cbo == "null" || this.maquinaria_cbo == null ) 
        {
          alert("Debe Seleccionar un Equipo");
          validacion=false;

        }
        else
        {
           if(this.tipo_autorizacion_cbo == "null" || this.tipo_autorizacion_cbo == null )
          {
            alert("Debe Seleccionar El tipo de Autorización");
            validacion=false;
          }
          else
          {
            if(this.tipo_autorizacion_cbo == "A")
              {
                if(this.vigencia_cbo == "null" || this.vigencia_cbo == null )
                  {
                    alert("Debe Seleccionar la Vigencia");
                    validacion=false;
                  }
                  
              }
          }
 
        }
          if(validacion)
          {
              if(this.programa_cbo == "null" || this.programa_cbo == null )
              {
                this.programa_cbo=1;
              }

            axios.get('<%=request.getContextPath()%>/forms/insertModAutorizacion.json?fecha='+formatDate(this.fecha)+"&maquinaria="+this.maquinaria_cbo+"&monitor="+this.monitores_cbo+"&vigencia="+this.vigencia_cbo+"&tipo_autorizacion="+this.tipo_autorizacion_cbo+"&ope_invierno="+this.operacion_invierno+"&programa="+this.programa_cbo+"&id_autorizacion="+prodId)
                .then(function(response){
                  //console.log(response);
                alert("Se modifica la autorizacion");
                window.location = "<%=request.getContextPath()%>/forms/formReporteAutorizaciones.html";
              })
              .catch(function(){
                alert("Problemas al Modificar la autorizacion");
              });
           }     
      }
  }
  })

</script>


