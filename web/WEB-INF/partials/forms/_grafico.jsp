<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core'      prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt'       prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div id="app">
  
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-2">
                            <h4 class="card-title mb-0">Graficos Caudales</h4>    
                    </div>
                    <div class="col-sm-4">
                      <div class="form-inline">
                      <div class="form-group mx-sm-3 mb-2">
                        <label>Fecha De B&uacute;squeda</label>
                        <vuejs-datepicker  id="fecha" v-model="fecha" format='dd-MM-yyyy'> </vuejs-datepicker>
                      </div>
                      <button v-on:click="buscar" class="btn btn-primary mb-2">Buscar</button>
                    </div> 
                  </div>
                </div>
              <div id="chart-wrapper" class="chart-wrapper">
                    <canvas id="myChart" ></canvas>
            </div>
          </div>
        </div>   
      </div>
    </div>
</div>

<script type="text/javascript"> 

      Array.prototype.randomElement = function () 
      {
        return this[Math.floor(Math.random() * this.length)]
      }

      var app6 = new Vue({
      el: '#app',
      data: 
      {
        fecha: new Date(),
        estaciones: []

      },
      components: 
      {
      vuejsDatepicker

      },
      methods:{
        buscar: function(){
                axios.get('<%=request.getContextPath()%>/forms/reporteCaudalGrafico.json?fecha='+this.fecha)
                .then(response => {

      let chartColors = [
       'rgb(255, 99, 132)',
       'rgb(255, 159, 64)',
       'rgb(255, 205, 86)',
       'rgb(75, 192, 192)',
       'rgb(54, 162, 235)',
       'rgb(153, 102, 255)',
       'rgb(201, 203, 207)'
      ];

                  console.log(response.data);

                  let g_labels = [];
                  let g_datasets = [];
                  for (let i=0;i<response.data.length;i++)
                   {
                      
                     var estacion = response.data[i];
                     let caudal_medio = [];
                     let caudal_limite = [];
                      
                    for ( j = 0 ; j < estacion.definitions.length; j++)
                    { 
                        if ( i == 0 )
                        {
                            g_labels.push(estacion.definitions[j].fecha)
                        }

                        caudal_medio[j] = estacion.definitions[j].caudal_medio;
                        caudal_limite[j] = estacion.definitions[j].limite_caudal;

                     }
                    let color1 = chartColors[Math.floor(Math.random() * chartColors.length)];
                    let dataset_1 = {};
                    dataset_1.label = 'Estacion ' + estacion.nombre;
                    dataset_1.backgroundColor =  color1;
                    dataset_1.borderColor = color1;
                    dataset_1.fill=  false;
                    dataset_1.data = caudal_medio; 

                    console.log(color1); 
                    let color2 = chartColors[Math.floor(Math.random() * chartColors.length)];
                    let dataset_2 = {};
                    dataset_2.label = 'Limite medio  ' + estacion.nombre;
                    dataset_2.backgroundColor =  color2;
                    dataset_2.borderColor = color2;
                    dataset_2.fill=  false;
                    dataset_2.data = caudal_limite; 
                    dataset_2.borderDash = [5, 15];
                    console.log(color2); 

                    g_datasets.push(dataset_1); g_datasets.push(dataset_2);
                   }

                  console.log(g_labels); 

            var cfg = {
              
              type: 'line',
              data: {
                labels: g_labels,
                datasets: g_datasets
              },
              options: {
                responsive: true,
                maintainAspectRatio:true,
                title: {
                  display: true,
                  text: 'Caudales'
                },
                scales: {
                  xAxes: [{
                    display: true,
                    type: "category",
                     tticks: {
                           callback: function(tick, index, ticks) 
                           {          
                            return tick.toString();        }
                }

                  }],
                  yAxes: [{
                    display: true,
                    type: 'linear',
                    distribution: 'series',
                     ticks: {
                           callback: function(tick, index, ticks) 
                           {          
                            return tick.toString();        }
                }
                  }]
                }
              }
            };

 
            var ctx = document.getElementById("myChart").getContext('2d');
            var myChart = new Chart(ctx, cfg );
            console.log(this.estaciones);
            })
                  .catch(function (error) {
                  console.log(error);
            });  
            }
          }
        })

    </script>
  <script>
</script>