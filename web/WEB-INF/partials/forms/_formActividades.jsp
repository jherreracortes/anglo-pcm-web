<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core'      prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt'       prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div id="app">
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                      <h3 class="card-title mb-0" style="padding-left: 20px;">Sistema Gesti&oacute;n Competencias - Actividades </h3>
                        <div class="col-sm-4">
                        </div>
                        </div>
                    </div>
                    <div>
                    <div class="table-responsive-md">
                    <table class="table">  
                    <thead>
                        <tr> 
                          <th scope="col">Actividad</th>
                          <th scope="col">Horas</th>
                          <th scope="col">Programa</th>
                          <th scope="col">Vigencia Actividad</th>
                          <th scope="col">Acciones</th>
                          <th scope="col"></th>
                          <th scope="col"
                            style="
                            padding-top: 12px;
                            padding-right: 12px;
                            padding-bottom: 12px;
                            padding-left: 12px;
                            "
                            >
                              <button v-on:click="agregar_actividad" class="btn btn-primary">Agregar Actividad</button>
                            </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr  v-for="actividad in actividades">
                            <td>{{actividad.nombre_actividad}}</td>
                            <td>{{actividad.horas}}</td>
                            <td>{{actividad.nombre_programa}}</td>
                            <td>{{actividad.estado_actividad}}</td>
                            <!--
                            <td ><a :href="'<%=request.getContextPath()%>/forms/formEditActividad.html?id_actividad='+actividad.id_actividades"> Editar</td> <td ><a :href="'<%=request.getContextPath()%>/forms/formProgramaEdit.html?id_programa='+actividad.id_actividades"> Agregar sesi&oacute;n</td>-->
                        </tr>
                      </tbody>
                    </table>
                  </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript"> 
var app6 = new Vue({
  el: '#app',
  data:{
    actividades: []
  },
  created: function () {
    this.obtenerAPI()
    
  },
    methods:{
      obtenerAPI ()
       {
                axios.get('<%=request.getContextPath()%>/forms/ReporteActividades.json')
                .then(response => {
                    this.actividades = response.data;
                    })
                    .catch(function (error) {
                    });
        },

        agregar_actividad: function(){


              window.location = "<%=request.getContextPath()%>/forms/formNewActividad.html";

          }
          }
        })

</script>


