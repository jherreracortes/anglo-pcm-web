<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core'      prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt'       prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div id="app">
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                      <h3 class="card-title mb-0" style="padding-left: 20px;">Sistema Gesti&oacute;n Competencias - Edicion Programa</h3>
                        <div class="col-sm-4">
                        </div>
                        </div>
                    </div>
                    <div>
                    <div class="table-responsive-md">
                    <table class="table">  
                    <thead v-for="programa in programas">
                      <div>
                        <tr > 
                          <th scope="col">Nombre Programa</th>
                          <th scope="col"><input type="text" v-model="programa.nombre"></th>
                        </tr>
                        <tr > 
                          <th scope="col">Estado Vigencia</th>
                          <th scope="col">
                          <div v-for="answer in ['ACTIVO', 'INACTIVO']">
                                    <input 
                                      type="radio" 
                                      name="answer" 
                                      :id="answer"
                                      :value="answer"
                                      v-model="programa.estado_programa">
                                    {{ answer }}
                                </div></th> 
                        </tr>
                         <tr>
                          <td><button v-on:click="modificar(programa)" class="btn btn-primary">Modificar</button></td>
                        </tr>
                      </div>
                      </thead>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript"> 

var app6 = new Vue({
  el: '#app',
   mounted:function(){
     this.obtenerAPI()
   },
  data:{
    programas:[{
    nombre:'',
    estado_programa:''
    }],
    
    form : {}
  },
    methods:{
      obtenerAPI ()
       {

                var prodId = getParameterByName('id_programa');
                     

                     function getParameterByName(name) 
                     {
                        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                        results = regex.exec(location.search);
                        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
                      }

                      axios.get('<%=request.getContextPath()%>/forms/ProgramaEdit.json?id_programa='+prodId)
                .then(response => {

                    this.programas = response.data;
                    })
                    .catch(function (error) {
                    });

            },

       modificar: function(data){


               var prodId = getParameterByName('id_programa');
                     

                     function getParameterByName(name) 
                     {
                        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                        results = regex.exec(location.search);
                        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
                      }
              
              axios.get('<%=request.getContextPath()%>/forms/insertModPrograma.json?id_programa='+prodId+"&nombre="+data.nombre+"&vigencia="+data.estado_programa)
                .then(function(response){

                alert("Se realiza modificacion al Programa");
                window.location = "<%=request.getContextPath()%>/forms/formPrograma.html";
              })
              .catch(function(){
                alert("Problemas al modificar el Programa");

              });
                 
            
          }
          }
        })

</script>


