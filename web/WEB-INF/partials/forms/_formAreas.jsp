<%@ page pageEncoding="UTF-8" %>
<%@ page session="true"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core'      prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt'       prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div id="app">
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                      <h3 class="card-title mb-0" style="padding-left: 20px;">Sistema Gesti&oacute;n Competencias - Area </h3>
                        <div class="col-sm-4">
                        </div>
                        </div>
                    </div>
                    <div>
                    <div class="table-responsive-md">
                    <table class="table">  
                    <thead>
                        <tr> 
                          <th scope="col">Area</th>
                          <th scope="col">Acciones</th>
                          <th scope="col"></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr  v-for="area in areas">
                            <td class="col-sm-4">{{area.label}}</td>
                            <td class="col-sm-4"><a :href="'<%=request.getContextPath()%>/forms/formReporteEquipos.html?id_area='+area.id"> Equipos</td>
                            <td class="col-sm-4"><a :href="'<%=request.getContextPath()%>/forms/formGruposOperarios.html?id_area='+area.id"> Cargos</td>
                        </tr>
                      </tbody>
                    </table>
                  </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript"> 
var app6 = new Vue({
  el: '#app',
  data:{
    areas: []
  },
  created: function () {
    this.obtenerAPI()
    
  },
    methods:{
      obtenerAPI ()
       {
                axios.get('<%=request.getContextPath()%>/forms/reporteAreas.json')
                .then(response => {
                    this.areas = response.data;
                    })
                    .catch(function (error) {
                    });
            }
          }
        })

</script>


