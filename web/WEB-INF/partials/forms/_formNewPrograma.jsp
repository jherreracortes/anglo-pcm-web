<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core'      prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt'       prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div id="app">
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                      <h3 class="card-title mb-0" style="padding-left: 20px;">Sistema Gesti&oacute;n Competencias - Nuevo Programa </h3>
                        <div class="col-sm-4">
                        </div>
                        </div>
                    </div>
                    <div>
                    <div class="table-responsive-md">
                    <table class="table">  
                    <thead >
                      <div>
                        <tr > 
                          <th scope="col">Nombre Programa</th>
                          <th scope="col"><input type="text" v-model="nombre_programa" ref="nombre_programa"></th>
                        </tr>
                        <tr > 
                          <th scope="col">Estado Vigencia</th>
                          <th scope="col">
                          <div v-for="answer in ['ACTIVO', 'INACTIVO']">
                                    <input 
                                      type="radio" 
                                      name="answer" 
                                      :id="answer"
                                      :value="answer"
                                      v-model="estado_vigencia_programa">
                                    {{ answer }}
                                </div></th> 
                        </tr>
                         <tr>
                          <td><button v-on:click="agregar_programa" class="btn btn-primary">Agregar Nuevo Programa</button></td>
                        </tr>
                      </div>
                      </thead>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript"> 

var app6 = new Vue({
  el: '#app',
  data:{
    nombre_programa:'',
    estado_vigencia_programa:'ACTIVO',
    selected: 0,
  },
  
    methods:{
       agregar_programa: function(){

          if(this.nombre_programa.lenght > 0 || this.nombre_programa !='')
          {
              
              axios.get('<%=request.getContextPath()%>/forms/insertNewPrograma.json?nombre='+this.nombre_programa+"&validez="+this.estado_vigencia_programa)
                .then(function(response){

                alert("Se creo el nuevo Programa");
                window.location = "<%=request.getContextPath()%>/forms/formPrograma.html";
              })
              .catch(function(){
                alert("Problemas al ingresar el nuevo Programa");

              });
          }
        else
        {
          alert("Debe ingresar un nombre para asignarle al Programa");
          this.$refs.nombre_programa.focus();
        }
      }

    }
  })

</script>


