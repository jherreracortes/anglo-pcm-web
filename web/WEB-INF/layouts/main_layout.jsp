<%@page pageEncoding="UTF-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiaxa" tagdir="/WEB-INF/tags"%>
<html>
<tiaxa:head_tag />
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
   <header class="app-header navbar">
      <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="#">
        <img class="navbar-brand-full" src="<%=request.getContextPath()%>/img/brand/logo.png" width="89" height="25" alt="CoreUI Logo">
        <img class="navbar-brand-minimized" src="<%=request.getContextPath()%>/img/brand/sygnet.png" width="30" height="30" alt="CoreUI Logo">
      </a>
      <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
      </button>
        <ul class="nav navbar-nav d-md-down-none"></ul>
        <ul class="nav navbar-nav ml-auto"></ul>
      <!--<ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item px-3">
          <a class="nav-link" href="#">Inicio</a>
        </li>
        <li class="nav-item px-3">
          <a class="nav-link" href="#">Users</a>
        </li>
        <li class="nav-item px-3">
          <a class="nav-link" href="#">Settings</a>
        </li>
      </ul>-->
      <ul class="nav navbar-nav ml-auto">
        <!--<li class="nav-item d-md-down-none">
          <a class="nav-link" href="#">
            <i class="icon-bell"></i>
            <span class="badge badge-pill badge-danger">5</span>
          </a>
        </li>
        <li class="nav-item d-md-down-none">
          <a class="nav-link" href="#">
            <i class="icon-list"></i>
          </a>
        </li>
        <li class="nav-item d-md-down-none">
          <a class="nav-link" href="#">
            <i class="icon-location-pin"></i>
          </a>
        </li>-->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <img class="img-avatar" src="<%=request.getContextPath()%>/img/avatars/7.jpg" alt="admin@bootstrapmaster.com">
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-header text-center">
              <strong>Opciones</strong>
            </div>
            <a class="dropdown-item" href="#">
              <i class="fa fa-user"></i> Perfil</a>
            <a class="dropdown-item" href="<%=request.getContextPath()%>/logout.shtml">
              <i class="fa fa-lock"></i> Logout</a>
          </div>
        </li>
      </ul>
      <!--<button class="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <button class="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
        <span class="navbar-toggler-icon"></span>
      </button>-->
    </header>
    <div class="app-body">
        <div class="sidebar">
            <nav class="sidebar-nav">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="<%=request.getContextPath()%>/forms/formAreas.html">
                             Areas
                         </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<%=request.getContextPath()%>/forms/formGruposOperarios.html">
                             Operadores
                         </a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link" href="<%=request.getContextPath()%>/forms/formMonitor.html">
                             Monitores
                         </a>
                    </li>  
                    <li class="nav-item">
                        <a class="nav-link" href="<%=request.getContextPath()%>/forms/formReporteEquipos.html">
                             Equipos
                         </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<%=request.getContextPath()%>/forms/formReporteAutorizaciones.html">
                             Autorizaciones
                         </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<%=request.getContextPath()%>/forms/formPrograma.html">
                             Programas
                         </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<%=request.getContextPath()%>/forms/formActividades.html">
                             Actividades
                         </a>
                    </li>
                </ul>
            </nav>
        </div>
        <main class="main">
            <c:out value='${contentForLayout__}' escapeXml="false" />
        </main>
    </div>
</body>
</html>
