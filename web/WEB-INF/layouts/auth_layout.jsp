<%@page pageEncoding="UTF-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="tiaxa" tagdir="/WEB-INF/tags"%>
<html>
<tiaxa:head_tag />

<body class="app flex-row align-items-center">

  <div class="container">
      <div class="row justify-content-center">
          <div class="col-md-8">
              <div class="card-group">
                  <div class="card p-4">
                      <div class="card-body">
                        <main class="main">
                          <c:out value='${contentForLayout__}' escapeXml="false" />
                        </main>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <script>
  var form = new Vue({
    el: '#form',
    data: {
      email: '',
      attemptSubmit: false,
    },
    computed: {
      missingEmail: function () { return this.email === ''; },
      missingPassword: function () { return this.password === ''; },
    },
    methods: {
      isNumeric: function (n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
      },

      validateForm: function (event) {
        this.attemptSubmit = true;
        if (this.missingEmail || this.missingPassword) event.preventDefault();
      },
    },
  });
  </script>
</body>
</html>
