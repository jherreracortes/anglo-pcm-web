<%@page pageEncoding="UTF-8" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt'%>
<!--<%@ taglib prefix="tiaxa" tagdir="/WEB-INF/tags" %>-->
<html>
<tiaxa:head_tag />

<body class="app flex-row align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card-group">
                    <div class="card p-4">
                        <div class="card-body">
                            <form method="POST" name="logform" id="logform" role="form" action="<%=request.getContextPath()%>/index.html">
                                <h1>Login</h1>
                                <p class="text-muted">Sign In to your account</p>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="icon-envelope-letter"></i>
                                        </span>
                                    </div>
                                    <input type="email" class="form-control" name="j_username" placeholder="Username">
                                </div>
                                <div class="input-group mb-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="icon-lock"></i>
                                        </span>
                                    </div>
                                    <input type="password" class="form-control" name="j_password" placeholder="Password">
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <button onclick="document.logform.submit();" type="button" class="btn btn-primary px-4">Login</button>
                                    </div>
                                    <div class="col-6 text-right">
                                        <a class="nav-link" href="auth/emailpassword.html">Forgot password?</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

<!--<body class="gray-bg">

	<div class="middle-box text-center loginscreen animated fadeInDown">

		<div>
			<div>
				<h2 class="logo-name">Omnia-Bot</h2>
			</div>
			<h3>Bienvenido a Omnia-Bot</h3>

			<form method="POST" name="logform" id="logform" class="m-t" role="form" action="<%=request.getContextPath()%>/index.html">
				<div class="form-group">
					<input type="email" class="form-control" placeholder="Username" name="j_username" size="13" required="" />
				</div>
				<div class="form-group">
					<input type="password"	class="form-control"  placeholder="Password" name="j_password" size="13" required="" />
				</div>
				<button onclick="document.logform.submit();" class="btn btn-primary block full-width m-b">Entrar</button>
				<a href="#"><small>Olvidó su Clave?</small></a>
			</form>

		</div>
	</div>

	<script src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
	<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>

</body>-->
</html>
