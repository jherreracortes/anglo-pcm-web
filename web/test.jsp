<%@page pageEncoding="UTF-8" %>
<%@ page import="java.util.*" %>
<%@ page import="java.math.*" %>

<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt'%>
<%@ taglib prefix="tiaxa" tagdir="/WEB-INF/tags/" %>
<%@ taglib uri="/WEB-INF/tags/functions.tld" prefix="fntx" %>

<% request.setAttribute("bb", new BigDecimal(19922990000000L)); %>

<!DOCTYPE html>
<html>
<head>
<body>
<div>
Amount: <c:out value="${fntx:formatBigDecimal(bb)}" />
</div>
</body>
</html>
