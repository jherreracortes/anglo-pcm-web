/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tiaxa.controllers;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface WebAction {
    String actionName();
    String author() default "[no-author]";
    String date()    default "[no-date]";
    String description() default "[no-data]";
    boolean isAutaditable() default false;
}
