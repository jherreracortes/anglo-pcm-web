package com.tiaxa.controllers;

import com.tiaxa.util.CharArrayWriterResponse;
import java.io.IOException;
import java.lang.reflect.Method;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import java.io.StringWriter;
import java.io.PrintWriter;
import com.tiaxa.login.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract class AbstractController extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(AbstractController.class);
  static final String CONTENT_FOR_LAYOUT = "contentForLayout__";
  static final String TAS_TAG = "TAS_TAG";
  public static String TAS_Module_Key = null;
  //public String[] TAS_Module_Key  = null;

  public boolean requireRestrictedAccess() { return true;}

	@Override
    public void init(ServletConfig config)
    throws ServletException
    {
        super.init(config);
        TAS_Module_Key  = this.getServletConfig().getInitParameter(TAS_TAG);
        log.debug("TAS_Module_Key " + TAS_Module_Key);
    }

    /****************************************************************************/
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
		if (this.requireRestrictedAccess()) {
			UserContext userCtx = FormLoginFilter.getUserContext(request.getSession());

			if (userCtx != null ) {
				log.debug("TAS_Module_Key " + TAS_Module_Key + " USER " + userCtx);

				/*if (userCtx.requireModule(TAS_Module_Key) == false) {

					method403(request, response); // not allowed
					return;
				}*/
			}
		}

        log.info("    String uri " + request.getAttribute("javax.servlet.forward.request_uri"));


        for (Method method : this.getClass().getDeclaredMethods())
        {
            // checks if there is annotation present of the given url
            if (method.isAnnotationPresent(WebAction.class))
            {
                /* iterates all the annotations available in the method
                for (Annotation annotation : method.getDeclaredAnnotations())
                {
                    log.info("Annotation in Method '" + method+ "' : " + annotation);
                }
		*/
                WebAction webaction = method.getAnnotation(WebAction.class);
                if ((request.getRequestURL().toString()).endsWith(webaction.actionName()))
                {
                    try
                    {
                    	log.debug("method captured ! "+ method);
                        method.invoke(this, request, response);
	                if ( webaction.isAutaditable() )
				log.info("metodo auditable");
                	log.debug("execution ok");
			return;
                    }
		    catch (Throwable ex)
		    {
			    StringWriter sw = new StringWriter();
			    PrintWriter pw = new PrintWriter(sw);
			    ex.printStackTrace(pw);
			    request.setAttribute("messageEx", sw.toString());
			    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			    renderView(request, response, "_500", null);
			    log.error(" Error ejecutando metodo de controlador" , ex );
			    return;
		    }
                }
            }
        }

	method404(request, response);
    }

   protected void method404(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
	    renderView(request, response, "_404", null );
    }
   protected void method403(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
	 response.setStatus(HttpServletResponse.SC_FORBIDDEN);
	    renderView(request, response, "_403", null);
    }



    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

protected static void renderView(HttpServletRequest request, HttpServletResponse response, String layout, String partialView) throws ServletException, IOException
{
			request.setAttribute(CONTENT_FOR_LAYOUT, renderAsString(request, response, partialView));
	    RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/layouts/" + layout + ".jsp");
	    log.debug("[renderView] /WEB-INF/layouts/" + layout + ".jsp");
	    rd.forward(request, response);
}

protected static void renderFile(HttpServletRequest request, HttpServletResponse response, String partialView) throws ServletException, IOException
{
            (response.getWriter()).println(renderAsString(request,response,partialView));

}
protected static void renderPartial(HttpServletRequest request, HttpServletResponse response, String partialView) throws ServletException, IOException
{
	    response.setContentType("text/html; charset=UTF-8;");
            (response.getWriter()).println(renderAsString(request,response,partialView));

}

protected static String renderAsString(HttpServletRequest request, HttpServletResponse response, String partialView) throws ServletException, IOException
{

	    if ( partialView == null || partialView.length() < 1 )
		return "";
	    CharArrayWriterResponse customResponse  = new CharArrayWriterResponse(response);
	    request.getRequestDispatcher("/WEB-INF/partials/" + partialView+ ".jsp").include(request, customResponse);
	    return customResponse.getOutput();
}

    public static void renderAsJSON(HttpServletRequest request, HttpServletResponse response, String output) throws ServletException, IOException
    {
            if ( output == null || output.length() < 1 )
                    return ;
            HttpSession session = request.getSession();
            ServletContext sc = session.getServletContext();
            //response.setContentType("application/json;charset=utf-8");
            response.setContentType("text/json; charset=iso-8859-1;");
            //response.setCharacterEncoding("UTF-8");
            response.setContentLength(output.length());
            (response.getWriter()).println(output);
            return;
    }


}
