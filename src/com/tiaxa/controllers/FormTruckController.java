package com.tiaxa.controllers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletConfig;
import com.tiaxa.login.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.Part;
import java.util.Scanner;
import java.util.HashMap;
import java.util.List;
import java.util.Arrays;

import com.tiaxa.common.*;
import com.tiaxa.model.*;
import com.tiaxa.util.*;
import java.util.Properties;
import java.io.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FormTruckController extends AbstractController {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unused")
	private static Logger log = LoggerFactory.getLogger(FormTruckController.class);

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	@Override
	public boolean requireRestrictedAccess() {
		return false;
	}
	/****************************************************************************/
	@WebAction(actionName = "formTruck.html")
	protected static void formTruck(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
    	renderView(request, response, "main_layout", "/forms/_formTruck");
	}

    @WebAction(actionName = "formAreas.html")
    protected static void formAreas(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        /*HttpSession session       = request.getSession();
        System.err.println("formAreas areas json: "+session.getAttribute("username"));
        if(session.getAttribute("username") == null )
        {   
            log.debug("Redirecciona al login no tiene session iniciada");
            RequestDispatcher rdObj = null;
            rdObj = request.getRequestDispatcher("/logout.shtml");
            rdObj.forward(request, response);
        }
        else    
        {
            */
            renderView(request, response, "main_layout", "/forms/_formAreas");
        //}
        
    }

    @WebAction(actionName = "formReporteAutorizaciones.html")
    protected static void formReporteAutorizaciones(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        /*HttpSession session       = request.getSession();
        System.err.println("formAreas areas json: "+session.getAttribute("username"));
        if(session.getAttribute("username") == null )
        {   
            log.debug("Redirecciona al login no tiene session iniciada");
            RequestDispatcher rdObj = null;
            rdObj = request.getRequestDispatcher("/logout.shtml");
            rdObj.forward(request, response);
        }
        else    
        {   
            */
            renderView(request, response, "main_layout", "/forms/_formReporteAutorizaciones");
        //}
    }

    @WebAction(actionName = "formEditAutorizaciones.html")
    protected static void formEditAutorizaciones(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        /*HttpSession session       = request.getSession();
        //System.err.println("formAreas areas json: "+session.getAttribute("username"));
        if(session.getAttribute("username") == null )
        {   
            log.debug("Redirecciona al login no tiene session iniciada");
            RequestDispatcher rdObj = null;
            rdObj = request.getRequestDispatcher("/logout.shtml");
            rdObj.forward(request, response);
        }
        else    
        {
            */
            renderView(request, response, "main_layout", "/forms/_formEditAutorizaciones");
        //}
        
    }

    @WebAction(actionName = "formNewEquipo.html")
    protected static void formNewEquipo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        /*HttpSession session       = request.getSession();
        //System.err.println("formAreas areas json: "+session.getAttribute("username"));
        if(session.getAttribute("username") == null )
        {   
            log.debug("Redirecciona al login no tiene session iniciada");
            RequestDispatcher rdObj = null;
            rdObj = request.getRequestDispatcher("/logout.shtml");
            rdObj.forward(request, response);
        }
        else    
        {   */
            renderView(request, response, "main_layout", "/forms/_formNewEquipo");
         //}   
    }

    @WebAction(actionName = "formNewOperador.html")
    protected static void formNewOperador(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      
            renderView(request, response, "main_layout", "/forms/_formNewOperador");  
    }


    @WebAction(actionName = "formAutorizaciones.html")
    protected static void formAutorizaciones(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
            renderView(request, response, "main_layout", "/forms/_formAutorizaciones");
 
    }

    @WebAction(actionName = "formGruposOperarios.html")
    protected static void formGruposOperarios(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        
            renderView(request, response, "main_layout", "/forms/_formGruposOperarios");

    }

    @WebAction(actionName = "formReporteEquipos.html")
    protected static void formReporteEquipos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
            renderView(request, response, "main_layout", "/forms/_formReporteEquipos");   
    }
    
    @WebAction(actionName = "formReporteEquipoEdit.html")
    protected static void formReporteEquipoEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
            renderView(request, response, "main_layout", "/forms/_formReporteEquipoEdit");  
    }

    @WebAction(actionName = "formEditOperador.html")
    protected static void formEditOperador(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
            renderView(request, response, "main_layout", "/forms/_formEditOperador");    
    }

    @WebAction(actionName = "formNewPrograma.html")
    protected static void formNewPrograma(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
            renderView(request, response, "main_layout", "/forms/_formNewPrograma");   
    }

    @WebAction(actionName = "formNewActividad.html")
    protected static void formNewActividad(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
            renderView(request, response, "main_layout", "/forms/_formNewActividad");  
    }

    @WebAction(actionName = "formEditActividad.html")
    protected static void formEditActividad(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
            renderView(request, response, "main_layout", "/forms/_formEditActividad");  
    }

    @WebAction(actionName = "formPrograma.html")
    protected static void formPrograma(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
            renderView(request, response, "main_layout", "/forms/_formPrograma"); 
    }

    @WebAction(actionName = "formActividades.html")
    protected static void formActividades(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
            renderView(request, response, "main_layout", "/forms/_formActividades");  
    }

    @WebAction(actionName = "formProgramaEdit.html")
    protected static void formProgramaEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
            renderView(request, response, "main_layout", "/forms/_formProgramaEdit");
    }

    @WebAction(actionName = "formEditMonitor.html")
    protected static void formEditMonitor(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
            renderView(request, response, "main_layout", "/forms/_formEditMonitor");  
    }

    @WebAction(actionName = "formMonitor.html")
    protected static void formMonitor(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
            renderView(request, response, "main_layout", "/forms/_formMonitor");   
    }

    @WebAction(actionName = "reporteAreas.json")
    protected void reporteAreas(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String res = ReportModel.reporteAreas();

        renderAsJSON(request, response, res);
        
    }

    @WebAction(actionName = "ReporteProgramas.json")
    protected void ReporteProgramas(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String res = ReportModel.ReporteProgramas();
        //System.err.println("response"+res);
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "ReporteActividades.json")
    protected void ReporteActividades(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String res = ReportModel.ReporteActividades();
        //System.err.println("response"+res);
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "ReporteAutorizacion.json")
    protected void ReporteAutorizacion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Integer area = Integer.parseInt(request.getParameter("area"));
        Integer equipo = Integer.parseInt(request.getParameter("equipo"));
        String fecha = (String) request.getParameter("fecha");
        String fecha_termino = (String) request.getParameter("fecha_termino");
        String res = ReportModel.ReporteAutorizacion(area,equipo,fecha,fecha_termino);
        //System.err.println("response"+res);
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "ReporteAutorizacionMaquina.json")
    protected void ReporteAutorizacionMaquina(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Integer area = Integer.parseInt(request.getParameter("area"));
        Integer equipo = Integer.parseInt(request.getParameter("equipo"));
        Integer reporte = Integer.parseInt(request.getParameter("reporte"));
        String res = ReportModel.ReporteAutorizacionMaquina(area,equipo,reporte);
        //System.err.println("response"+res);
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "ReporteEquipos.json")
    protected void ReporteEquipos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Integer id_area= Integer.parseInt(request.getParameter("id_area"));
        String res = ReportModel.ReporteEquipos(id_area);
        //System.err.println("response"+res);
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "ReporteEquipoEdit.json")
    protected void ReporteEquipoEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Integer id_equipo= Integer.parseInt(request.getParameter("id_equipo"));
        String res = ReportModel.ReporteEquipoEdit(id_equipo);
        //System.err.println("response"+res);
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "ActividadEdit.json")
    protected void ActividadEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Integer id_actividad= Integer.parseInt(request.getParameter("id_actividad"));
        String res = ReportModel.ActividadEdit(id_actividad);
        //System.err.println("response"+res);
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "ProgramaEdit.json")
    protected void ProgramaEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Integer id_programa= Integer.parseInt(request.getParameter("id_programa"));
        String res = ReportModel.ProgramaEdit(id_programa);
        //System.err.println("response"+res);
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "ReporteCargos.json")
    protected void ReporteCargos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Integer id_area= Integer.parseInt(request.getParameter("id_area"));
        String res = ReportModel.ReporteCargos(id_area);
        //System.err.println("response"+res);
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "BuscadorOperadores.json")
    protected void BuscadorOperadores(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String fecha = (String) request.getParameter("fecha");
        String res = ReportModel.BuscadorOperadores(fecha);
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "BuscadorProgramas.json")
    protected void BuscadorProgramas(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        String res = ReportModel.BuscadorProgramas();
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "BuscadorTipoActividades.json")
    protected void BuscadorTipoActividades(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        String res = ReportModel.BuscadorTipoActividades();
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "BuscadorSolicitantes.json")
    protected void BuscadorSolicitantes(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        String res = ReportModel.BuscadorSolicitantes();
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "BuscadorOperadoresEdit.json")
    protected void BuscadorOperadoresEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String rut = (String) request.getParameter("rut");
        String res = ReportModel.BuscadorOperadoresEdit(rut);
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "BuscadorMaquinarias.json")
    protected void BuscadorMaquinarias(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String res = ReportModel.BuscadorMaquinarias();
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "BuscadorGrupos.json")
    protected void BuscadorGrupos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
 
        String res = ReportModel.BuscadorGrupos();
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "BuscadorGruposParaActividades.json")
    protected void BuscadorGruposParaActividades(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
 
        String res = ReportModel.BuscadorGruposParaActividades();
        renderAsJSON(request, response, res);
    }
     
    @WebAction(actionName = "CargosCBO.json")
    protected void CargosCBO(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
 
        String res = ReportModel.CargosCBO();
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "SuperintendenciaCBO.json")
    protected void SuperintendenciaCBO(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
 
        String res = ReportModel.SuperintendenciaCBO();
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "TramoCBO.json")
    protected void TramoCBO(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
 
        String res = ReportModel.TramoCBO();
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "BuscadorMonitores.json")
    protected void BuscadorMonitores(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        String res = ReportModel.BuscadorMonitores();
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "BuscadorMonitoresEdit.json")
    protected void BuscadorMonitoresEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String rut = (String) request.getParameter("rut");
        String res = ReportModel.BuscadorMonitoresEdit(rut);
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "MonitorEdit.json")
    protected void MonitorEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String rut = (String) request.getParameter("rut");
        String res = ReportModel.MonitorEdit(rut);
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "AutorizacionesEdit.json")
    protected void AutorizacionesEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Integer id_autorizacion = Integer.parseInt(request.getParameter("id_autorizacion"));
        String res = ReportModel.AutorizacionesEdit(id_autorizacion);
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "ReporteMonitoresEdit.json")
    protected void ReporteMonitoresEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String res = ReportModel.ReporteMonitoresEdit();
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "insertAutorizacion.json")
    protected  void insertAutorizacion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String fecha = (String) request.getParameter("fecha");
        String rut_operador = (String) request.getParameter("operador");
        Integer id_maquinaria = Integer.parseInt(request.getParameter("maquinaria"));
        String rut_monitor = (String) request.getParameter("monitor");
        String vigencia = (String) request.getParameter("vigencia");
        String tipo_autorizacion = (String) request.getParameter("tipo_autorizacion");
        String operacion_invierno = (String) request.getParameter("ope_invierno");
        Integer programa = Integer.parseInt(request.getParameter("programa"));
        String mde= (String) request.getParameter("mde");
        HttpSession session       = request.getSession();
        String usuario = (String) session.getAttribute("username");

        Integer res = ReportModel.insert_autorizacion(fecha,rut_operador,id_maquinaria,rut_monitor,vigencia,tipo_autorizacion,operacion_invierno,programa,usuario,mde);
        if(res != 1)
        {
            method403(request, response);
        }
       
    }

    @WebAction(actionName = "insertOperador.json")
    protected  void insertOperador(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        
        String rut_operador = (String) request.getParameter("rut_operador");
        String area_grupo = (String) request.getParameter("area_grupo");
        String nombre_operador = UpperString((String) request.getParameter("nombre").trim());
        String app_pat_operador = UpperString((String) request.getParameter("app_paterno").trim());
        String app_mat_operador = UpperString((String) request.getParameter("app_materno").trim());
        Integer sap_operador = Integer.parseInt(request.getParameter("sap"));
        String estado_vigencia_operador = (String) request.getParameter("estado_vigencia");
        String tipo_de_operador = (String) request.getParameter("empleado_por");
        String fecha_nacimiento = (String) request.getParameter("fecha_nacimiento");
        String cargo_operador = (String) request.getParameter("cargo_operador").trim();
        String superintendencia = (String) request.getParameter("superintendencia").trim();
        Integer tramo = Integer.parseInt(request.getParameter("tramo"));
        String fecha_ingreso = (String) request.getParameter("fecha_ingreso");
        String genero_ope =(String) request.getParameter("genero");
        String[] area_and_grupo_ope = area_grupo.split(";");
        String area_ope=area_and_grupo_ope[0];
        String grupo_ope=area_and_grupo_ope[1];
        HttpSession session       = request.getSession();
        String usuario = (String) session.getAttribute("username");

        Integer res = ReportModel.insertOperador(rut_operador,area_ope,grupo_ope,nombre_operador,app_pat_operador,app_mat_operador,sap_operador,
            estado_vigencia_operador,tipo_de_operador,fecha_nacimiento,cargo_operador,superintendencia,tramo,fecha_ingreso,genero_ope,usuario);
        if(res != 1)
        {
            method403(request, response);
        }
      
    }

    @WebAction(actionName = "insertModOperador.json")
    protected  void insertModOperador(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String rut_operador = (String) request.getParameter("rut_operador");
        String area_grupo = (String) request.getParameter("area_grupo");
        String nombre_operador = UpperString((String) request.getParameter("nombre").trim());
        String app_pat_operador = UpperString((String) request.getParameter("app_paterno").trim());
        String app_mat_operador = UpperString((String) request.getParameter("app_materno").trim());
        Integer sap_operador = Integer.parseInt(request.getParameter("sap"));
        String estado_vigencia_operador = (String) request.getParameter("estado_vigencia");
        String tipo_de_operador = (String) request.getParameter("empleado_por");
        String fecha_nacimiento = (String) request.getParameter("fecha_nacimiento");
        String cargo_operador = (String) request.getParameter("cargo_operador");
        String superintendencia = (String) request.getParameter("superintendencia");
        Integer tramo = Integer.parseInt(request.getParameter("tramo"));
        String fecha_ingreso = (String) request.getParameter("fecha_ingreso");
        String[] area_and_grupo_ope = area_grupo.split(";");
        String area_ope=area_and_grupo_ope[0];
        String grupo_ope=area_and_grupo_ope[1];
        HttpSession session       = request.getSession();
        String usuario = (String) session.getAttribute("username");
        Integer res = ReportModel.insertModOperador(rut_operador,area_ope,grupo_ope,nombre_operador,app_pat_operador,app_mat_operador,sap_operador,
            estado_vigencia_operador,tipo_de_operador,fecha_nacimiento,cargo_operador,superintendencia,tramo,fecha_ingreso,usuario);
        if(res != 1)
        {
            method403(request, response);
        }
        
    }

    @WebAction(actionName = "insertModMonitor.json")
    protected  void insertModMonitor(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String rut_operador = (String) request.getParameter("rut_operador");
        String area_grupo = (String) request.getParameter("area_grupo");
        String[] area_and_grupo_ope = area_grupo.split(";");
        String area_ope=area_and_grupo_ope[0];
        String grupo_ope=area_and_grupo_ope[1];
        HttpSession session       = request.getSession();
        String usuario = (String) session.getAttribute("username");
        Integer res = ReportModel.insertModMonitor(rut_operador,area_ope,grupo_ope,usuario);
        if(res != 1)
        {
            method403(request, response);
        }
      
    }

    @WebAction(actionName = "insertModAutorizacion.json")
    protected  void insertModAutorizacion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String fecha = (String) request.getParameter("fecha");
        Integer id_maquinaria = Integer.parseInt(request.getParameter("maquinaria"));
        String rut_monitor = (String) request.getParameter("monitor");
        String vigencia = (String) request.getParameter("vigencia");
        String tipo_autorizacion = (String) request.getParameter("tipo_autorizacion");
        String operacion_invierno = (String) request.getParameter("ope_invierno");
        Integer programa = Integer.parseInt(request.getParameter("programa"));
        Integer id_autorizacion = Integer.parseInt(request.getParameter("id_autorizacion"));
        HttpSession session       = request.getSession();
        String usuario = (String) session.getAttribute("username");
        Integer res = ReportModel.insertModAutorizacion(fecha,id_maquinaria,rut_monitor,vigencia,
            tipo_autorizacion,operacion_invierno,programa,id_autorizacion,usuario);
        if(res != 1)
        {
            method403(request, response);
        }
    }

    @WebAction(actionName = "validacion_rut_autorizacion.json")
    protected void validacion_rut_autorizacion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String rut = (String) request.getParameter("rut");
        String res = ReportModel.validacion_rut_autorizacion(rut);
        //System.err.println("response"+res);
        renderAsJSON(request, response, res);
    }
    

    @WebAction(actionName = "validacion_autorizacion_equipos.json")
    protected void validacion_autorizacion_equipos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String rut = (String) request.getParameter("rut");
        Integer equipo = Integer.parseInt(request.getParameter("equipo"));
        String res = ReportModel.validacion_autorizacion_equipos(rut,equipo);
        renderAsJSON(request, response, res);
    }

    @WebAction(actionName = "insertModMaquina.json")
    protected  void insertModMaquina(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Integer id_maquinaria = Integer.parseInt(request.getParameter("id_equipo"));
        String nombre_maquinaria = (String) request.getParameter("nombre");
        String estado_vigencia = (String) request.getParameter("vigencia");
        HttpSession session       = request.getSession();
        String usuario = (String) session.getAttribute("username");
        Integer res = ReportModel.insertModMaquina(nombre_maquinaria,estado_vigencia,id_maquinaria,usuario);
         if(res != 1)
        {
            method403(request, response);
        }
      
    }

    @WebAction(actionName = "insertModPrograma.json")
    protected  void insertModPrograma(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Integer id_programa = Integer.parseInt(request.getParameter("id_programa"));
        String nombre_programa = (String) request.getParameter("nombre");
        String estado_programa = (String) request.getParameter("vigencia");
        HttpSession session       = request.getSession();
        String usuario = (String) session.getAttribute("username");
        Integer res = ReportModel.insertModPrograma(nombre_programa,estado_programa,id_programa,usuario);
         if(res != 1)
        {
            method403(request, response);
        }
        
    }

    @WebAction(actionName = "insertNewPrograma.json")
    protected  void insertNewPrograma(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        String nombre_programa = (String) request.getParameter("nombre");
        String estado_vigencia_programa = (String) request.getParameter("validez");
        HttpSession session       = request.getSession();
        String usuario = (String) session.getAttribute("username");

        Integer res = ReportModel.insertNewPrograma(nombre_programa,estado_vigencia_programa,usuario);
         if(res != 1)
        {
            method403(request, response);
        }
        
    }

    @WebAction(actionName = "insertNewActividad.json")
    protected  void insertNewActividad(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String nombre_actividad = (String) request.getParameter("nombre_actividad");
        Integer programa = Integer.parseInt(request.getParameter("programa"));
        String descripcion = (String) request.getParameter("descripcion");
        Integer hh_actividad = Integer.parseInt(request.getParameter("hh_actividad"));
        String id_equipo = (String) request.getParameter("id_equipos");
        String validez = (String) request.getParameter("validez");
        String id_equipo_new = "{"+id_equipo+"}";
        String grupos = (String) request.getParameter("grupos");
        String id_grupos = "{"+grupos+"}";
        String fecha_inicio = (String) request.getParameter("fecha_inicio");
        String fecha_termino = (String) request.getParameter("fecha_termino");
        Integer id_tipo_actividad = Integer.parseInt(request.getParameter("tipo_actividades"));
        Integer id_solicitante =Integer.parseInt(request.getParameter("solicitante"));
        HttpSession session       = request.getSession();
        String usuario = (String) session.getAttribute("username");
       Integer res= ReportModel.insertNewActividad(nombre_actividad,programa,descripcion,hh_actividad,validez,id_equipo_new,
        id_grupos,fecha_inicio,fecha_termino,id_tipo_actividad,id_solicitante,usuario);
         if(res != 0)
        {
            method403(request, response);
        }

    }

    @WebAction(actionName = "updateActividad.json")
    protected  void updateActividad(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String nombre_actividad = (String) request.getParameter("nombre_actividad");
        Integer programa = Integer.parseInt(request.getParameter("programa"));
        String descripcion = (String) request.getParameter("descripcion");
        Integer hh_actividad = Integer.parseInt(request.getParameter("hh_actividad"));
        String id_equipo = (String) request.getParameter("id_equipos");
        String validez = (String) request.getParameter("validez");
        String id_equipo_new = "{"+id_equipo+"}";
        String grupos = (String) request.getParameter("grupos");
        String id_grupos = "{"+grupos+"}";
        String fecha_inicio = (String) request.getParameter("fecha_inicio");
        String fecha_termino = (String) request.getParameter("fecha_termino");
        Integer id_tipo_actividad = Integer.parseInt(request.getParameter("tipo_actividades"));
        Integer id_solicitante =Integer.parseInt(request.getParameter("solicitante"));
        Integer id_actividad = Integer.parseInt(request.getParameter("id_actividad"));
        HttpSession session       = request.getSession();
        String usuario = (String) session.getAttribute("username");
       Integer res= ReportModel.updateActividad(nombre_actividad,programa,descripcion,hh_actividad,validez,id_equipo_new,
        id_grupos,fecha_inicio,fecha_termino,id_tipo_actividad,id_solicitante,id_actividad,usuario);
         if(res != 0)
        {
            method403(request, response);
        }

    }

    @WebAction(actionName = "insertNewEquipo.json")
    protected  void insertNewEquipo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        String nombre_maquinaria = (String) request.getParameter("nombre_maquinaria");
        Integer area = Integer.parseInt(request.getParameter("area"));
        String estado_vigencia = (String) request.getParameter("vigencia");
        HttpSession session       = request.getSession();
        String usuario = (String) session.getAttribute("username");
        System.err.println("insertNewEquipo: "+usuario);
        Integer res = ReportModel.insertNewEquipo(nombre_maquinaria,area,estado_vigencia,usuario);
         if(res != 1)
        {
            method403(request, response);
        }
       
    }

    @WebAction(actionName = "formPlanWeek.html")
    protected static void formPlanWeek(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        renderView(request, response, "main_layout", "/forms/_formPlanWeek");
    }


    public static String UpperString(String cadena) {
       
        String nuevacadena = "";
        String[] palabras = cadena.split(" ");
        for (int i = 0; i < palabras.length; i++) {
 
        nuevacadena += palabras[i].substring(0, 1).toUpperCase() + palabras[i].substring(1, palabras[i].length()).toLowerCase() + " ";
        }
        nuevacadena = nuevacadena.trim();
        return nuevacadena;
        
    
    }

    public static int[] strArrayToIntArray(String[] a){
    int[] b = new int[a.length];
    for (int i = 0; i < a.length; i++) {
        b[i] = Integer.parseInt(a[i]);
    }

    return b;
    }

}


