package com.tiaxa.controllers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.ServletConfig;

import com.tiaxa.common.*;
import com.tiaxa.util.*;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainController extends AbstractController {

	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private static Logger log = LoggerFactory.getLogger(MainController.class);

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	@Override
	public boolean requireRestrictedAccess() {
		return false;
	}

	/****************************************************************************/
	@WebAction(actionName = "login.shtml")
	protected static void login(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		log.debug("Redirecciona al login");
		renderView(request, response, "login", null);
	}

	@WebAction(actionName = "/")
  protected void hashlash(HttpServletRequest request, HttpServletResponse response)
					throws ServletException, IOException {
        		//ReportController.repGeneral(request, response);
    }

	@WebAction(actionName = "index.html")
	protected void index(HttpServletRequest request, HttpServletResponse response)
					throws ServletException, IOException {
            //ReportController.repGeneral(request, response);
						renderView(request, response, "main_layout", "/main/_index");
	}

	/****************************************************************************/
	@WebAction(actionName = "logout.shtml")
	protected static void logout(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		log.debug("Entro aca al main");
		HttpSession session = request.getSession();
		
		
		if (session != null) {
			session.invalidate();
		}
		request.setAttribute("logout_ok", 0);
		login(request, response);
		return;

	}

	@WebAction(actionName = "suspended.shtml")
	protected void suspendedAccount(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		renderView(request, response, "main_layout", "/main/_suspended");
	}

	@WebAction(actionName = "changepassword.html", isAutaditable = true)
	protected void changePassword(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		renderPartial(request, response, "/main/_changepassword");
	}

	@WebAction(actionName = "/403.shtml")
	protected void error403(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		method403(request, response);
	}

	/****************************************************************************/

}
