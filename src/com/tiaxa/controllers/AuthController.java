package com.tiaxa.controllers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import com.tiaxa.login.*;
import javax.servlet.ServletConfig;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.InvalidKeyException;
import javax.xml.bind.DatatypeConverter;

import java.net.URLEncoder;
import java.util.*;

import com.tiaxa.common.*;
import com.tiaxa.model.*;
import com.tiaxa.util.*;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class AuthController extends AbstractController
{
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private static Logger log = LoggerFactory.getLogger(AuthController.class);

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

  public boolean requireRestrictedAccess() { return true;}


	/****************************************************************************/
	@WebAction(actionName = "emailpassword.html")
	protected static void repGeneral(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
			String key = "CWQv0au/mc9mI/ULIuoNkC3BofxzF3ROJR8a4vQe"; //Esto sacarlo de un config de la App
			String token_uuid = UUID.randomUUID().toString();

			try {
				Mac hasher = Mac.getInstance("HmacSHA256");
				hasher.init(new SecretKeySpec(key.getBytes(), "HmacSHA256"));

				byte[] hash = hasher.doFinal(token_uuid.getBytes());

				request.setAttribute("token", DatatypeConverter.printHexBinary(hash));
			}
			catch (NoSuchAlgorithmException e) {}
			catch (InvalidKeyException e) {}

    	renderView(request, response, "auth_layout", "/auth/_emailpassword");
	}
	/****************************************************************************/
	@WebAction(actionName = "resetpassword.html")
	protected static void resetPassword(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
			String email_to = (String) request.getParameter("email");
			String token = (String) request.getParameter("token");

			Integer count_email = ServiceModel.validateEmail(email_to);

			request.setAttribute("token", token);

			if (count_email == 1) {
				//Cuenta de Correo está registrada en Omnia

				String link_token = "http://localhost:8080/webBoss/auth/reset_password.shtml?t=" + token;

				try {
					HttpResponse<JsonNode> requestSend = Unirest.post("https://api.mailgun.net/v3/sandbox3d7fd609857f41fba2fe51cb4376c1db.mailgun.org/messages")
							.basicAuth("api", "004b1b21a4f540d3dc36cc8585186227-a5d1a068-f08462be")
							.queryString("from", "Excited User <mailgun@sandbox3d7fd609857f41fba2fe51cb4376c1db.mailgun.org>")
							.queryString("to", email_to)
							.queryString("subject", "Cambio de Clave")
							.queryString("text", "Hola!\n\nTe enviamos el link adjunto porque recibimos una solicitud para cambiar la clave de tu cuenta.\n\n" +
																		link_token + "\n\nSi no solicitaste el cambio de clave, no se necesita acción alguna de tu parte.\n\n" +
																		"Saludos\nOmnia")
							.asJson();
				}
				catch (UnirestException e) {}

				request.setAttribute("status", "success");
				request.setAttribute("message", "Te hemos enviado un correo para que cambies tu clave.");
				renderView(request, response, "auth_layout", "/auth/_emailpassword");

			}

			request.setAttribute("status", "danger");
			request.setAttribute("message", "No hemos podido encontrar la dirección de correo que indicas.");
    	renderView(request, response, "auth_layout", "/auth/_emailpassword");
	}
	/****************************************************************************/
	@WebAction(actionName = "reset_password.shtml")
	protected static void resPassword(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
    	renderView(request, response, "auth_layout", "/auth/_resetpassword");
	}

}
