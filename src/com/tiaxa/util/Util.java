package com.tiaxa.common;


import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.*;
import java.io.FileInputStream;
import java.io.InputStream;

/*
 ** @author dbl
*/
public class Util
{

    public static Properties getPlatformConfig(File confFile) 
    {

        Properties properties = new Properties();
        InputStream input = null;

        try {

                input = new FileInputStream(confFile);
                properties.load(input);

        } catch (IOException ex) {
                ex.printStackTrace();
                System.exit(-1);
        } finally {
                if (input != null) {
                        try {
                                input.close();
                        } catch (IOException e) {
                                e.printStackTrace();
                        }
                }
        }


        return properties;
    }
}
