package com.tiaxa.model;


import java.util.*;
import com.tiaxa.model.DBAccess;
import com.tiaxa.model.JDBCUtilities;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class ServiceModel
{
    private static final Logger log = LoggerFactory.getLogger("com.tiaxa.sql") ;

public static HashMap<String,String> authenticateUser(String email, String password)
    {
            StringBuilder sql = new StringBuilder(" SELECT  u.id_user as \"userId\", u.name ");
            sql.append(" FROM config.user u ");
            sql.append(" WHERE  u.email = ? ");
            sql.append(" AND  u.password = ? LIMIT 1");
            //sql.append(" AND  sha1(CONCAT(?, ':',u.salt)::bytea) = u.password LIMIT 1");

            log.debug("SQL:" + sql);

            //System.out.println("mvc SQL:" + sql);

            ArrayList<HashMap<String,String>> rows = JDBCUtilities.getResultSet(DBAccess.getOMNIADS(),sql.toString(),email,password);
            for ( HashMap<String,String> candidate : rows )
            {
                return candidate;
            }
            return null;
    }

  public static ArrayList<HashMap<String,String>> getPassword(String passw ,String salt, Integer userId)
  {
          StringBuilder sql = new StringBuilder(" SELECT password ");
          sql.append(" FROM gestionpassword ");
          sql.append(" WHERE userid=? AND  password=sha1(CONCAT(?,':',?)::bytea) ");
          return JDBCUtilities.getResultSet(DBAccess.getOMNIADS(),sql.toString(),userId,passw,salt);
  }
    
    public static String getBots(String username) {
        StringBuilder sql = new StringBuilder(" SELECT array_to_json(array_agg(row_to_json(f.*))) as result ");
        sql.append(" FROM (SELECT b.id_bot, b.name FROM config.bot b JOIN config.client_user c ON b.id_client = c.id ");
        sql.append(" WHERE c.email =  ?) f");
        ArrayList<HashMap<String,String>> rows = JDBCUtilities.getResultSet(DBAccess.getOMNIADS(), sql.toString(), username);
        String result = null;
        result = rows.get(0).get("result");
        log.debug("RESULT: " + result);
        return result;
    }
    
    

    public static Integer getClient(String username) {
        StringBuilder sql = new StringBuilder(" SELECT id_client");
        sql.append(" FROM config.client_user ");
        sql.append(" WHERE email = ? LIMIT 1");
        ArrayList<HashMap<String,String>> rows = JDBCUtilities.getResultSet(DBAccess.getOMNIADS(), sql.toString(), username);
        Integer result = null;
        if (rows != null && rows.size() > 0)
            result = Integer.parseInt(rows.get(0).get("id_client"));
        return result;
    }

    public static int modifyPassword(HashMap<String,String> modPass, int cantPassw, String date)
    {
            ArrayList<Object> userArguments = new ArrayList<>();
            int nextVal=0;
            int aux=0;
            String expiredPassw=modPass.get("expiredPassw");
            userArguments.add(modPass.get("passw"));
            userArguments.add(modPass.get("salt"));
            userArguments.add(modPass.get("p_userId"));
            StringBuffer sql = null;

            if (!"".equals(date.trim()))
            {
                sql = new StringBuffer(String.format("UPDATE  user_tas set password=sha1(CONCAT(?,':',?)::bytea), last_access=now(),password_valid_until='%s'",date))
                .append(" WHERE userid=?::integer " );
            }
            else
            {
                sql =new StringBuffer ("UPDATE  user_tas set password=sha1(CONCAT(?,':',?)::bytea), last_access=now()  ")
                .append(" WHERE userid=?::integer " );
            }

            int idUser = JDBCUtilities.executeUpdate( DBAccess.getOMNIADS(), sql.toString(), true, (Object[]) userArguments.toArray() );
            sql=new StringBuffer(" SELECT max(countreg)+1 as \"nextVal\" from gestionpassword where userid=?::integer ");
            userArguments = new ArrayList<>();
            userArguments.add(modPass.get("p_userId"));
            ArrayList<HashMap<String,String>> passwList = JDBCUtilities.getResultSet(DBAccess.getOMNIADS(),sql.toString(),(Object[]) userArguments.toArray());
            for ( HashMap<String,String> row : passwList )
            {
                nextVal=Integer.parseInt(row.get("nextVal"));
            }
            if (idUser>0)
            {
                userArguments = new ArrayList<>();

                StringBuffer sqlInsert = new StringBuffer();
                sqlInsert.append("INSERT INTO gestionpassword (passwid,userid,password,fechmod,countreg) ");
                sqlInsert.append(" SELECT ").append(nextVal).append(" % ").append(cantPassw).append(",?::integer,sha1(CONCAT(?,':',?)::bytea),now()::date,").append(nextVal);

                StringBuffer sqlUpsert = new StringBuffer("UPDATE gestionpassword SET password = sha1(CONCAT(?,':',?)::bytea), fechmod = now()::date, countreg = " + nextVal);
                StringBuffer append = sqlUpsert.append(" WHERE passwid = ").append(nextVal).append(" % ").append(cantPassw).append(" AND userid = ").append(modPass.get("p_userId"));

                sql = new StringBuffer("WITH upsert AS (");
                sql.append(sqlUpsert);
                sql.append(" RETURNING *) ");
                sql.append(sqlInsert);
                sql.append(" WHERE NOT EXISTS (SELECT * FROM upsert) ");

                userArguments.add(modPass.get("passw"));
                userArguments.add(modPass.get("salt"));
                userArguments.add(modPass.get("p_userId"));
                userArguments.add(modPass.get("passw"));
                userArguments.add(modPass.get("salt"));

                aux = JDBCUtilities.executeUpdate( DBAccess.getOMNIADS(), sql.toString(), true,  (Object[]) userArguments.toArray() );
            }

            return aux;
    }

    public static Integer validateEmail(String email) {
        StringBuilder sql = new StringBuilder(" SELECT count(*) AS count_email");
        sql.append(" FROM config.user ");
        sql.append(" WHERE email = ? LIMIT 1");
        ArrayList<HashMap<String,String>> rows = JDBCUtilities.getResultSet(DBAccess.getOMNIADS(), sql.toString(), email);
        Integer result = null;
        if (rows != null && rows.size() > 0)
            result = Integer.parseInt(rows.get(0).get("count_email"));
        return result;
    }


}
