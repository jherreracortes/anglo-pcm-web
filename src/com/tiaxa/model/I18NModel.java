package com.tiaxa.model;


import java.sql.*;
import java.util.*;
import com.tiaxa.model.DBAccess;
import com.tiaxa.model.JDBCUtilities;
import com.tiaxa.model.RowMapper;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class I18NModel
{

    private static final Logger log = LoggerFactory.getLogger(I18NModel.class);

    /**
     * @return ********************************************************************************************************/
    /* Get I18N translation bundle from DB as properties format*/

    public static HashMap<String,Properties> getI18NTranslations()
    {
        HashMap<String,Properties> i18nTranslations = new HashMap<>();
        String sqlTrans = "select i18n_label, i18n_value, i18n_lang from web_i18n ORDER by 3";
        Properties curr_prop ;

        RowMapper<HashMap<String,String>> mapper = new RowMapper<HashMap<String, String>>()
        {
            @Override
            public HashMap<String, String> mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException
            {
                HashMap<String, String> hmI18 = new HashMap<>();
                hmI18.put("i18n_label", rs.getString(1));
                hmI18.put("i18n_value", new String(rs.getBytes(2),"UTF-8"));
                hmI18.put("i18n_lang", rs.getString(3));
                return hmI18;
            }
        };

        ArrayList<HashMap<String,String>> i18Nrows = JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper, sqlTrans);

        for ( HashMap<String,String> i18Nrow : i18Nrows )
        {
            String i18n_lang  = i18Nrow.get("i18n_lang");
            curr_prop = i18nTranslations.get(i18n_lang);
            if ( curr_prop == null )
            {
                log.info("adding language Property bundle " + i18n_lang);
                curr_prop = new Properties();
                i18nTranslations.put( i18n_lang , curr_prop );
            }
            curr_prop.setProperty(i18Nrow.get("i18n_label"),i18Nrow.get("i18n_value"));
        }

        //log.debug("[getI18NTranslations] i18nTranslations : " + i18nTranslations.toString() );

        return i18nTranslations;
    }
    /**********************************************************************************************************/


}
