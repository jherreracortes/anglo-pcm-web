package com.tiaxa.model;


import java.util.*;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GrantedAccess 
{

    private boolean authenticated = false;
    private HashMap<String, String> userData = null;
    private HashSet<String> allowedModules = null;

    public boolean isAuthenticated() {
        return authenticated;
    }

    @XmlElement
    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public HashMap<String, String> getUserData() {
        return userData;
    }
    @XmlElement
    public void setUserData(HashMap<String, String> userData) {
        this.userData = userData;
    }

    public HashSet<String> getAllowedModules() {
        return allowedModules;
    }
    @XmlElement
    public void setAllowedModules(HashSet<String> allowedModules) {
        this.allowedModules = allowedModules;
    }

}

