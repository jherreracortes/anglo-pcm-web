package com.tiaxa.model;
import java.sql.ResultSet;

public interface RowMapper<T>
{
	public T mapRow(ResultSet rs, int row) throws Exception;
}
