package com.tiaxa.model;

import java.util.*;
import java.sql.*;
import com.tiaxa.model.DBAccess;
import com.tiaxa.model.JDBCUtilities;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
 
import java.io.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReportModel
{
    private static final Logger log = LoggerFactory.getLogger("com.tiaxa.sql") ;

    public static Integer insert_autorizacion(String fecha,String rut_operador,Integer id_maquinaria,String rut_monitor,String vigencia, String tipo_autorizacion, String operacion_invierno,
     Integer programa , String ussername, String mde) 
    {
           
            if(rut_monitor.length() == 0 || rut_monitor.matches("ninguno"))
            {    
               rut_monitor="NULL";
            }
            else
            {
                rut_monitor=rut_monitor.trim();
            }

            if(vigencia.matches("null"))
            {
                String sql = "INSERT INTO work.autorizaciones (rut, id_maquinaria, fecha_inicio,autorizador_por,tipo_autorizacion,operacion_invierno, id_programa,mde)"
            +" VALUES(?,?,to_date(?,'YYYY-MM-DD'),?,?,?::e_operacion_invierno,?,?);";
                Integer res=insertRegistroActividadUsuarios(ussername,"Sistema Gestión Competencias - Agregar Autorizacion",
                "rut operador: "+rut_operador+" id equipo: "+id_maquinaria+" fecha inicio: "+fecha+" fecha termino: NULL tipo autorizacion: "+tipo_autorizacion+" operacion invierno: "+operacion_invierno+" MDE: "+mde+" programa: "+programa+" autorizado por: "+rut_monitor
                ,"INSERT");

                return (JDBCUtilities.executeUpdate(DBAccess.getOMNIADS(),sql ,rut_operador,id_maquinaria,fecha,rut_monitor,tipo_autorizacion,operacion_invierno,programa,mde));
            }
            else
            {
                String sql = "INSERT INTO work.autorizaciones (rut, id_maquinaria, fecha_inicio,autorizador_por,fecha_termino,tipo_autorizacion,operacion_invierno, id_programa,mde)"
            +" VALUES(?,?,to_date(?,'YYYY-MM-DD'),?,date(to_date(?,'YYYY-MM-DD')+ cast (?  as interval)),?,?::e_operacion_invierno,?,?);";
                String fecha_vigencia=vigencia + " year";
                Integer res=insertRegistroActividadUsuarios(ussername,"Sistema Gestión Competencias - Agregar Autorizacion",
                "rut operador: "+rut_operador+" id equipo: "+id_maquinaria+" fecha inicio: "+fecha+" fecha termino: "+fecha_vigencia+" tipo autorizacion: "+tipo_autorizacion+" operacion invierno: "+operacion_invierno+" MDE: "+mde+" programa: "+programa+" autorizado por: "+rut_monitor
                ,"INSERT");
                return (JDBCUtilities.executeUpdate(DBAccess.getOMNIADS(),sql ,rut_operador,id_maquinaria,fecha,rut_monitor,fecha,fecha_vigencia,tipo_autorizacion,operacion_invierno,programa,mde));
            }           
            
    }

    public static Integer insertRegistroActividadUsuarios(String ussername,String modulo,String descripcion,String tipo_actividad) 
    {   

        String sql="SELECT work.insert_registro_actividad_usuario(?,?,?,?)";        
        //log.debug(sql);
        return (JDBCUtilities.queryForInt(DBAccess.getOMNIADS(),sql ,ussername.trim(),modulo,descripcion,tipo_actividad));
    }

    public static Integer insertOperador(String rut_operador,String area,String grupo,String nombre_operador,String app_pat_operador,String app_mat_operador,Integer sap_operador,
        String estado_vigencia_operador,String tipo_de_operador,
        String fecha_nacimiento,String cargo_operador,String superintendencia,Integer tramo,String fecha_ingreso,String genero_ope,String ussername) 
    {
            String sql =" INSERT INTO work.operadores(rut,id_area, id_grupo,nombre,apellido_paterno,apellido_materno,sap,estado_vigencia,tipo_empleado,fecha_nacimiento,cargo,superintendencia,id_tramo, fecha_ingreso,genero) "
                       +" VALUES(?,?,?,?,?,?,?,?,?,to_date(?,'DY MON DD YYYY'),?,?,?,to_date(?,'DY MON DD YYYY'),?);";

            Integer res=insertRegistroActividadUsuarios(ussername,"Sistema Gestión Competencias - Nuevo Operador",
                "rut operador: "+rut_operador+" area: "+area+" grupo: "+grupo+" nombre operador: "+nombre_operador+" app paterno: "+app_pat_operador+" app materno: "+app_mat_operador+" sap: "+sap_operador+" genero: "+genero_ope+" vigencia: "+estado_vigencia_operador+" empleado por: "+tipo_de_operador+" fecha nacimiento: "+fecha_nacimiento+" cargo: "+cargo_operador+" superintendencia: "+superintendencia+" tramo: "+tramo+" fecha ingreso: "+fecha_ingreso
                ,"INSERT");       

        return (JDBCUtilities.executeUpdate(DBAccess.getOMNIADS(),sql ,rut_operador,Integer.parseInt(area),Integer.parseInt(grupo),nombre_operador,app_pat_operador,app_mat_operador,sap_operador,estado_vigencia_operador,tipo_de_operador,fecha_nacimiento,Integer.parseInt(cargo_operador),Integer.parseInt(superintendencia),tramo,fecha_ingreso,genero_ope)); 
    }

    public static Integer insertModOperador(String rut_operador,String area,String grupo,String nombre_operador,String app_pat_operador,
        String app_mat_operador,Integer sap_operador,String estado_vigencia_operador,String tipo_de_operador,String fecha_nacimiento,
        String cargo_operador,String superintendencia,Integer tramo,String fecha_ingreso,String ussername) 
    {   

        String sql="UPDATE work.operadores SET id_area=?,id_grupo=?,nombre=?,apellido_paterno=?,apellido_materno=?, "
            +" sap=?,estado_vigencia=?,tipo_empleado=?,fecha_nacimiento=to_date(?,'YYYY-MM-DD'),cargo=?, "
            +" superintendencia=?,id_tramo=?,fecha_ingreso=to_date(?,'YYYY-MM-DD') "
            +" WHERE rut=?;";  
        Integer res=insertRegistroActividadUsuarios(ussername,"Sistema Gestión Competencias - Edicion Operadores",
                "rut operador: "+rut_operador+" area: "+area+" grupo: "+grupo+" nombre operador: "+nombre_operador+" app paterno: "+app_pat_operador+" app materno: "+app_mat_operador+" sap: "+sap_operador+" vigencia: "+estado_vigencia_operador+" empleado por: "+tipo_de_operador+" fecha nacimiento: "+fecha_nacimiento+" cargo: "+cargo_operador+" superintendencia: "+superintendencia+" tramo: "+tramo+" fecha ingreso: "+fecha_ingreso
                ,"UPDATE");          
        //log.debug(sql);
        return (JDBCUtilities.executeUpdate(DBAccess.getOMNIADS(),sql ,Integer.parseInt(area),Integer.parseInt(grupo),nombre_operador,app_pat_operador,app_mat_operador,sap_operador,estado_vigencia_operador,tipo_de_operador,fecha_nacimiento,Integer.parseInt(cargo_operador),Integer.parseInt(superintendencia),tramo,fecha_ingreso,rut_operador)); 
    }

    public static Integer insertModMonitor(String rut_operador,String area,String grupo,String ussername) 
    {   

        String sql="UPDATE work.operadores SET id_area=?,id_grupo=?"
                +" WHERE rut=?;";
        Integer res=insertRegistroActividadUsuarios(ussername,"Sistema Gestión Competencias - Edicion Monitor",
                "rut operador: "+rut_operador+" area: "+area+" grupo: "+grupo
                ,"UPDATE");                
        //log.debug(sql);
        return (JDBCUtilities.executeUpdate(DBAccess.getOMNIADS(),sql ,Integer.parseInt(area),Integer.parseInt(grupo),rut_operador)); 
    }

    public static Integer insertModAutorizacion(String fecha,Integer id_maquinaria,String rut_monitor,String vigencia,
     String tipo_autorizacion, String operacion_invierno, Integer programa , Integer id_autorizacion ,String ussername ) 
    {   


        if(rut_monitor.length() == 0 || rut_monitor.matches("ninguno"))
            {    
               rut_monitor="NULL";
            }
            else
            {
                rut_monitor=rut_monitor.trim();
            }

            if(vigencia.matches("null"))
            {
                
                String sql="UPDATE work.autorizaciones SET id_maquinaria=?,fecha_inicio=to_date(?,'YYYY-MM-DD'),"
                +" tipo_autorizacion=?,autorizador_por=? ,operacion_invierno=?::e_operacion_invierno,id_programa=? WHERE id_autorizacion=?;";
                 Integer res=insertRegistroActividadUsuarios(ussername,"Sistema Gestión Competencias - Modificar Autorizacion",
                "equipo: "+id_maquinaria+" tipo de autorizacion: "+tipo_autorizacion+" operacion invierno: "+operacion_invierno+" fecha autorizacion: "+fecha+" programa: "+programa+" id autorizacion: "+id_autorizacion
                ,"UPDATE");   
                return (JDBCUtilities.executeUpdate(DBAccess.getOMNIADS(),sql ,id_maquinaria,fecha,tipo_autorizacion,rut_monitor,operacion_invierno,programa,id_autorizacion)); 
            }
            else
            {
                String sql="UPDATE work.autorizaciones SET id_maquinaria=?,fecha_inicio=to_date(?,'YYYY-MM-DD'),fecha_termino=date(to_date(?,'YYYY-MM-DD')+ cast (?  as interval)) ,"
                +" tipo_autorizacion=?,autorizador_por=? ,operacion_invierno=?::e_operacion_invierno,id_programa=? WHERE id_autorizacion=?;";
                String fecha_vigencia=vigencia + " year";
                 Integer res=insertRegistroActividadUsuarios(ussername,"Sistema Gestión Competencias - Modificar Autorizacion",
                "equipo: "+id_maquinaria+" tipo de autorizacion: "+tipo_autorizacion+" operacion invierno: "+operacion_invierno+" fecha autorizacion: "+fecha+" programa: "+programa+" fecha autorizacion: "+fecha_vigencia+" id autorizacion: "+id_autorizacion
                ,"UPDATE");   
                return (JDBCUtilities.executeUpdate(DBAccess.getOMNIADS(),sql ,id_maquinaria,fecha,fecha,fecha_vigencia,tipo_autorizacion,rut_monitor,operacion_invierno,programa,id_autorizacion)); 

            }                
}

    public static String validacion_rut_autorizacion(String rut) {
    
       String sql = "select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                    +" from "
                    +" (SELECT nombre, rut "
                    +" FROM work.operadores where rut=?) t;";
                  
        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql ,rut)).get(0); 
    }

    public static String validacion_autorizacion_equipos(String rut,Integer equipo) {
       
       String sql = "select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                    +" from  "
                    +" (SELECT to_char(date(fecha_termino + cast ('1 day'  as interval)) , 'YYYY,MM,DD') as fecha_termino "
                    +" FROM work.autorizaciones where rut=? and id_maquinaria=? AND tipo_autorizacion ='A' and fecha_termino is not null order by fecha_termino desc limit 1) t; ";   
        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql ,rut,equipo)).get(0); 
    }

    public static Integer insertModMaquina(String nombre_maquinaria,String estado_vigencia,Integer id_maquinaria,String ussername) 
    {
        
        String sql = "UPDATE work.maquinarias set nombre_maquinaria=? , estado_vigencia =? where id_maquinaria=?;";
        Integer res=insertRegistroActividadUsuarios(ussername,"Sistema Gestión Competencias - Edicion De Equipo",
                "nombre maquina: "+nombre_maquinaria+" estado vigencia: "+estado_vigencia+" id maquinaria: "+id_maquinaria,"UPDATE");

        return (JDBCUtilities.executeUpdate(DBAccess.getOMNIADS(),sql ,nombre_maquinaria.trim(),estado_vigencia,id_maquinaria));
    }

    public static Integer insertModPrograma(String nombre_programa,String estado_programa,Integer id_programa,String ussername) 
    {
        
        String sql = "UPDATE work.programas set nombre=? , estado_programa =? where id_programa=?;";
        Integer res=insertRegistroActividadUsuarios(ussername,"Sistema Gestión Competencias - Edicion Programa",
                "nombre programa: "+nombre_programa+" estado vigencia: "+estado_programa+" id programa: "+id_programa,"UPDATE");
        
        return (JDBCUtilities.executeUpdate(DBAccess.getOMNIADS(),sql ,nombre_programa.trim(),estado_programa,id_programa));
    }

    public static Integer insertNewPrograma(String nombre_programa,String estado_vigencia_programa,String ussername) 
    {
        
            String sql = "INSERT INTO work.programas (nombre,estado_programa) VALUES(?, ?);";

            Integer res=insertRegistroActividadUsuarios(ussername,"Sistema Gestión Competencias - Nuevo Programa",
                "nombre programa: "+nombre_programa+" estado vigencia: "+estado_vigencia_programa,"INSERT");
         
            return (JDBCUtilities.executeUpdate(DBAccess.getOMNIADS(),sql ,nombre_programa.trim(),estado_vigencia_programa));

    }
  
    public static Integer insertNewActividad(String nombre_actividad,Integer programa,String descripcion,Integer hh_actividad,String validez ,
        String id_equipo_new ,String id_grupos,String fecha_inicio , String fecha_termino, Integer id_tipo_actividad,Integer id_solicitante,
        String ussername) 
    {
            String sql = "SELECT work.insert_actividad_maquinas(?::character varying,?::character varying,?,?,?::character varying,?::text[],?::text[],?::character varying,?::character varying,?,?);";

            Integer res=insertRegistroActividadUsuarios(ussername,"Sistema Gestión Competencias - Nueva Actividad",
                "nombre actividad: "+nombre_actividad+" programa: "+programa+" tipo de actividad: "+id_tipo_actividad+" solicitante: "+id_solicitante+" horas asignadas: "+hh_actividad+" equipos: "+id_equipo_new+" area grupo: "+id_grupos+" fecha inicio: "+fecha_inicio+" fecha termino: "+fecha_termino+" vigencia: "+validez
                ,"INSERT");

            return (JDBCUtilities.queryForInt(DBAccess.getOMNIADS(),sql ,nombre_actividad.trim(),descripcion,programa,hh_actividad,validez,id_equipo_new,id_grupos,fecha_inicio,fecha_termino,id_tipo_actividad,id_solicitante));

    }

    public static Integer updateActividad(String nombre_actividad,Integer programa,String descripcion,Integer hh_actividad,String validez ,
        String id_equipo_new ,String id_grupos,String fecha_inicio , String fecha_termino, Integer id_tipo_actividad,Integer id_solicitante,
        Integer id_actividad,String ussername) 
    {
            String sql = "SELECT work.update_actividad_maquinas(?::character varying,?::character varying,?,?,?::character varying,?::text[],?::text[],?::character varying,?::character varying,?,?,?);";

            Integer res=insertRegistroActividadUsuarios(ussername,"Sistema Gestión Competencias - Nueva Actividad",
                "id_actividad: "+id_actividad+" nombre actividad: "+nombre_actividad+" programa: "+programa+" tipo de actividad: "+id_tipo_actividad+" solicitante: "+id_solicitante+" horas asignadas: "+hh_actividad+" equipos: "+id_equipo_new+" area grupo: "+id_grupos+" fecha inicio: "+fecha_inicio+" fecha termino: "+fecha_termino+" vigencia: "+validez
                ,"INSERT");

            return (JDBCUtilities.queryForInt(DBAccess.getOMNIADS(),sql ,nombre_actividad.trim(),descripcion,programa,hh_actividad,validez,id_equipo_new,id_grupos,fecha_inicio,fecha_termino,id_tipo_actividad,id_solicitante,id_actividad));

    }
 
    public static Integer insertNewEquipo(String nombre_maquinaria,Integer area,String estado_vigencia,String ussername) 
    {
        
            String sql = "INSERT INTO work.maquinarias (nombre_maquinaria, id_area, estado_vigencia) VALUES(?, ?, ?);";
            
            Integer res=insertRegistroActividadUsuarios(ussername,"Sistema Gestión Competencias - Equipo Nuevo"," nombre maquina: "+nombre_maquinaria+" area: "+area+" vigencia: "+estado_vigencia,"INSERT") ;

            return (JDBCUtilities.executeUpdate(DBAccess.getOMNIADS(),sql ,nombre_maquinaria.trim(),area,estado_vigencia));

    }

    public static String BuscadorOperadores(String fecha) {
        
        fecha=fecha+"%";
      String sql ="select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                +" from " 
                +" (select ope.rut as rut,ar.area as area ,gr.grupo_turno as grupo, "
                +" CONCAT (trim(nombre),' ',trim(apellido_paterno),' ',trim(apellido_materno)) as nombre "
                +" from work.operadores ope "
                +" join work.areas ar on ope.id_area=ar.id_area "
                +" join work.grupos gr on ope.id_grupo=gr.id_grupo "
                +" where rut ilike ? and ope.estado_vigencia='VIGENTE' order by nombre asc limit 15 " 
                +" ) t;";           
        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1).getBytes(),"UTF-8");
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql,fecha )).get(0); 
    }



    public static String BuscadorMonitoresEdit(String rut) {
        
       rut=rut+"%";
      String sql ="select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                +" from  "
                +" (select ope.rut as rut,CONCAT (ar.id_area,';',gr.id_grupo) as area_grupo, "
                +" nombre,apellido_paterno,apellido_materno,sap,estado_vigencia, "
                +" tipo_empleado,to_char(fecha_nacimiento, 'YYYY,MM,DD') as fecha_nacimiento ,cargo,superintendencia, "
                +" tramo,to_char(fecha_ingreso, 'YYYY,MM,DD') as fecha_ingreso "
                +" from work.operadores ope "
                +" join work.areas ar on ope.id_area=ar.id_area "
                +" join work.grupos gr on ope.id_grupo=gr.id_grupo "
                +" where rut ilike ? and ope.id_area=6 and ope.estado_vigencia='VIGENTE' order by nombre asc limit 15 "
                +" ) t;";                     
        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1).getBytes(),"UTF-8");
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql,rut )).get(0); 
    }

    public static String MonitorEdit(String rut) {
        

      String sql ="select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                +" from  "
                +" (select ope.rut as rut,CONCAT (ar.id_area,';',gr.id_grupo) as area_grupo, "
                +" nombre,apellido_paterno,apellido_materno,sap,estado_vigencia, "
                +" tipo_empleado,to_char(fecha_nacimiento, 'YYYY,MM,DD') as fecha_nacimiento ,cargo,superintendencia, "
                +" tramo,to_char(fecha_ingreso, 'YYYY,MM,DD') as fecha_ingreso "
                +" from work.operadores ope "
                +" join work.areas ar on ope.id_area=ar.id_area "
                +" join work.grupos gr on ope.id_grupo=gr.id_grupo "
                +" where rut= ? and ope.id_area=6 and ope.estado_vigencia='VIGENTE' order by nombre asc"
                +" ) t;";                     
        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1).getBytes(),"UTF-8");
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql,rut )).get(0); 
    }

    public static String AutorizacionesEdit(Integer id_autorizacion) {
        

      String sql ="select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                +" from " 
                +" (select ope.rut,CONCAT (trim(ope.nombre),' ',trim(ope.apellido_paterno),' ',trim(ope.apellido_materno)) as nombre_operador, "
                +" ar.area,gr.grupo_turno,auto.tipo_autorizacion,auto.id_maquinaria,auto.operacion_invierno,auto.id_programa,auto.autorizador_por, "
                +" EXTRACT(years FROM age(date(auto.fecha_termino),date(auto.fecha_inicio) ) ) as fecha_termino, "
                +" to_char(auto.fecha_inicio, 'YYYY,MM,DD') as fecha_inicio "
                +" FROM work.autorizaciones auto "
                +" join work.operadores ope ON auto.rut=ope.rut "
                +" join work.areas ar on ope.id_area=ar.id_area  "
                +" join work.grupos gr on ope.id_grupo=gr.id_grupo  "
                +" where auto.id_autorizacion=? "
                +" ) t;";                     
        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1).getBytes(),"UTF-8");
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql,id_autorizacion )).get(0); 
    }

    public static String ReporteMonitoresEdit() {
        
      String sql ="select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                +" from  "
                +" (select ope.rut as rut,ar.area as area_monitor,gr.grupo_turno as grupo_monitor, "
                +" CONCAT (trim(nombre),' ',trim(apellido_paterno),' ',trim(apellido_materno)) as nombre_monitor "
                +" from work.operadores ope "
                +" join work.areas ar on ope.id_area=ar.id_area "
                +" join work.grupos gr on ope.id_grupo=gr.id_grupo "
                +" where  ope.id_area=6 and ope.estado_vigencia='VIGENTE' order by nombre asc  ) t;";                     
        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1).getBytes(),"UTF-8");
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql )).get(0); 
    }

    public static String BuscadorOperadoresEdit(String rut) {
        
        rut=rut+"%";
      String sql ="select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                +" from  "
                +" (select ope.rut as rut,CONCAT (ar.id_area,';',gr.id_grupo) as area_grupo, "
                +" trim(nombre) as nombre,trim(apellido_paterno) as apellido_paterno,trim(apellido_materno) as apellido_materno,sap,estado_vigencia, "
                +" tipo_empleado,to_char(fecha_nacimiento, 'YYYY,MM,DD') as fecha_nacimiento ,cargo,superintendencia, "
                +" id_tramo,to_char(fecha_ingreso, 'YYYY,MM,DD') as fecha_ingreso "
                +" from work.operadores ope "
                +" join work.areas ar on ope.id_area=ar.id_area "
                +" join work.grupos gr on ope.id_grupo=gr.id_grupo "
                +" where rut ilike ? order by nombre asc limit 15 "
                +" ) t;";           
        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1).getBytes(),"UTF-8");
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql,rut )).get(0); 
    }

    public static String ReporteAutorizacion(Integer area,Integer equipo,String fecha ,String fecha_termino ) {

        String sql="";

        if(area == 0 && equipo !=0)
         {
            sql="select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                    +" from "
                    +" (select auto.id_autorizacion,area.area,ope.rut as rut_operador ,maq.nombre_maquinaria, "
                    +" CONCAT (trim(nombre),' ',trim(apellido_paterno),' ',trim(apellido_materno)) as nombre_operador, "
                    +" ope.cargo,auto.fecha_inicio as fecha_autorizacion, "
                    +" auto.fecha_termino as fecha_termino_autorizacion,auto.tipo_autorizacion,auto.autorizador_por as rut_monitor, "
                    +" ( "
                    +"    select CONCAT (trim(ope.nombre),' ',trim(ope.apellido_paterno),' ',trim(ope.apellido_materno)) as nombre_monitor "
                    +"    from work.operadores ope "
                    +"    join work.autorizaciones autos  on ope.rut=autos.autorizador_por "
                    +"    where autos.autorizador_por=auto.autorizador_por limit 1 "
                    +") as nombre_monitor "
                    +" from work.maquinarias maq"
                    +" join work.autorizaciones auto on auto.id_maquinaria=maq.id_maquinaria"
                    +" join work.operadores ope on ope.rut=auto.rut"
                    +" join work.areas area on ope.id_area=area.id_area  "
                    +" where maq.id_maquinaria=? and auto.fecha_inicio >=to_date(?,'DY MON DD YYYY') "
                    +" and auto.fecha_inicio <=to_date(?,'DY MON DD YYYY') order by auto.fecha_inicio desc "
                    +" ) t;";

         }
         else
         {
            if(equipo == 0 && area !=0)
            {
                sql="select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                    +" from "
                    +" (select auto.id_autorizacion,area.area,ope.rut as rut_operador ,maq.nombre_maquinaria, "
                    +" CONCAT (trim(nombre),' ',trim(apellido_paterno),' ',trim(apellido_materno)) as nombre_operador, "
                    +" ope.cargo,auto.fecha_inicio as fecha_autorizacion, "
                    +" auto.fecha_termino as fecha_termino_autorizacion,auto.tipo_autorizacion,auto.autorizador_por as rut_monitor, "
                    +" ( "
                    +"    select CONCAT (trim(ope.nombre),' ',trim(ope.apellido_paterno),' ',trim(ope.apellido_materno)) as nombre_monitor "
                    +"    from work.operadores ope "
                    +"    join work.autorizaciones autos  on ope.rut=autos.autorizador_por "
                    +"    where autos.autorizador_por=auto.autorizador_por limit 1 "
                    +") as nombre_monitor "
                    +" from work.maquinarias maq"
                    +" join work.autorizaciones auto on auto.id_maquinaria=maq.id_maquinaria"
                    +" join work.operadores ope on ope.rut=auto.rut"
                    +" join work.areas area on ope.id_area=area.id_area  "
                    +" where area.id_area=?  and auto.fecha_inicio >=to_date(?,'DY MON DD YYYY') "
                    +" and auto.fecha_inicio <=to_date(?,'DY MON DD YYYY')order by auto.fecha_inicio desc "
                    +" ) t;";
            }
            else
            {   
                if(equipo != 0 && area !=0)
                {
                sql="select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                    +" from "
                    +" (select auto.id_autorizacion,area.area,ope.rut as rut_operador ,maq.nombre_maquinaria, "
                    +" CONCAT (trim(nombre),' ',trim(apellido_paterno),' ',trim(apellido_materno)) as nombre_operador, "
                    +" ope.cargo,auto.fecha_inicio as fecha_autorizacion, "
                    +" auto.fecha_termino as fecha_termino_autorizacion,auto.tipo_autorizacion,auto.autorizador_por as rut_monitor, "
                    +" ( "
                    +"    select CONCAT (trim(ope.nombre),' ',trim(ope.apellido_paterno),' ',trim(ope.apellido_materno)) as nombre_monitor "
                    +"    from work.operadores ope "
                    +"    join work.autorizaciones autos  on ope.rut=autos.autorizador_por "
                    +"    where autos.autorizador_por=auto.autorizador_por limit 1 "
                    +") as nombre_monitor "
                    +" from work.maquinarias maq"
                    +" join work.autorizaciones auto on auto.id_maquinaria=maq.id_maquinaria"
                    +" join work.operadores ope on ope.rut=auto.rut"
                    +" join work.areas area on ope.id_area=area.id_area  "
                    +" where area.id_area=? and maq.id_maquinaria=? and auto.fecha_inicio >=to_date(?,'DY MON DD YYYY') "
                    +" and auto.fecha_inicio <=to_date(?,'DY MON DD YYYY') order by auto.fecha_inicio desc "
                    +" ) t;";
                }
                else
                {
                    
                      
                         sql ="select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                        +" from "
                        +" (select auto.id_autorizacion,area.area,ope.rut as rut_operador ,maq.nombre_maquinaria, "
                        +" CONCAT (trim(nombre),' ',trim(apellido_paterno),' ',trim(apellido_materno)) as nombre_operador, "
                        +" ope.cargo,auto.fecha_inicio as fecha_autorizacion, "
                        +" auto.fecha_termino as fecha_termino_autorizacion,auto.tipo_autorizacion,auto.autorizador_por as rut_monitor, "
                        +" ( "
                        +"    select CONCAT (trim(ope.nombre),' ',trim(ope.apellido_paterno),' ',trim(ope.apellido_materno)) as nombre_monitor "
                        +"    from work.operadores ope "
                        +"    join work.autorizaciones autos  on ope.rut=autos.autorizador_por "
                        +"    where autos.autorizador_por=auto.autorizador_por limit 1 "
                        +") as nombre_monitor "
                        +" from work.maquinarias maq"
                        +" join work.autorizaciones auto on auto.id_maquinaria=maq.id_maquinaria"
                        +" join work.operadores ope on ope.rut=auto.rut"
                        +" join work.areas area on ope.id_area=area.id_area "
                        +" where auto.fecha_inicio >=to_date(?,'DY MON DD YYYY') "
                        +" and auto.fecha_inicio <=to_date(?,'DY MON DD YYYY') order by auto.fecha_inicio desc "
                        +" ) t;";
                      
                } 

                  

            }   
         }   
        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };
       
        if(area == 0 && equipo !=0)
         {
            return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql,equipo,fecha,fecha_termino )).get(0);
         }
         else
         {
            if(equipo == 0 && area !=0)
            {
               return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql,area,fecha,fecha_termino )).get(0); 
            }
            else
            {
                if(equipo != 0 && area != 0)
                 {   
                 return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql,area,equipo,fecha,fecha_termino )).get(0); 
                }
                else
                 {
                    return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql,fecha,fecha_termino)).get(0);
                 }   
            }    
         
         }   
    }

    public static String ReporteAutorizacionMaquina(Integer area,Integer equipo,Integer reporte) {
    
                       String sql ="select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                        +" from "
                        +" (select area.area,ope.rut as rut_operador ,maq.nombre_maquinaria, "
                        +" CONCAT (trim(nombre),' ',trim(apellido_paterno),' ',trim(apellido_materno)) as nombre_operador, "
                        +" ope.cargo,auto.fecha_inicio as fecha_autorizacion, "
                        +" auto.fecha_termino as fecha_termino_autorizacion,auto.tipo_autorizacion,auto.autorizador_por as rut_monitor, "
                        +" ( "
                        +"    select CONCAT (trim(ope.nombre),' ',trim(ope.apellido_paterno),' ',trim(ope.apellido_materno)) as nombre_monitor "
                        +"    from work.operadores ope "
                        +"    join work.autorizaciones autos  on ope.rut=autos.autorizador_por "
                        +"    where autos.autorizador_por=auto.autorizador_por limit 1 "
                        +") as nombre_monitor "
                        +" from work.maquinarias maq"
                        +" join work.autorizaciones auto on auto.id_maquinaria=maq.id_maquinaria"
                        +" join work.operadores ope on ope.rut=auto.rut"
                        +" join work.areas area on ope.id_area=area.id_area "
                        +" where maq.id_maquinaria=? order by auto.fecha_inicio desc "
                        +" ) t;";
            
        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };
       
            return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql,equipo)).get(0);
         
    }

    public static String BuscadorMaquinarias() {

        String sql="SELECT json_agg(row_to_json(c)) "
                    +" FROM ( select concat(a.id_area,';') as id,a.area as label, "                        
                    +" ( SELECT json_agg(row_to_json(w)) "
                    +" FROM ( SELECT ma.id_maquinaria as id ,ma.nombre_maquinaria as label from work.maquinarias ma "
                    +" WHERE ma.id_area = a.id_area "
                    +" ) w "
                    +" ) children "
                    +" FROM work.areas a "
                    +" ) c;";                 
        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql )).get(0); 
    }

    public static String BuscadorGrupos() {

        String sql="SELECT json_agg(row_to_json(c)) "
                +" FROM ( select a.id_area as id,a.area as label, "                        
                +" ( SELECT json_agg(row_to_json(w)) "
                +" FROM ( SELECT concat(a.id_area,';',gp.id_grupo) as id,concat(a.area,' - - ',gp.grupo_turno) as label from work.grupos gp"
                +" WHERE gp.id_area = a.id_area "
                +" ) w "
                +" ) children "
                +" FROM work.areas a "
                +" ) c;";          
        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql )).get(0); 
    }

    public static String BuscadorGruposParaActividades() {

        String sql="SELECT json_agg(row_to_json(c)) "
                +" FROM ( select concat(a.id_area,';') as id,a.area as label, "                        
                +" ( SELECT json_agg(row_to_json(w)) "
                +" FROM ( SELECT gp.id_grupo as id,concat(a.area,' - - ',gp.grupo_turno) as label from work.grupos gp"
                +" WHERE gp.id_area = a.id_area "
                +" ) w "
                +" ) children "
                +" FROM work.areas a "
                +" ) c;";          
        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql )).get(0); 
    }

    public static String BuscadorMonitores() {

      String sql ="select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                 +" from " 
                 +" (SELECT CONCAT (trim(nombre),' ',trim(apellido_paterno),' ',trim(apellido_materno)) as nombre,rut, fecha_nacimiento,apellido_paterno,apellido_materno "
                 +" FROM work.operadores where id_area=6 and estado_vigencia='VIGENTE' order by nombre asc"
                 +" ) t;";
                  
       // log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql )).get(0); 
    }

    public static String BuscadorProgramas() {

      String sql ="select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                +" from "
                +" (select id_programa as id, nombre as label "
                +" from work.programas where estado_programa='ACTIVO') t;";
                  
       // log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql )).get(0); 
    }

    public static String BuscadorTipoActividades() {

      String sql ="select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                +" from "
                +" (select id_tipo_actividades as id, tipo_actividades as label "
                +" from work.tipo_actividades) t;";
                  
       // log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql )).get(0); 
    }

    public static String BuscadorSolicitantes() {

      String sql ="select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                +" from "
                +" (select id_solicitante as id, descripcion as label "
                +" from work.solicitantes) t;";
                  
       // log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql )).get(0); 
    }

    public static String ReporteCargos(Integer area) {

        String sql ="";

        if(area > 0)
        {    
            sql =" select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                +" from "
                +" (select ar.area as area,gr.grupo_turno as grupo ,ope.rut as rut,"
                +" CONCAT (trim(nombre),' ',trim(apellido_paterno),' ',trim(apellido_materno)) as nombre,"
                +" cg.cargo as cargo"
                +" from work.operadores ope "
                +" join work.areas ar on ope.id_area=ar.id_area "
                +" join work.grupos gr on ope.id_grupo=gr.id_grupo "
                +" join work.cargos cg ON ope.cargo =cg.id_cargo"
                +" where ope.id_area=?) t; ";
         }
         else
         {
                sql =" select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                +" from "
                +" (select ar.area as area,gr.grupo_turno as grupo ,ope.rut as rut,"
                +" CONCAT (trim(nombre),' ',trim(apellido_paterno),' ',trim(apellido_materno)) as nombre,"
                +" cg.cargo as cargo"
                +" from work.operadores ope "
                +" join work.areas ar on ope.id_area=ar.id_area "
                +" join work.grupos gr on ope.id_grupo=gr.id_grupo "
                +" join work.cargos cg ON ope.cargo =cg.id_cargo"
                +" ) t; ";

         }         
        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };

        if(area >0)
        {  
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql ,area)).get(0); 
        }
        else
        {
            return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql )).get(0);
        }    
    }

    public static String ReporteEquipos(Integer area) {
        String sql ="";

        if(area > 0)
        {    
            sql =" select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                +" from "
                +" (select maq.id_maquinaria,maq.nombre_maquinaria,maq.estado_vigencia "
                +" from work.areas ar "
                +" join work.maquinarias maq on maq.id_area=ar.id_area  "
                +" where maq.id_area=? order by maq.nombre_maquinaria asc) t; ";
         }
         else
         {
                sql =" select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                +" from "
                +" (select maq.id_maquinaria,maq.nombre_maquinaria,maq.estado_vigencia "
                +" from work.areas ar "
                +" join work.maquinarias maq on maq.id_area=ar.id_area order by maq.nombre_maquinaria asc  "
                +" ) t; ";

         }         
        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };

        if(area >0)
        {  
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql ,area)).get(0); 
        }
        else
        {
            return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql )).get(0);
        }    
    }

    public static String reporteAreas() {
   
       String sql ="SELECT json_agg(row_to_json(c)) "
                    +"FROM ( select area.id_area as id,area.area as label "  
                    +"FROM work.areas area order by area.id_area asc ) c;";

        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql )).get(0); 
    }

    public static String ReporteProgramas() {
   
       String sql ="select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                +" from "
                +" (select id_programa, nombre, estado_programa "
                +" from work.programas) t;";

        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql )).get(0); 
    }

    public static String ReporteActividades() {
   
       String sql ="select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                +" from "
                +" (select act.id_actividades, act.nombre as nombre_actividad, act.descripcion, "
                +" pro.nombre as nombre_programa, act.horas, act.estado_actividad as estado_actividad "
                +" from work.actividades act "
                +" join work.programas pro on act.id_programa=pro.id_programa "
                +" ) t;";

        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql )).get(0); 
    }

    public static String CargosCBO() {
   
       String sql ="SELECT json_agg(row_to_json(c)) "
                +" FROM ( select cg.id_cargo as id,cg.cargo as label  FROM work.cargos cg order by cg.cargo asc ) c;";

        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql )).get(0); 
    }

    public static String SuperintendenciaCBO() {
   
       String sql ="SELECT json_agg(row_to_json(c)) "
                +" FROM ( select cg.id_suptcia as id,cg.superintendencia as label  FROM work.superintendencia cg order by cg.superintendencia asc  ) c;";

        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql )).get(0); 
    }

    public static String TramoCBO() {
   
       String sql ="SELECT json_agg(row_to_json(c)) "
                +" FROM ( select id_tramo as id,tramo as label  FROM work.tramos order by tramo asc  ) c;";

        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql )).get(0); 
    }
    
    public static String ReporteEquipoEdit(Integer id_equipo) {
   
       
       String sql ="select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                   +" from "
                   +" (select maq.id_maquinaria,maq.nombre_maquinaria,maq.estado_vigencia "
                   +" from work.maquinarias maq  " 
                   +" where maq.id_maquinaria=?) t;";
                  
        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql ,id_equipo)).get(0); 
    }

    public static String ProgramaEdit(Integer id_programa) {
   
       
       String sql ="select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                   +" from "
                   +" (select id_programa, nombre, estado_programa "
                   +" from work.programas " 
                   +" where id_programa=?) t;";
                  
        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql ,id_programa)).get(0); 
    }

    public static String ActividadEdit(Integer id_actividad) {
   
       
       String sql ="select coalesce(array_to_json(array_agg(row_to_json(t))),'[]'::json) "
                +" from "
                +" (select act.nombre,act.id_programa,act.id_tipo_actividad,act.id_solicitante, "
                +" act.descripcion,act.horas,to_char(act.fecha_inicio, 'YYYY,MM,DD') as fecha_inicio, "
                +" to_char(act.fecha_termino, 'YYYY,MM,DD') as fecha_termino, "
                +" (select string_agg(cast(act_maq.id_maquinaria as varchar),',') "
                +" from work.actividad_maquinas act_maq  "
                +" where act_maq.id_actividades=?) as id_maquinarias, "
                +" (select string_agg(cast( gr.id_grupo as varchar),',') "
                +" from work.actividad_grupos actgr "
                +" join work.grupos gr on gr.id_grupo=actgr.id_grupo "
                +" where actgr.id_actividades=?) as id_grupos "
                +" from work.actividades act "
                +" where act.id_actividades=?) t;";
                  
        //log.debug(sql);
        RowMapper <String> mapper = new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int row) throws SQLException, java.io.UnsupportedEncodingException {
                String respuesta = new String(rs.getString(1));
                return respuesta;
            }
        };
        return (JDBCUtilities.getResultSetGeneric(DBAccess.getOMNIADS(),mapper,sql ,id_actividad,id_actividad,id_actividad)).get(0); 
    }

    
}
