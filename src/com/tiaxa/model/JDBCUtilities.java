package com.tiaxa.model;

import javax.sql.DataSource;
import java.io.UnsupportedEncodingException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Arrays;
import java.io.*;
import com.tiaxa.common.LogGK;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class JDBCUtilities {

    private static boolean debugSQL = true;

    public static void setDebug( boolean dbg )
    {
                debugSQL = dbg;
    }

    private static final Logger log = LoggerFactory.getLogger("com.tiaxa.sql") ;

    public static <T> ArrayList<T> getResultSetGeneric(DataSource ds, RowMapper<T> rowmapper ,String sql, Object... queryArgs) {
        log.debug(printArgs(sql,queryArgs));
        PreparedStatement stmt = null;
        Connection connection = null;
        ResultSet rs = null;
        ArrayList<T> resultList = new ArrayList<>();

        try {
            connection  = ds.getConnection();
            stmt = connection.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY,  ResultSet.CONCUR_READ_ONLY);
            setPreparedStatementArgs(stmt, queryArgs);
            rs = stmt.executeQuery();
            int row = 0;
            while (rs.next()) {
                row++;
                T t = rowmapper.mapRow(rs, row);
                resultList.add(t);
            }
        } catch (SQLException  e) {
            log.error("ERROR: getResultSetGeneric() SQLException ", e );
            return null;
        }
        catch (Exception e) {
            log.error("ERROR: getResultSetGeneric() Exception ", e );
            return null;
        }

        finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
                if (connection != null) connection.close();
            } catch (SQLException e) {
                log.error("ERROR: getResultSetGeneric() Silent SQLException ", e );
            }
        }
        return resultList;
    }

public static int queryForInt(DataSource ds, String sql, Object... queryArgs) {
        log.debug(printArgs(sql,queryArgs));
        PreparedStatement stmt = null;
        Connection connection = null;
        ResultSet rs = null;
        int res = 0;

        try {
            connection  = ds.getConnection();
            stmt = connection.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY,  ResultSet.CONCUR_READ_ONLY);
            setPreparedStatementArgs(stmt, queryArgs);
            rs = stmt.executeQuery();
            int row = 0;
            if (rs.next()) {
                row++;
                res = rs.getInt(1); 
            }
        } catch (SQLException  e) {
            log.error("ERROR: getResultSetGeneric() SQLException ", e );
            return -1;
        }
        catch (Exception e) {
            log.error("ERROR: getResultSetGeneric() Exception ", e );
            return -1;
        }

        finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
                if (connection != null) connection.close();
            } catch (SQLException e) {
                log.error("ERROR: getResultSetGeneric() Silent SQLException ", e );
            }
        }
        return res;
    }



    protected static String printArgs(String sqlQuery, Object[] args)
    { 
        return String.format("query \"%s\" - params %s",sqlQuery, Arrays.deepToString(args));
    }

    private static void setPreparedStatementArgs(PreparedStatement preparedStatement, Object queryArgs[])
            throws SQLException {
        int index = 0;
    if ( queryArgs != null )
        for (Object query_arg : queryArgs) {
            index++;
            if (query_arg == null)
                preparedStatement.setNull(index, Types.VARCHAR);
            else if (query_arg instanceof Integer)
                preparedStatement.setInt(index, (Integer) query_arg);
            else if (query_arg instanceof String)
                preparedStatement.setString(index, (String) query_arg);
            else if (query_arg instanceof Float)
                preparedStatement.setFloat(index, (Float) query_arg);
            else if (query_arg instanceof Double)
                preparedStatement.setDouble(index, (Double) query_arg);
            else if (query_arg instanceof byte[])
                preparedStatement.setBytes(index, (byte[]) query_arg);
            else if (query_arg instanceof Byte)
                preparedStatement.setByte(index, (Byte) query_arg);
            else if (query_arg instanceof Date)
                preparedStatement.setDate(index, (Date) query_arg);
            else if (query_arg instanceof Timestamp)
                preparedStatement.setTimestamp(index, (Timestamp) query_arg);
            else if (query_arg instanceof Time)
                preparedStatement.setTime(index, (Time) query_arg);
            else if (query_arg instanceof Long)
                preparedStatement.setLong(index, (Long) query_arg);
            else if (query_arg instanceof Short)
                preparedStatement.setShort(index, (Short) query_arg);
            else
                preparedStatement.setObject(index, query_arg);
        }
    }

    public static int executeUpdate(DataSource pool, String sql, Object... queryArgs) 
    {
        return executeUpdate(pool,sql,false,queryArgs);
    }

    public static int executeUpdate(DataSource pool,
                                     String sql, boolean getGeneratedKey, Object... queryArgs) {
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int index = 0;
        log.debug(printArgs(sql,queryArgs));
        try {
            connection = pool.getConnection();
            if (getGeneratedKey)
                stmt = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            else
                stmt = connection.prepareStatement(sql);
            setPreparedStatementArgs(stmt, queryArgs);
            index = stmt.executeUpdate();
            if (getGeneratedKey) {
                rs = stmt.getGeneratedKeys();
                while (rs.next()) {
                    index = rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            log.error("ERROR: executeUpdate() SQLException ", e );
            return -1;
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
                if (connection != null) connection.close();
            } catch (SQLException e) {
                log.error("ERROR: executeUpdate() silent - SQLException ", e );
            }
        }
        return index;
    }


    public static ArrayList<HashMap<String,String>> getResultSet(DataSource ds,String sql, Object... queryArgs)
    {
        RowMapper<HashMap<String,String>> rowmapper; 
        rowmapper = new RowMapper<HashMap<String, String>>() 
        {
            @Override
            public HashMap<String, String> mapRow(ResultSet rs, int row) throws SQLException,UnsupportedEncodingException
            {
                ResultSetMetaData rsmetada = rs.getMetaData();
                HashMap<String, String> hm = new HashMap<>();
                for (int i = 1; i <= rsmetada.getColumnCount(); i++) 
                {
                    String columnname = rsmetada.getColumnLabel(i);
                    
                    if (null != rsmetada.getColumnTypeName(i)) 
                    switch (rsmetada.getColumnTypeName(i)) {
                        case "DATETIME":
                            if (rs.getString(i) != null)
                            {
                                hm.put(columnname, rs.getDate(i) + " " + rs.getTime(i));
                            } else
                            {
                                hm.put(columnname, null);
                            }   break;
                        case "LONGVARCHAR":
                        case "CHAR":
                        case "VARCHAR":
                            if ( rs.getString(i) != null )
                                hm.put(columnname, new String(rs.getString(i).getBytes("UTF-8")));
                            else
                                hm.put(columnname, null);
                            break;
                        default:
                            if ( rs.getString(i) != null )
                                hm.put(columnname, rs.getString(i));
                            else
                                hm.put(columnname, null);
                            break;
                    }
                }

                return hm;
            }
        };

        return getResultSetGeneric(ds,rowmapper,sql,queryArgs);
    }



    public static Page<HashMap<String,String>> fetchPage( final DataSource ds, final String sqlCountRows,
        final String sqlFetchRows,
        final Object args[],
        final int pageNo,
        final int pageSize) 
    {
        // determine how many rows are available
        Page<HashMap<String,String>> page = new Page<>();
        int rowCount = queryForInt(ds,sqlCountRows, args);

        if ( rowCount < 1 ) 
        {
            //log.debug(" rows < 1 --> pagination");
            page.setPageNumber(1);
            page.setPagesAvailable(0);
            return page;
        }

        // calculate the number of pages
        //int pageCount = (int)Math.ceil(( 1f* rowCount )/ pageSize);
        int pageCount = (int)Math.ceil((1.f*rowCount) / pageSize);
        // create the page object
        page.setPageNumber(pageNo);
        page.setPagesAvailable(pageCount);
        // fetch a single page of results
        int startRow = (pageNo - 1) * pageSize;
        log.debug(String.format("%s LIMIT %d OFFSET %d" , sqlFetchRows, pageSize, startRow));
        page.setPageItems(getResultSet(ds,String.format("%s LIMIT %d OFFSET %d" , sqlFetchRows, pageSize, startRow),args));

        return page;
    }

    public static int executeInsert(DataSource pool, String sql, Object... queryArgs) throws SQLException 
{
    return executeInsert(pool,sql,false,queryArgs);
}

public static int executeInsert(DataSource pool, String sql, boolean getGeneratedKey, Object... queryArgs) throws SQLException
{
    Connection connection = null;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    int index = 0;
    if (debugSQL) LogGK.log(printArgs(sql,queryArgs));
    try {
        connection = pool.getConnection();
        if (getGeneratedKey)
            stmt = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
        else
            stmt = connection.prepareStatement(sql);
        setPreparedStatementArgs(stmt, queryArgs);
        index = stmt.executeUpdate();
        if (getGeneratedKey)
        {
            rs = stmt.getGeneratedKeys();
            while (rs.next()) {
                index = rs.getInt(1);
            }
        }
    } catch (Exception e) {
        LogGK.log("ERROR: executeInsert() SQLException ", e );
        LogGK.syncLog();
        throw new SQLException(e);
    } finally {
        try {
            if (rs != null) rs.close();
            if (stmt != null) stmt.close();
            if (connection != null) connection.close();
        } catch (SQLException e) {
            LogGK.log("ERROR: executeInsert() silent - SQLException ", e );
        LogGK.syncLog();
        throw e;
        }
    }
    return index;
}

}
