package com.tiaxa.model;


import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.*;
import java.sql.*;
import javax.naming.*;
import java.util.Enumeration;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;


public class DBAccess implements ServletContextListener
{

private static DataSource poolOMNIA;
private static final Logger log = LoggerFactory.getLogger(DBAccess.class);


@Override
public void contextInitialized(ServletContextEvent event)
{

	try
	{
		Context env = (Context) new InitialContext().lookup("java:comp/env");

		poolOMNIA = (DataSource) env.lookup("jdbc/anglo_pcm");
		if (poolOMNIA == null)
		{
			System.out.println("\n\n 'jdbc/anglo_pcm' is an unknown DataSource \n\n");
			throw new RuntimeException("'jdbc/anglo_pcm' is an unknown DataSource");
		}

	}
	catch (NamingException e)
	{
		throw new RuntimeException(e);
	}

}

@Override
public void contextDestroyed(ServletContextEvent event)
{
                Enumeration<Driver> drivers = DriverManager.getDrivers();
                while (drivers.hasMoreElements()) {
                        Driver driver = drivers.nextElement();
                        try {
                                DriverManager.deregisterDriver(driver);
                                System.out.println(String.format("Deregistering jdbc driver: %s", driver));
                        } catch (SQLException e) {
                                System.err.println(String.format("Error deregistering driver %s", driver));
                                e.printStackTrace(System.err);
                        }

                }
                MDC.clear();
}

public static DataSource getOMNIADS()
{
	return  poolOMNIA;
}

}
