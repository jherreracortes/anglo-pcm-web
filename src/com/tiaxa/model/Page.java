package com.tiaxa.model;
import java.util.List;
import java.util.ArrayList;



public class Page<E> {
        private int pageNumber;
        private int pagesAvailable;
        private List<E> pageItems = new ArrayList<>();
        public void setPageNumber(int pageNumber) {
            this.pageNumber = pageNumber;
        }
        public void setPagesAvailable(int pagesAvailable) {
            this.pagesAvailable = pagesAvailable;
        }
        public void setPageItems(List<E> pageItems) {
            this.pageItems = pageItems;
        }
        public int getPageNumber() {
            return pageNumber;
        }
        public int getPagesAvailable() {
            return pagesAvailable;
        }
        public List<E> getPageItems() {
            return pageItems;
        }
       public String toString(){
            return String.format("pageNumber = %d , pagesAvailable = %d , size of pageItems(%d)", pageNumber, pagesAvailable, pageItems.size());
        }

    }
