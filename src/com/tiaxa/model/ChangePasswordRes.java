package com.tiaxa.model;


import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ChangePasswordRes 
{

    private boolean modPassword = false;
    private boolean bissPassword = false;

    public boolean isModPassword() {
        return modPassword;
    }
    public boolean isBissPassword() {
        return bissPassword;
    }

    public void setModPassword(boolean modPassword) {
        this.modPassword = modPassword;
    }
    public void setBissPassword(boolean bissPassword) {
        this.bissPassword = bissPassword;
    }

}

