package com.tiaxa.common;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Date;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.lang.ThreadLocal;


public class LogGK {

    private static String logPattern = null;
    static BufferedWriter fdLog = null;
    private static final Lock mutexFdLog = new ReentrantLock(true);

    private static final ThreadLocal<DateFormat> dfLogSuffix = new ThreadLocal<DateFormat>()
    {
        @Override
            protected DateFormat initialValue() {
                return new SimpleDateFormat("yyyyMMddHH");
            }
    };

    private static final ThreadLocal<DateFormat> dfTimeLog = new ThreadLocal<DateFormat>()
    {
        @Override
            protected DateFormat initialValue() {
                return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            }

    };


    private static String logSuffix = "";

    static void setLogPattern(String pattern) throws IOException {
        logPattern = pattern;
        logSuffix = dfLogSuffix.get().format(new Date());
        String file = String.format("%s.%s.log", logPattern, logSuffix);
        //System.out.println("OUT -- " + file );
        fdLog = new BufferedWriter(new FileWriter(file, true));
    }

    static public int log(String strlog) {
        return log(strlog, null);
    }

    static public void syncLog() {
        try {
            if (fdLog != null) {
                fdLog.flush();
            }
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        } finally {
        }
    }

    static public int log(String strlog, Throwable throwable) {


        try {

            mutexFdLog.lock();
            String timeSuffix = dfLogSuffix.get().format(new Date());
            if (timeSuffix.compareTo(logSuffix) != 0) {
                if (fdLog != null) {
                    fdLog.write("==> LOG FILE CLOSED FOR ROTATING <==\n");
                    fdLog.flush();
                    fdLog.close();
                }
                logSuffix = timeSuffix;
                String file = String.format("%s.%s.log", logPattern, logSuffix);
                fdLog = new BufferedWriter(new FileWriter(file, true));
                fdLog.write("==> ROTATING LOG NEW FILE OPEN <==\n");
            }
            mutexFdLog.unlock();

            if (throwable == null) {
                fdLog.write(String.format("[%s] %s\n", dfTimeLog.get().format(new Date()), strlog));

            } else {
                StringWriter errors = new StringWriter();
                throwable.printStackTrace(new PrintWriter(errors));
                fdLog.write(String.format("[%s] . %s -> %s\n", dfTimeLog.get().format(new Date()), strlog, errors.toString()));
            }
        } catch (Exception e) {
            System.err.print("Error escribiendo log");
            e.printStackTrace(System.err);
            System.exit(-1);
        } finally {
        }

        return 0;
    }

    public static void shutDownLog() {
        if (fdLog != null)
        {
            try {
                fdLog.flush();
                fdLog.close();
            } catch (IOException ex) {
                System.err.println("Error closing log file");
                ex.printStackTrace(System.err);
            }
           
        }
    }
}
