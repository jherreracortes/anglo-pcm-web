package com.tiaxa.login;
import java.util.*;

public final class UserContext {

	public UserContext(String username , String perfilid) 
        {
		setUser(username);
		setPerfilId(perfilid);
	}

	public UserContext()
        {
		
	}

        ArrayList<HashMap<String,String>> allowedUrls = new ArrayList<>(); 
	String userName  = null;
	String perfil_id = null;
	String admin_Role = null;
	String passw_date = null;
	String user_date = null;
	String user_status = null;
	Integer organization_Id = null;
        boolean expiredAccount = false;
        boolean inactiveAccount = false;
    Integer bot_id = null;
    String bot_availables = null;
    Integer id_client = null;

    public void setIdClient(Integer client) {
        this.id_client = client;
    }

    public Integer getIdClient() {
        return this.id_client;
    }

    public void setBotsAvailable(String bots) {
        this.bot_availables = bots;
    }

    public String getBotsAvailable() {
        return bot_availables;
    }

    public void setBotId(Integer botId) {
        this.bot_id = botId;
    }

    public Integer getBotId() {
        return this.bot_id;
    }

	public void setUser(String username_) 
	{
		this.userName = username_;
	}

	public final void setPerfilId(String perfil_id) 
        {
		this.perfil_id= perfil_id;
	}
        public void setExpired(boolean expired)
        {
                this.expiredAccount = expired;
        }
        public void setUserRol(String admin_Role)
        {
                this.admin_Role= admin_Role;
        }
        public void setOrganizationId(String organizationId)
        {
                this.organization_Id= Integer.parseInt(organizationId);
        }
        public void setInactiveAccount(boolean inactive)
        {
                this.inactiveAccount = inactive;
        }
        public void setPasswDate(String passw_date)
        {
                this.passw_date= passw_date;
        }
        public void setUserDate(String user_date)
        {
                this.user_date= user_date;
        }
        public void setUserStatus(String user_status)
        {
                this.user_status= user_status;
        }

	public boolean isExpired()
	{
		return this.expiredAccount;
	}
	public boolean isInactiveAccount()
	{
	   if ("INACTIVE".equals(this.user_status))
	   {
		this.inactiveAccount=true;
	   }
		return this.inactiveAccount;
	}



	public String getPerfilID()
	{
		return this.perfil_id;
	}
        public String getUserRol()
        {
                return this.admin_Role;
        }
        public Integer getOrganizationId()
        {
                return this.organization_Id;
        }
        public String getPasswDate()
        {
                return this.passw_date;

        }
        public String getUserDate()
        {
                return this.user_date;
        }
        public String getUserStatus()
        {
                return this.user_status;
        }


	public String getUserName()
	{
		return this.userName;
	}
	
	public void setAllowedUrls( ArrayList<HashMap<String,String>> urls ) 
	{
		this.allowedUrls  = urls;
	}

        @Override
	public String toString() 
	{
		return (String.format(" USERNAME : %s - TAS_PROFILE_ID %s", this.userName , this.perfil_id ));
	}
      
        public boolean checkProtected(String currentURL) 
        {
		for ( HashMap<String,String> allowedUrl : allowedUrls )
		{
			
                    if ( currentURL.startsWith(allowedUrl.get("location")) ) 
			{

				if ("Y".equals(allowedUrl.get("allowedLocation")))
				{
					return true;
				}
				if ("N".equals(allowedUrl.get("allowedLocation")))
				{
					return false;
				}
			}
		
		}
			return true;
        }

}

