package com.tiaxa.login;

import com.tiaxa.model.ServiceModel;

import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

public class FormLoginFilter implements Filter {

	protected FilterConfig filterConfig;
	public final static String sessionkey ="USERCONTEXT";

	private static final Logger log = LoggerFactory.getLogger("FormLoginFilter.class") ;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	@Override
	public void destroy() {
		this.filterConfig = null;
	}

	public static UserContext getUserContext(HttpSession session) {
		if (session == null)
			return null;
		UserContext uc = (UserContext) session.getAttribute(sessionkey);
		return uc;
	}

	private static void setUserContext(HttpSession session, UserContext uc) {
		session.setAttribute(sessionkey, uc);
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain) throws java.io.IOException, ServletException
{

	HttpServletRequest req    = (HttpServletRequest)request;
	HttpServletResponse res   = (HttpServletResponse)response;
	HttpSession session       = req.getSession();

	if (session.getAttribute(sessionkey)!=null )
	{

			UserContext currentUser = getUserContext(session);
			String prtPath = req.getRequestURI() ;

			if ( currentUser == null  )
			{
					MDC.put("userName", getUserContext(session).getUserName());
					MDC.put("sid", session.getId());
					RequestDispatcher rd = req.getRequestDispatcher("/login.shtml");
					rd.forward(req, res);
					MDC.remove("userName");
					MDC.remove("sid");
					return;
			}


			if ( currentUser.isExpired())
			{
					RequestDispatcher rd = req.getRequestDispatcher("/suspended.shtml");
					rd.forward(req, res);
					return;
			}
			else if (true || currentUser.checkProtected( prtPath))
			{
					MDC.put("userName", getUserContext(session).getUserName());
					MDC.put("sid", session.getId());
					chain.doFilter(new UserRoleRequestWrapper( getUserContext(session).getUserName() , req) , response);
					MDC.remove("userName");
					MDC.remove("sid");
					return;
			}
			else
			{
					MDC.put("userName", getUserContext(session).getUserName());
					MDC.put("sid", session.getId());
					RequestDispatcher rd = req.getRequestDispatcher("/403.shtml");
					rd.forward(req, res);
					MDC.remove("userName");
					MDC.remove("sid");
					return;
			}
	}else
	{
			boolean auth    = false;
			String username = req.getParameter("j_username");
			String password = req.getParameter("j_password");
			UserContext uc  = new UserContext();

			if ( "POST".equals(req.getMethod()))
			{
				 	HashMap<String,String> user = ServiceModel.authenticateUser(username,password);

					log.debug("user " + username);
					log.debug("password " + password);

					System.out.println("mvc user:" + username);

					req.setAttribute("no_login",null);
					if ( user != null)
					{
						boolean expired = false ;
    				uc.setExpired(expired);
						uc.setUser(username);
						//log.debug("Bots: " + bot_ids);
						setUserContext(session, uc);
						session.setAttribute("username",username);
						System.out.println("mvc user user != null:" +session.getAttribute("username"));
						auth = true;
					}
					else
					{
							req.setAttribute("no_login","no_login");
					}
			}

			if (auth == false)
			{
					RequestDispatcher rd = req.getRequestDispatcher("/login.shtml");
					res.addHeader("REQUIRES_AUTH", "1");
					rd.forward(req, res);
					return;
			}
			else if ( uc.isExpired() )
			{
					RequestDispatcher rd = req.getRequestDispatcher("/suspended.shtml");
					rd.forward(req, res);
					return;
			}
			else if ( uc.isInactiveAccount() )
			{
					RequestDispatcher rd = req.getRequestDispatcher("/suspended.shtml");
					rd.forward(req, res);
					return;
			}
			else
			{
					RequestDispatcher rd = req.getRequestDispatcher("/index.html");
					rd.forward(req, res);
					return;
			}
	}
}
}
