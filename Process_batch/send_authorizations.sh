#! /bin/sh

DB_HOST=40.76.33.201
DB_NAME=anglo_pcm
DB_USER=anglo_pcm
DB_PASSWORD=anglo_pcm
FECHA=`date '+%Y-%m-%d'`
CREDENCIALES=/home/jherrera/Documentos/test_mail/credenciales.txt
PAD_ARCHIVO_AUTORIZACIONES=/home/jherrera/Documentos/data_enso/AutorizacionesDispatch.csv

echo "**********************************************"
date '+%Y-%m-%d %H:%M:%S'

echo "*************** GENERANDO CSV Y GUARDANDO ARCHIVO EN EL DIRECTORIO $PAD_ARCHIVO_AUTORIZACIONES ***************************"
date '+%Y-%m-%d %H:%M:%S'
PGPASSWORD=$DB_PASSWORD psql -U $DB_USER -h$DB_HOST -d $DB_NAME -t -A -F";" -c "SELECT op.sap as sap,maq.id_dispatch
	 ,2 as capacitado, concat (op.nombre, ' ', op.apellido_paterno,' ', op.apellido_materno) as operador
	 ,gr.grupo_turno as turno,ar.area as area
	FROM work.autorizaciones aut 
	left join work.operadores op on aut.rut = op.rut
 	left join work.maquinarias  maq on aut.id_maquinaria = maq.id_maquinaria
	left join work.grupos gr on op.id_grupo = gr.id_grupo
	left join work.areas ar on op.id_area = ar.id_area
	where
	aut.tipo_autorizacion = 'A'
	AND op.estado_vigencia = 'VIGENTE'
	AND id_dispatch is not null
	AND maq.estado_vigencia = 'ACTIVO' 
	AND (op.cargo = 4
	 OR op.cargo = 6
	 OR op.cargo = 7
	 OR op.cargo = 8
	 OR op.cargo = 12
	 OR op.cargo = 14
	) 
	group by aut.rut, maq.id_dispatch, 
	op.sap, op.nombre, op.apellido_paterno, op.apellido_materno, gr.grupo_turno,  ar.area 
	order by operador asc
	;" > $PAD_ARCHIVO_AUTORIZACIONES

date '+%Y-%m-%d %H:%M:%S'
echo "*************** TERMINO DEL GENERADO CSV ***************************"

echo "************ COPIANDO ARCHIVO POR smbclient **********************************"
date '+%Y-%m-%d %H:%M:%S'
smbclient //172.16.1.104/qwerty  -A $CREDENCIALES -R host -c 'put /home/jherrera/Documentos/data_enso/AutorizacionesDispatch.csv AutorizacionesDispatch.csv'

echo "************  FIN DEL COPIANDO DEL ARCHIVO **********************************"
date '+%Y-%m-%d %H:%M:%S'
