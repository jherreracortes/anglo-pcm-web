CREATE TYPE config.e_user_status AS ENUM (
	'Habilitado',
	'Inhabilitado');

CREATE TABLE config."user" (
	id_user serial NOT NULL,
	email varchar(255) NOT NULL,
	"name" varchar(80) NOT NULL,
	lastname varchar(80) NOT NULL,
	"password" varchar(255) NOT NULL,
	status config.e_user_status NULL,
	CONSTRAINT user_pk PRIMARY KEY (id_user)
);

