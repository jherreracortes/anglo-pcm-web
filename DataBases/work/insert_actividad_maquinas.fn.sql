 CREATE OR REPLACE FUNCTION work.insert_actividad_maquinas(p_nombre character varying, p_descripcion character varying, p_id_programa integer, p_horas integer, p_estado character varying, p_id_maquinas text[], p_id_grupos text[], p_fecha_inicio character varying, p_fecha_termino character varying, p_id_tipo_actividad integer, p_id_solicitante integer)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$

DECLARE

r_id_actividades INTEGER;
r_valor INTEGER;
r_id_grupos INTEGER;

BEGIN

INSERT INTO "work".actividades (nombre, descripcion, id_programa, horas, estado_actividad,fecha_inicio,fecha_termino,id_tipo_actividad,id_solicitante) 
VALUES(p_nombre,p_descripcion, p_id_programa, p_horas, p_estado ,to_date(p_fecha_inicio,'DY MON DD YYYY'),to_date(p_fecha_termino,'DY MON DD YYYY'),p_id_tipo_actividad,p_id_solicitante);
SELECT currval(pg_get_serial_sequence('actividades','id_actividades')) into r_id_actividades;
FOR i IN array_lower(p_id_maquinas,1) .. array_upper(p_id_maquinas,1) LOOP
SELECT CAST(nullif(p_id_maquinas[i], '') AS integer) into r_valor;
INSERT INTO "work".actividad_maquinas (id_actividades, id_maquinaria)
VALUES(r_id_actividades, r_valor);

END LOOP;

FOR i IN array_lower(p_id_grupos,1) .. array_upper(p_id_grupos,1) LOOP
SELECT CAST(nullif(p_id_grupos[i], '') AS integer) into r_id_grupos;
INSERT INTO "work".actividad_grupos (id_actividades, id_grupo)
VALUES(r_id_actividades, r_id_grupos);

END LOOP;

return 0;

END;

$function$
;

