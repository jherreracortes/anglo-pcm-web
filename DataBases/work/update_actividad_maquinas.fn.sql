CREATE OR REPLACE FUNCTION work.update_actividad_maquinas(p_nombre character varying, p_descripcion character varying, p_id_programa integer, p_horas integer, p_estado character varying, p_id_maquinas text[], p_id_grupos text[], p_fecha_inicio character varying, p_fecha_termino character varying,
 p_id_tipo_actividad integer, p_id_solicitante integer, p_id_actividad integer)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$

DECLARE

r_valor INTEGER;
r_id_grupos INTEGER;

BEGIN

UPDATE work.actividades SET nombre=p_nombre, descripcion=p_descripcion, id_programa=p_id_programa, horas=p_horas, estado_actividad=p_estado, 
fecha_inicio=to_date(p_fecha_inicio,'DY MON DD YYYY'), fecha_termino=to_date(p_fecha_termino,'DY MON DD YYYY'), id_tipo_actividad=p_id_tipo_actividad,
id_solicitante=p_id_solicitante
WHERE id_actividades=p_id_actividad;

DELETE FROM work.actividad_grupos
WHERE id_actividades=p_id_actividad;

DELETE FROM work.actividad_maquinas
WHERE id_actividades=p_id_actividad;

FOR i IN array_lower(p_id_maquinas,1) .. array_upper(p_id_maquinas,1) LOOP
SELECT CAST(nullif(p_id_maquinas[i], '') AS integer) into r_valor;
INSERT INTO "work".actividad_maquinas (id_actividades, id_maquinaria)
VALUES(p_id_actividad, r_valor);

END LOOP;

FOR i IN array_lower(p_id_grupos,1) .. array_upper(p_id_grupos,1) LOOP
SELECT CAST(nullif(p_id_grupos[i], '') AS integer) into r_id_grupos;
INSERT INTO "work".actividad_grupos (id_actividades, id_grupo)
VALUES(p_id_actividad, r_id_grupos);

END LOOP;

return 0;

END;

$function$
;
