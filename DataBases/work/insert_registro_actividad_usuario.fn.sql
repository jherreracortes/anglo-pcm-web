CREATE OR REPLACE FUNCTION work.insert_registro_actividad_usuario(p_username character varying, p_modulo character varying, p_descripcion character varying, p_tipo_de_actividad character varying)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$

DECLARE

r_id_actividades INTEGER;

BEGIN


SELECT id_user into r_id_actividades FROM config."user" where email=p_username;
INSERT INTO work.registro_actividades_usuarios (id_usuario, modulo, descripcion, tipo_de_actividad, fecha) 
VALUES(r_id_actividades, p_modulo, p_descripcion, p_tipo_de_actividad, now());

return 0;

END;

$function$;
