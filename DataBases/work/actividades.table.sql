CREATE TABLE "work".actividades (
	id_actividades serial NOT NULL,
	nombre varchar(255) NOT NULL,
	descripcion varchar(255) NOT NULL,
	id_programa int4 NOT NULL,
	horas int4 NULL,
	estado_actividad varchar(50) NULL,
	fecha_inicio date NULL,
	fecha_termino date NULL,
	id_tipo_actividad int4 NULL,
	id_solicitante int4 NULL,
	CONSTRAINT actividades_pk PRIMARY KEY (id_actividades),
	CONSTRAINT actividades_fk FOREIGN KEY (id_solicitante) REFERENCES solicitantes(id_solicitante),
	CONSTRAINT actividades_tipo_actividades_fk FOREIGN KEY (id_tipo_actividad) REFERENCES tipo_actividades(id_tipo_actividades),
	CONSTRAINT fk_programas_programa FOREIGN KEY (id_programa) REFERENCES programas(id_programa)
);
