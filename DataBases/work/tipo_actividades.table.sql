 CREATE TABLE "work".tipo_actividades (
	id_tipo_actividades serial NOT NULL,
	tipo_actividades varchar(50) NOT NULL,
	CONSTRAINT tipo_actividades_pk PRIMARY KEY (id_tipo_actividades)
);

INSERT INTO "work".tipo_actividades (id_tipo_actividades, tipo_actividades) VALUES(1, 'Difusion');
INSERT INTO "work".tipo_actividades (id_tipo_actividades, tipo_actividades) VALUES(2, 'Cierre Brechas');
INSERT INTO "work".tipo_actividades (id_tipo_actividades, tipo_actividades) VALUES(3, 'Reinstruccion');
INSERT INTO "work".tipo_actividades (id_tipo_actividades, tipo_actividades) VALUES(4, 'Toma de Conocimiento');
