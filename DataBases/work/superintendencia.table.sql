 CREATE TABLE "work".superintendencia (
	id_suptcia serial NOT NULL,
	superintendencia varchar(50) NOT NULL,
	CONSTRAINT superintendencia_pkey PRIMARY KEY (id_suptcia)
);

INSERT INTO "work".superintendencia (id_suptcia, superintendencia) VALUES(1, 'Servicios LB');
INSERT INTO "work".superintendencia (id_suptcia, superintendencia) VALUES(2, 'Dewatering y Donoso II');
INSERT INTO "work".superintendencia (id_suptcia, superintendencia) VALUES(3, 'Perforacion y Tronadura LB');
INSERT INTO "work".superintendencia (id_suptcia, superintendencia) VALUES(4, 'Operaciones Mina LB');
INSERT INTO "work".superintendencia (id_suptcia, superintendencia) VALUES(5, 'Gestion Mina LB');
INSERT INTO "work".superintendencia (id_suptcia, superintendencia) VALUES(6, 'Mantencion Mina LB');
INSERT INTO "work".superintendencia (id_suptcia, superintendencia) VALUES(7, 'Confiabilidad Mina LB');

