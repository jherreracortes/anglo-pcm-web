 CREATE TABLE "work".solicitantes (
	id_solicitante serial NOT NULL,
	descripcion varchar(100) NOT NULL,
	CONSTRAINT solicitantes_pkey PRIMARY KEY (id_solicitante)
);


INSERT INTO "work".solicitantes (id_solicitante, descripcion) VALUES(1, 'Gerencia Mina');
INSERT INTO "work".solicitantes (id_solicitante, descripcion) VALUES(2, 'Superintendencia de Operaciones');
INSERT INTO "work".solicitantes (id_solicitante, descripcion) VALUES(3, 'Superintendencia de Servicios');
INSERT INTO "work".solicitantes (id_solicitante, descripcion) VALUES(4, 'Superintendencia de Perforación y Tronadura');
INSERT INTO "work".solicitantes (id_solicitante, descripcion) VALUES(5, 'Superintendencia Donoso y Dewatering');
INSERT INTO "work".solicitantes (id_solicitante, descripcion) VALUES(6, 'Seguridad y Salud Ocupacional');
INSERT INTO "work".solicitantes (id_solicitante, descripcion) VALUES(7, 'Recursos Humanos');
