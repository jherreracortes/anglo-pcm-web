 CREATE TABLE "work".maquinarias (
	id_maquinaria serial NOT NULL,
	nombre_maquinaria varchar(255) NOT NULL,
	id_area int4 NOT NULL,
	comentario varchar(255) NULL,
	estado_vigencia varchar(20) NOT NULL,
	CONSTRAINT maquinaria_pkey PRIMARY KEY (id_maquinaria),
	CONSTRAINT fk_areas_area FOREIGN KEY (id_area) REFERENCES areas(id_area)
);

INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(7, 'L-1400', 2, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(9, 'Tele-remoto Pala Hidraulica PC5500 BH Control', 2, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(14, 'D 11 T', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(15, '475 A', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(16, '854 K', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(20, 'D 10 R', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(23, 'L-1850', 2, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(24, '830', 1, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(25, '930', 1, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(26, '960', 1, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(27, '795', 1, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(28, 'D 10 T', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(31, '854 G', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(33, 'WD 900', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(34, '24 H', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(35, '16 M', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(37, '980 G', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(38, '980 H', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(39, '988 H', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(41, '773 D', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(42, 'HD 785', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(43, 'PC 450', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(44, '385 CL', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(45, 'CAT 345 B', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(47, 'Rodillo', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(48, 'Trac Agricola', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(49, 'Control Remoto', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(50, 'EXC 390D', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(51, 'PEB 5  Modelo 49 R II', 4, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(52, 'PEB 8  Modelo 59R', 4, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(53, 'PEB 10  Modelo 49 HR III', 4, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(54, 'PEB 9  Modelo 49 HR', 4, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(55, 'PV 351', 4, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(56, 'PV 271', 4, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(57, 'P/ANGULAR', 4, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(58, 'P/ANGULAR A DISTANCIA', 4, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(59, 'TOW HOUL', 4, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(61, 'Perforacion Remota desde sala CIGO', 4, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(62, 'Reinstruccion Instalacion 3era Barra PV351', 4, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(63, 'Entrenamiento Panel View PEB.08,09,10', 4, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(65, 'Pisten Bully 600 Polar', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(66, 'PB 170 DR', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(32, '824 G', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(67, 'Pisten Bully 300 Polar', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(36, '16 H', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(64, 'PV 271D MULTI PASS', 4, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(40, '992 K', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(4, 'Pala Eléctrica 495 HR II', 2, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(5, 'Pala Eléctrica CAT 7495', 2, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(2, 'Pala Eléctrica 495 B II', 2, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(1, 'Pala Eléctrica 495 BI', 2, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(3, 'Pala Eléctrica 495 HR', 2, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(6, 'Pala Hidraulica PC5500 BH', 2, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(8, 'Pala Hidraulica PC5500 FS', 2, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(46, 'Retroexcavadora  WB  164', 3, NULL, 'ACTIVO');
INSERT INTO "work".maquinarias (id_maquinaria, nombre_maquinaria, id_area, comentario, estado_vigencia) VALUES(60, 'Camión Manipulador de barra', 4, NULL, 'ACTIVO');


