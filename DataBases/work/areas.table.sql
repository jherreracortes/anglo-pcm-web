 CREATE TABLE "work".areas (
	id_area serial NOT NULL,
	area varchar(50) NOT NULL,
	comentario varchar(255) NULL,
	CONSTRAINT areas_pkey PRIMARY KEY (id_area)
);


INSERT INTO "work".areas (id_area, area, comentario) VALUES(1, 'Transporte', NULL);
INSERT INTO "work".areas (id_area, area, comentario) VALUES(2, 'Carguio', NULL);
INSERT INTO "work".areas (id_area, area, comentario) VALUES(3, 'Servicio', NULL);
INSERT INTO "work".areas (id_area, area, comentario) VALUES(4, 'Perforacion', NULL);
INSERT INTO "work".areas (id_area, area, comentario) VALUES(5, 'Mantencion Mina', NULL);
INSERT INTO "work".areas (id_area, area, comentario) VALUES(6, 'Capacitacion Mina', NULL);
