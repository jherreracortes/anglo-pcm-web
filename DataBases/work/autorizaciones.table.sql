CREATE TYPE work.e_operacion_invierno AS ENUM (
	'SI',
	'NO');

CREATE TABLE "work".autorizaciones (
	id_autorizacion serial NOT NULL,
	rut varchar(255) NOT NULL,
	id_maquinaria int4 NOT NULL,
	fecha_inicio date NOT NULL,
	fecha_termino date NULL,
	tipo_autorizacion varchar(255) NOT NULL,
	autorizador_por varchar(255) NULL,
	operacion_invierno "work".e_operacion_invierno NULL,
	id_programa int4 NULL,
	mde varchar(5) NULL,
	CONSTRAINT autorizaciones_pkey PRIMARY KEY (id_autorizacion, rut, id_maquinaria),
	CONSTRAINT fk_maquinarias_id_maquinaria FOREIGN KEY (id_maquinaria) REFERENCES maquinarias(id_maquinaria),
	CONSTRAINT fk_operadores_rut FOREIGN KEY (rut) REFERENCES operadores(rut),
	CONSTRAINT id_programa_fk_programas FOREIGN KEY (id_programa) REFERENCES programas(id_programa)
);
