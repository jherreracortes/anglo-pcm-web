 CREATE TABLE "work".cargos (
	id_cargo serial NOT NULL,
	cargo varchar(50) NOT NULL,
	CONSTRAINT cargos_pkey PRIMARY KEY (id_cargo)
);

INSERT INTO "work".cargos (id_cargo, cargo) VALUES(1, 'Mantenedor Principal');
INSERT INTO "work".cargos (id_cargo, cargo) VALUES(2, 'Administrativo');
INSERT INTO "work".cargos (id_cargo, cargo) VALUES(3, 'Coordinador Dewatering');
INSERT INTO "work".cargos (id_cargo, cargo) VALUES(4, 'Monitor entrenamiento');
INSERT INTO "work".cargos (id_cargo, cargo) VALUES(5, 'Mantenedor Sintomático');
INSERT INTO "work".cargos (id_cargo, cargo) VALUES(6, 'Operador Superior');
INSERT INTO "work".cargos (id_cargo, cargo) VALUES(7, 'Operador Principal');
INSERT INTO "work".cargos (id_cargo, cargo) VALUES(8, 'Monitor Capacitación');
INSERT INTO "work".cargos (id_cargo, cargo) VALUES(9, 'Mantenedor Estructural');
INSERT INTO "work".cargos (id_cargo, cargo) VALUES(10, 'Mantenedor Superior');
INSERT INTO "work".cargos (id_cargo, cargo) VALUES(11, 'Mantenedor Mecánico');
INSERT INTO "work".cargos (id_cargo, cargo) VALUES(12, 'Operador Mayor');
INSERT INTO "work".cargos (id_cargo, cargo) VALUES(13, 'Mantenedor Climatización');
INSERT INTO "work".cargos (id_cargo, cargo) VALUES(14, 'Operador Entrenamiento');
INSERT INTO "work".cargos (id_cargo, cargo) VALUES(15, 'Mantenedor Mayor');
INSERT INTO "work".cargos (id_cargo, cargo) VALUES(16, 'Dirigente Sindical');
