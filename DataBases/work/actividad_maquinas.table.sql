CREATE TABLE "work".actividad_maquinas (
	id_actividades int4 NOT NULL,
	id_maquinaria int4 NOT NULL,
	CONSTRAINT actividad_maquinas_actividades_fk FOREIGN KEY (id_actividades) REFERENCES actividades(id_actividades),
	CONSTRAINT actividad_maquinas_maquinarias_fk FOREIGN KEY (id_maquinaria) REFERENCES maquinarias(id_maquinaria)
);
