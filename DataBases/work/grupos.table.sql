 CREATE TABLE "work".grupos (
	id_grupo serial NOT NULL,
	grupo_turno varchar(25) NULL,
	id_area int4 NOT NULL,
	comentario varchar(255) NULL,
	CONSTRAINT grupos_pkey PRIMARY KEY (id_grupo),
	CONSTRAINT fk_grupos_areas FOREIGN KEY (id_area) REFERENCES areas(id_area)
);

INSERT INTO "work".grupos (id_grupo, grupo_turno, id_area, comentario) VALUES(1, 'G1', 1, NULL);
INSERT INTO "work".grupos (id_grupo, grupo_turno, id_area, comentario) VALUES(2, 'G2', 1, NULL);
INSERT INTO "work".grupos (id_grupo, grupo_turno, id_area, comentario) VALUES(3, 'G3', 1, NULL);
INSERT INTO "work".grupos (id_grupo, grupo_turno, id_area, comentario) VALUES(4, 'G4', 1, NULL);
INSERT INTO "work".grupos (id_grupo, grupo_turno, id_area, comentario) VALUES(5, 'G1', 2, NULL);
INSERT INTO "work".grupos (id_grupo, grupo_turno, id_area, comentario) VALUES(6, 'G2', 2, NULL);
INSERT INTO "work".grupos (id_grupo, grupo_turno, id_area, comentario) VALUES(7, 'G3', 2, NULL);
INSERT INTO "work".grupos (id_grupo, grupo_turno, id_area, comentario) VALUES(8, 'G4', 2, NULL);
INSERT INTO "work".grupos (id_grupo, grupo_turno, id_area, comentario) VALUES(9, 'G1', 3, NULL);
INSERT INTO "work".grupos (id_grupo, grupo_turno, id_area, comentario) VALUES(10, 'G2', 3, NULL);
INSERT INTO "work".grupos (id_grupo, grupo_turno, id_area, comentario) VALUES(11, 'G3', 3, NULL);
INSERT INTO "work".grupos (id_grupo, grupo_turno, id_area, comentario) VALUES(12, 'G4', 3, NULL);
INSERT INTO "work".grupos (id_grupo, grupo_turno, id_area, comentario) VALUES(13, 'G1', 4, NULL);
INSERT INTO "work".grupos (id_grupo, grupo_turno, id_area, comentario) VALUES(14, 'G2', 4, NULL);
INSERT INTO "work".grupos (id_grupo, grupo_turno, id_area, comentario) VALUES(15, 'G3', 4, NULL);
INSERT INTO "work".grupos (id_grupo, grupo_turno, id_area, comentario) VALUES(16, 'G4', 4, NULL);
INSERT INTO "work".grupos (id_grupo, grupo_turno, id_area, comentario) VALUES(17, 'G0', 5, NULL);
INSERT INTO "work".grupos (id_grupo, grupo_turno, id_area, comentario) VALUES(18, 'G00', 6, NULL);
INSERT INTO "work".grupos (id_grupo, grupo_turno, id_area, comentario) VALUES(19, 'G1', 6, NULL);
