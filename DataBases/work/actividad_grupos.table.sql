
CREATE TABLE "work".actividad_grupos (
	id_actividades int4 NOT NULL,
	id_grupo int4 NOT NULL,
	CONSTRAINT actividad_grupos_actividades_fk FOREIGN KEY (id_actividades) REFERENCES actividades(id_actividades),
	CONSTRAINT actividad_grupos_id_grupo_fk FOREIGN KEY (id_grupo) REFERENCES grupos(id_grupo)
);
