 CREATE TABLE "work".programas (
	id_programa serial NOT NULL,
	nombre varchar(255) NOT NULL,
	estado_programa varchar(50) NULL,
	CONSTRAINT programas_pkey PRIMARY KEY (id_programa)
);

INSERT INTO "work".programas (id_programa, nombre, estado_programa) VALUES(4, 'Avanza', 'ACTIVO');
INSERT INTO "work".programas (id_programa, nombre, estado_programa) VALUES(1, 'Inicia', 'ACTIVO');
INSERT INTO "work".programas (id_programa, nombre, estado_programa) VALUES(2, 'Puedo', 'ACTIVO');
INSERT INTO "work".programas (id_programa, nombre, estado_programa) VALUES(3, 'Mas', 'ACTIVO');
INSERT INTO "work".programas (id_programa, nombre, estado_programa) VALUES(5, 'MDE', 'ACTIVO');
