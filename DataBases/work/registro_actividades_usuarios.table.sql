 CREATE TABLE "work".registro_actividades_usuarios (
	id_registro_actividad serial NOT NULL,
	id_usuario int4 NOT NULL,
	modulo varchar(255) NOT NULL,
	descripcion text NOT NULL,
	tipo_de_actividad varchar(120) NOT NULL,
	fecha timestamp NOT NULL,
	CONSTRAINT registro_actividades_usuarios_pk PRIMARY KEY (id_registro_actividad),
	CONSTRAINT id_usuario_config_user_fk FOREIGN KEY (id_usuario) REFERENCES "user"(id_user)
);

